<?

class billing extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;

        }
        else
        {

            $this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));

        }

    }

    function index()
    {

        if(!$this->member['billing_customer_id'])
        {

            $params = array();
            $params['page_title'] = "Add A Billing Profile";
            $params['page_description'] = "By adding a new billing profile, your account will be automatically upgraded and charged the first months service. Click here to learn more about our premium members & what benefits you will receive by upgrading.";
            $params['save_button'] = "Create My Billing Profile";

        }
        else
        {

            $params = array();
            $params['page_title'] = "Modify Your Billing Profile";
            $params['page_description'] = "Use the form below to update your current billing profile. Your card will be saved and overwrite any current billing profile you have. Your card will NOT be charged until your next billing cycle.";
            $params['save_button'] = "Save My Billing Profile";

        }

        $this->load->view('header');
        $this->load->view('account/billing', $params);
        $this->load->view('footer');

    }

    public function save()
    {

        $this->form_validation->set_rules("cc_num","Credit Card Number","required|trim|xss_clean");
        $this->form_validation->set_rules("exp_month","Expiration Month","required|trim|xss_clean");
        $this->form_validation->set_rules("exp_year","Expiration Year","required|trim|xss_clean");
        $this->form_validation->set_rules("first_name","First Name","required|trim|xss_clean");
        $this->form_validation->set_rules("last_name","Last Name","required|trim|xss_clean");
        $this->form_validation->set_rules("address","Address","required|trim|xss_clean");
        $this->form_validation->set_rules("city","City","required|trim|xss_clean");
        $this->form_validation->set_rules("state","State","required|trim|xss_clean");
        $this->form_validation->set_rules("zip","Zip","required|trim|xss_clean");

        if(!$this->form_validation->run())
        {

            $this->index();

        }
        else
        {

            $array = array();
            $array['customerId'] = time();
            $array['email'] = $this->member['email'];
            $array['firstName'] = set_value('first_name');
            $array['lastName'] = set_value('last_name');
            $array['address'] = set_value('address');
            $array['city'] = set_value('city');
            $array['state'] = set_value('state');
            $array['zip'] = set_value('zip');
            $array['country'] = "US";
            $array['cardNumber'] = set_value('cc_num');
            $array['expirationDate'] = set_value('exp_year').'-'.set_value('exp_month');

            $profile = $this->authcim->create_profile($array);

            if(!$profile['status'])
            {

                $this->error = $profile['message'];
                $this->index();

            }
            else
            {

                $update = array();
                $update['billing_customer_id'] = $profile['customer_id'];
                $update['billing_payment_id'] = $profile['payment_id'];
                $update['billing_card'] = substr(set_value('cc_num'), -4,4);
                $update['billing_name'] = set_value('first_name')." ".set_value('last_name');
                $update['billing_address'] = set_value('address');
                $update['billing_city'] = set_value('city');
                $update['billing_state'] = set_value('state');
                $update['billing_zip'] = set_value('zip');

                //--- Delete current billing profile (if any exists)
                if($this->member['billing_customer_id']&&$this->member['billing_payment_id'])
                {

                    $this->authcim->delete_profile($this->member['billing_payment_id']);

                }

                //--- Save new profile
                $this->db->where('id', $this->member['id']);
                $this->db->update("members", $update);

                //--- Perform a redirect
                $redirect = "/account/upgrade";

                if($this->session->userdata('redirect'))
                {
                    $redirect = $this->session->userdata('redirect');
                    $this->session->unset_userdata('redirect');
                }

                redirect($redirect);

            }

        }

    }

    public function delete()
    {

        $update = array();
        $update['billing_customer_id'] = NULL;
        $update['billing_customer_id'] = NULL;
        $update['billing_payment_id'] = NULL;
        $update['billing_card'] = NULL;
        $update['billing_name'] = NULL;
        $update['billing_address'] = NULL;
        $update['billing_city'] = NULL;
        $update['billing_state'] = NULL;
        $update['billing_zip'] = NULL;
        $update['premium'] = 0;

        $this->db->where('id', $this->member['id']);
        $this->db->update('members', $update);

        $this->authcim->delete_profile($this->member['billing_payment_id']);

        redirect("/account/billing");

    }

}