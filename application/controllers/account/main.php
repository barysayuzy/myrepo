<?

class main extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can access your \"My StoryBox\" section.");
            redirect('/register/login');
            exit;

        }
        else
        {

            $this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
            $this->selectedTab = 'my_storybox';

        }

    }

    function index()
    {

        $this->load->model('member_model');

        $this->accountSearch = 1;

        $params['body_class'] = 'home';
        $params['films'] = $this->member_model->purchasedFilms(false);
        $params['hero_images'] = $this->site->getHeroImages('account');
        $params['purchasedFilms'] = $this->member_model->purchasedFilms(true,false,false);

        if($params['films'])
        {

            $this->load->view('header', $params);
            $this->load->view('account/my_storybox');
            $this->load->view('footer');

        }
        else
        {

            $this->load->view('header', $params);
            $this->load->view('account/no_movies');
            $this->load->view('footer');

        }

    }

    function listPurchased()
    {

        $params = array();
        $params['films'] = $this->member_model->purchasedFilms(true,false,false);

        //--- Format the search title
        $params['title'] = "Your Purchased Films";

        $this->load->view('header', $params);
        $this->load->view('movie/single_group_list');
        $this->load->view('footer');

    }

    function search()
    {

        $params = array();
        $params['films'] = $this->member_model->searchPurchasedFilms($this->input->get('query'), $this->input->get('type'));

        //--- Format the search title
        if($params['films']) $params['title'] = count($params['films'])." film(s) found matching your search criteria in your MyStoryBox";
        else $params['title'] = "There were no films matching your search criteria in your MyStoryBox";

        $this->load->view('header', $params);
        $this->load->view('movie/single_group_list');
        $this->load->view('footer');

    }

    function value($value_id)
    {

        $genre = $this->site->getValue($value_id);

        $params = array();
        $params['films'] = $this->films->search_playable(null, 'all', array('value'=>$value_id));
        $params['title'] = "My StoryBox > Value > ".$genre['title'];

        $this->load->view('header', $params);
        $this->load->view('movie/single_group_list');
        $this->load->view('footer');

    }

    function educational($value_id)
    {

        $genre = $this->site->getEducationalValue($value_id);

        $params = array();
        $params['films'] = $this->films->search_playable(null, 'all', array('educational_value'=>$value_id));
        $params['title'] = "My StoryBox > Educational Value > ".$genre['title'];

        $this->load->view('header', $params);
        $this->load->view('movie/single_group_list');
        $this->load->view('footer');

    }

    function genre($genre_id)
    {

        $genre = $this->site->getGenre($genre_id);

        $params = array();
        $params['films'] = $this->films->search_playable(null, 'all', array('genre'=>$genre_id));
        $params['title'] = "My StoryBox > Genre > ".$genre['title'];

        $this->load->view('header', $params);
        $this->load->view('movie/single_group_list');
        $this->load->view('footer');

    }

}