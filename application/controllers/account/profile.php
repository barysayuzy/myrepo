<?

class profile extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;

        }
        else
        {

            $this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));

        }

    }

    function index()
    {

        $this->load->view('header');
        $this->load->view('account/profile_form');
        $this->load->view('footer');

    }

    public function save()
    {

        $this->form_validation->set_rules('first_name','First Name','required|xss_clean|trim');
        $this->form_validation->set_rules('last_name','Last Name','required|xss_clean|trim');
        $this->form_validation->set_rules('password','Password','xss_clean|trim|matches[password2]');
        $this->form_validation->set_rules('password2','Re-Type Password','xss_clean|trim|matches[password]');

        if(!$this->form_validation->run())
        {

            $this->index();

        }
        else
        {

            $updateParams = array();
            $updateParams['first_name'] = set_value('first_name');
            $updateParams['last_name'] = set_value('last_name');
            if(set_value('password')) $updateParams['password'] = md5( set_value('password') );

            $this->db->where('id', $this->member['id']);
            $this->db->update('members', $updateParams);

            $this->session->set_flashdata('response', "Your profile has been updated");

            redirect("/account/profile");

        }

    }

}