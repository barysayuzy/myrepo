<?

class upgrade extends CI_Controller
{

    function __construct()
    {

        parent :: __construct();

        $this->settings = $this->system_vars->get_settings();

        if(!$this->session->userdata('member_logged'))
        {

            $this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
            redirect('/register/login');
            exit;

        }
        else
        {

            $this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));

        }

    }

    function index()
    {

        $params = array();
        $params['account_status'] = "Free";

        if($this->member['premium'])
        {

            $params['account_status'] = "Premium";

        }
        else
        {

            if($this->member_model->is_active_premium_member($this->member))
            {

                $params['account_status'] = "Premium Until ".date("m/d/Y", strtotime($this->member['service_expiration']));

            }

        }

        $this->load->view('header');
        $this->load->view('account/upgrade', $params);
        $this->load->view('footer');

    }

    public function process_upgrade()
    {

        $this->load->model('member_model');

        if(!$this->member['billing_customer_id']||!$this->member['billing_payment_id'])
        {

            $this->session->set_flashdata('error', "Please add a valid billing profile to upgrade your account to a premium account");
            $this->session->set_userdata('redirect', $this->uri->uri_string());
            redirect("/account/billing");

        }
        else
        {

            $charge = $this->member_model->charge_subscription_fee($this->member);

            if(!$charge['success'])
            {

                $this->authcim->delete_profile($this->member['payment_id']);

                $this->session->set_flashdata('error', "Failed to charge your billing profile. Please add a new billing profile to upgrade your account to a premium account");
                $this->session->set_userdata('redirect', $this->uri->uri_string());
                $this->index();

            }
            else
            {

                // Save new billing profile
                $update['premium'] = 1;
                $update['service_expiration'] = date("Y-m-d", strtotime("+1 month"));

                $this->db->where('id', $this->member['id']);
                $this->db->update("members", $update);

                //--- Perform a redirect
                redirect("/account/upgrade");

            }

        }

    }

    public function downgrade()
    {

        $update = array();
        $update['premium'] = 0;

        $this->db->where('id', $this->member['id']);
        $this->db->update('members', $update);

        redirect("/account/upgrade");

    }

}