<?

	class article extends CI_Controller
	{
	
		function __construct()
		{
			parent :: __construct();
		}
		
		function view($id)
		{
		
			$getArticle = $this->db->query("SELECT * FROM articles WHERE id = '{$id}' LIMIT 1");
			$t = $getArticle->row_array();
			
			$t['creator'] = $this->system_vars->get_member($t['member_id']);
		
			$this->load->view('header');
			$this->load->view('pages/view_article', $t);
			$this->load->view('footer');
		
		}
		
		function history()
		{
		
			$getArticles = $this->db->query("SELECT * FROM articles ORDER BY date DESC");
			$t['articles'] = $getArticles->result_array();
			
			$this->load->view('header');
			$this->load->view('pages/article_history', $t);
			$this->load->view('footer');
		
		}
	
	}