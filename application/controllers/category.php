<?

	class category extends CI_Controller
	{
	
		function __construct()
		{
			parent :: __construct();
		}
		
		function main($category_url)
		{
			
			// get all experts in this main category
			$date = date("Y-m-d H:i:s");
			$getExperts = $this->db->query("SELECT p.* FROM profiles p,categories c,featured f WHERE (p.category_id = c.id AND c.url = '{$category_url}') AND (f.type = 'category' AND f.profile_id = p.id AND f.end_date >= '{$date}') GROUP BY p.id");
			$t['experts'] = $getExperts->result_array();
			
			
			
		
			$this->load->view('header');
			$this->load->view('pages/expert_list', $t);
			$this->load->view('footer');
		
		}
		
		function sub($category_url,$subcategory_url)
		{
		
			// get all experts in this main category
			$getExperts = $this->db->query
			("
				SELECT
					profiles.* 
					
				FROM
					profile_subcategories, 
					subcategories, 
					profiles
					
				WHERE
					subcategories.url = '{$subcategory_url}' AND
					profile_subcategories.subcategory_id = subcategories.id AND 
					profiles.id = profile_subcategories.profile_id
			");
			
			$t['experts'] = $getExperts->result_array();
		
			$this->load->view('header');
			$this->load->view('pages/expert_list', $t);
			$this->load->view('footer');
		
		}
	
	}