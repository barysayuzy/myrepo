<?php

class Main extends CI_Controller
{
	public function __construct()
	{
		parent :: __construct();

		$this->settings = $this->system_vars->get_settings();
        $this->selectedTab = 'store';

	}

    public function index()
    {

        $params = array();
        $params['films'] = $this->films->obtainFilmsByGenre('popular');
        $params['body_class'] = 'home';
        $params['hero_images'] = $this->site->getHeroImages('store');

        $this->load->view('header', $params);
        $this->load->view('homepage');
        $this->load->view('footer');

    }

    public function store()
    {

        $params = array();
        $params['films'] = $this->films->obtainFilmsByGenre('popular');
        $params['body_class'] = 'home';

        $this->load->view('header', $params);
        $this->load->view('homepage');
        $this->load->view('footer');

    }

	public function getTime()
	{
		$now = new DateTime();
		echo $now->format("M j, Y H:i:s");
	}

	public function logout()
	{
		$this->session->sess_destroy();
		delete_cookie('login');
		redirect();
	}

}