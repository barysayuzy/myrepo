<?

    class Movie extends CI_Controller
    {

        var $purchased;

        function __construct()
        {

            parent :: __construct();

        }

        //--- List films by GENRE
        function genre($genre_id)
        {

            $this->selectedTab = 'store';
            $genre = $this->site->getGenre($genre_id);

            $params = array();
            $params['films'] = $this->films->search(null, 'all', array('genre'=>$genre_id));
            $params['title'] = "Genre > ".$genre['title'];

            $this->load->view('header', $params);
            $this->load->view('movie/single_group_list');
            $this->load->view('footer');

        }

        //--- List films by GENRE
        function value($value_id)
        {

            $this->selectedTab = 'store';
            $genre = $this->site->getValue($value_id);

            $params = array();
            $params['films'] = $this->films->search(null, 'all', array('value'=>$value_id));
            $params['title'] = "Value > ".$genre['title'];

            $this->load->view('header', $params);
            $this->load->view('movie/single_group_list');
            $this->load->view('footer');

        }

        //--- Educational Values
        function educational($value_id = null)
        {

            $this->selectedTab = 'store';
            $genre = $this->site->getEducationalValue($value_id);

            $params = array();
            $params['films'] = $this->films->search(null, 'all', array('educational_value'=>$value_id));
            $params['title'] = "Educational Value > ".$genre['title'];

            $this->load->view('header', $params);
            $this->load->view('movie/single_group_list');
            $this->load->view('footer');

        }

        //--- Search Films
        function search()
        {

            $this->selectedTab = 'store';

            //--- Perform film search
            $params = array();
            $params['films'] = $this->films->search($this->input->get('query'), $this->input->get('type'));

            //--- Format the search title
            if($params['films']) $params['title'] = count($params['films'])." film(s) found matching your search criteria";
            else $params['title'] = "There were no films matching your search criteria";

            $this->load->view('header', $params);
            $this->load->view('movie/single_group_list');
            $this->load->view('footer');

        }

        //--- View Film Details Page
        public function view($film_id = null, $preview = '')
        {

            //--- Check film purchase
            $this->purchased = $this->member_model->checkFilmPurchase($film_id);

            //--- Check to make sure a film ID has been passed, otherwise just fail
            if(!$film_id) die('Enter Film ID in the URI');

            //--- Obtain Film Information
            $film = $this->films->fetch($film_id);

            //--- If film is invalid, then just fail
            if(!$film) die("Invalid film identification");

            //--- Pass film data to array (to view)
            $array = $film->film;
            $array['values'] = $film->getValues();
            $array['genres'] = $film->getGenres();
            $array['educational_values'] = $film->getEducationalValues();
            $array['family_time'] = $film->getFamilyTimeEntries();
            $array['season'] = $film->checkForSeason();

            if(!empty($film->film['year']) && !empty($film->film['running_time']))
                $array['subtitle'] = "({$film->film['year']}) ({$film->film['running_time']})";

            //--- IF season film, replace poster
            if(is_array($array['season']) && isset($array['season']['series']['poster']))
            {
                $array['poster'] = $array['season']['series']['poster'];

                //--- Change title & subtitle for SHOWS
                $array['subtitle'] = $array['title']." ({$film->film['running_time']})";
                $array['title'] = $array['season']['series']['title'];

                //--- Check to see if show has show details, FILMS take precedence
                $array['director'] = (trim($array['director']) ? $array['director'] : $array['season']['series']['director']);
                $array['stars'] = (trim($array['stars']) ? $array['stars'] : $array['season']['series']['stars']);
                $array['creator'] = (trim($array['creator']) ? $array['creator'] : $array['season']['series']['creator']);
                $array['parents'] = (trim($array['parents']) ? $array['parents'] : $array['season']['series']['parents']);
            }
            
            //Pass premium user validation 
            if ($this->session->userdata('member_logged'))
            {
            	$this->memberObject = $this->member_model->fetch($this->session->userdata('member_logged'));
            	$array['isPremiumMember'] = $this->memberObject->isPremiumMember();  
            }
            else
            {
            	$array['isPremiumMember'] = false;
            }            

            //--- Load View
            $this->template_title = $film->film['title'];
            $this->load->template('movie/profile', $array);
        }

        //--- Play Movie
        function play($film_id)
        {

            $filmData = $this->films->prepareToPlay($film_id);

            if($filmData)
            {

                //--- Create the MediaSource URL -- Playable via DRM secured player
                $mediaSource = "http://50.19.115.70/buydrm/mp4:{$filmData['path']}/Manifest";

                //--- Load DRM secured player
                $this->load->model('AuthXMLGenerator');

                $auth_signer = new AuthXMLGenerator();
                $auth_signer->pem('./d7271608cf75c99322b2f2d28282f4aa.pem');

                //--- Set DRM Properties
                //--- $auth_signer->setIP('192.168.0.1');
                //--- $auth_signer->setKID('MHkeGJ1WYkqXlPjzcAJlIw==');

                $auth_signer->setExpirationTime($auth_signer->getUTC('+1 hour'));
                $auth_signer->setPolicy(KeyOSAuthXML_Policies::EXPIRATION_AFTER_FIRST_PLAY, 60 * 10);
                $auth_signer->setPolicy(KeyOSAuthXML_Policies::MINIMUM_SECURITY_LEVEL, '150');
                $auth_signer->setPlayEnablers('786627D8-C2A6-44BE-8F88-08AE255B01A7');
                $auth_signer->isPersistent(true);

                $encoded_auth_xml = $auth_signer->encodeAuthenticationXML($auth_signer->signAuthenticationXML($auth_signer->generateAuthenticationXML()));

                echo "
                <object data=\"data:application/x-silverlight-2,\" type=\"application/x-silverlight-2\" width=\"98%\" height=\"98%\">
                      <param name=\"source\" value=\"/media/player.xap\"/>
                      <param name=\"onError\" value=\"onSilverlightError\" />
                      <param name=\"background\" value=\"white\" />
                      <param name=\"minRuntimeVersion\" value=\"4.0.50826.0\" />
                      <param name=\"autoUpgrade\" value=\"true\" />
                  <param name=\"InitParams\" value=\"params =
                    <InitParams>
                      <PlayerSettings>
                        <LAUrl>http://sl.licensekeyserver.com/core/rightsmanager.asmx</LAUrl>
                        <AutoLoad>true</AutoLoad>
                        <AutoPlay>true</AutoPlay>
                        <StartMuted>false</StartMuted>
                        <IsInstallVisible>true</IsInstallVisible>
                        <LogLevel>None</LogLevel>
                        <ControlsAutohide>false</ControlsAutohide>
                      </PlayerSettings>
                      <PlayList>
                        <Item>
                          <IsDRM>true</IsDRM>
                          <AuthXML>{$encoded_auth_xml}</AuthXML>
                          <MediaSource>{$mediaSource}</MediaSource>
                          <Title>SmoothStreaming content</Title>
                          <DeliveryMethod>adaptivestreaming</DeliveryMethod>
                        </Item>
                        <Item>
                          <IsDRM>true</IsDRM>
                          <AuthXML>{$encoded_auth_xml}</AuthXML>
                          <MediaSource>{$mediaSource}</MediaSource>
                          <Title>ProgressiveDownload content</Title>
                          <DeliveryMethod>progressivedownload</DeliveryMethod>
                        </Item>
                      </PlayList>
                    </InitParams>
                    \" />
                      <a href=\"http://go.microsoft.com/fwlink/?LinkID=149156\" style=\"text-decoration:none\">
                          <img src=\"http://go.microsoft.com/fwlink/?LinkId=161376\" alt=\"Get Microsoft Silverlight\" style=\"border-style:none\"/>
                      </a>
                </object>
                ";

                //--- Load View
                //--- $this->load->view('movie/playback', $filmData);

            }
            else
            {

                $this->session->set_flashdata('error', "You attempted to play a film that you do not have access to. Please use the \"Play Now\" button below and purchase this film to continue.");
                redirect("/movie/view/{$film_id}");

            }

        }

        //--- If the member isn't logged in
        //--- Direct them to login page then redirect back here
        function login($film_id)
        {

            $this->session->set_userdata('redirect', "/movie/view/{$film_id}");
            redirect("/register/login");

        }

        //--- JSON popover data for a specific film
        function popover_data($film_id = null)
        {

            $array = array();

            if($film_id)
            {

                //--- Check film purchase
                $isPurchased = $this->member_model->checkFilmPurchase($film_id);
                $filmObject = $this->films->fetch($film_id);
                $seasonObject = $filmObject->checkForSeason();

                //--- Pass film data to array
                $film = $filmObject->film;
                $array['type'] = $film['type'];
                $array['title'] = $film['title'];
                $array['poster'] = $film['poster'];
                $array['description'] = $film['popover_description'];
                $array['value'] = $film['mainValue'];
                $array['educationalValue'] = $film['mainEducationalValue'];
                $array['isPurchased'] = false;

                //-- Check if this film is purchased already
                //--- If NOT, then provide ALL pricing info
                if($isPurchased)
                {

                    $array['isPurchased'] = true;

                }
                else
                {

                    //--- Get ALL Pricing For this Film
                    if($film['rent'])
                    {
                        if($film['sd_film_id']&&$film['rent_price_sd']>0) $array['purchaseObject'][] = array('link'=>"/purchase/rent/{$film['id']}/sd", 'title'=>"Rent - $".number_format($film['rent_price_sd'], 2));
                        if($film['hd_film_id']&&$film['rent_price_hd']>0) $array['purchaseObject'][] = array('link'=>"/purchase/rent/{$film['id']}/hd", 'title'=>"Rent HD - $".number_format($film['rent_price_hd'], 2));
                    }

                    if($film['purchase'])
                    {
                        if($film['sd_film_id']&&$film['purchase_price_sd']>0) $array['purchaseObject'][] = array('link'=>"/purchase/buy/{$film['id']}/sd", 'title'=>"Buy - $".number_format($film['purchase_price_sd'], 2));
                        if($film['hd_film_id']&&$film['purchase_price_hd']>0) $array['purchaseObject'][] = array('link'=>"/purchase/buy/{$film['id']}/hd", 'title'=>"Buy HD - $".number_format($film['purchase_price_hd'], 2));
                    }

                    if($seasonObject && $seasonObject['series']['allow_group_purchase'])
                    {
                        $array['purchaseObject'][] = array('link'=>"/purchase/group/{$seasonObject['series']['id']}/sd/{$film['id']}", 'title'=>"Buy Season Pass - $".number_format($seasonObject['series']['group_price'], 2));
                    }

                }

                //--- IF season film, replace poster
                if(is_array($seasonObject) && isset($seasonObject['series']['poster']))
                {
                    $array['title'] = $seasonObject['series']['title'];
                    $array['poster'] = $seasonObject['series']['poster'];
                    $array['description'] = $seasonObject['series']['popover_description'];
                }

            }

            //--- Output JSON encoded results to buffer
            echo json_encode($array);

        }

    }