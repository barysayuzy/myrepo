<?

	class articles extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$this->settings = $this->system_vars->get_settings();
			
			if(!$this->session->userdata('member_logged'))
			{
			
				$this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
				redirect('/register/login');
				exit;
			
			}
			else
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
			
			}
			
		}
		
		function index()
		{
		
			$getArticles = $this->db->query("SELECT * FROM articles WHERE member_id = {$this->member['id']} ");
			$t['articles'] = $getArticles->result_array();
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/articles', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function create_new()
		{
		
			$t['page_title'] = "Create A New Article";
			$t['save_button'] = "Add Article";
			$t['form_action'] = "/my_account/articles/create_new_submit";
			
			$t['categories'] = $this->system_vars->get_categories('all');
			$t['title'] = "";
			$t['content'] = "";
			$t['category_id'] = "";
			$t['subcategory_id'] = "";
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/article_form', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function edit($article_id)
		{
		
			$getArticle = $this->db->query("SELECT * FROM articles WHERE id = {$article_id} AND member_id = {$this->member['id']} LIMIT 1");
			$t = $getArticle->row_array();
		
			$t['page_title'] = "Edit Article";
			$t['save_button'] = "Save Article";
			$t['form_action'] = "/my_account/articles/edit_submit/{$article_id}";
			
			$t['categories'] = $this->system_vars->get_categories('all');
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/article_form', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function check_article_length($string)
		{
		
			if(strlen($string) >= 200)
			{
			
				return true;
			
			}
			else
			{
			
				$this->form_validation->set_message('check_article_length','Your article must be at least 200 characters long');
				return false;
			
			}
		
		}
		
		function create_new_submit()
		{
		
			$this->form_validation->set_rules('category','Category','required|trim|xss_clean');
			$this->form_validation->set_rules('subcategory','Subcategory','required|trim|xss_clean');
			$this->form_validation->set_rules('title','Article Title','required|trim|xss_clean');
			$this->form_validation->set_rules('content','Article Content','required|trim|xss_clean|callback_check_article_length');
			
			if(!$this->form_validation->run())
			{
			
				$this->create_new();
			
			}
			else
			{
			
				$insert = array();
				$insert['member_id'] = $this->member['id'];
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['category_id'] = set_value('category');
				$insert['subcategory_id'] = set_value('subcategory');
				$insert['title'] = set_value('title');
				$insert['content'] = set_value('content');
				
				$this->db->insert('articles', $insert);
				$article_id = $this->db->insert_id();
				
				redirect("/my_account/articles");
			
			}
		
		}
		
		function edit_submit($article_id)
		{
		
			$this->form_validation->set_rules('category','Category','required|trim|xss_clean');
			$this->form_validation->set_rules('subcategory','Subcategory','required|trim|xss_clean');
			$this->form_validation->set_rules('title','Article Title','required|trim|xss_clean');
			$this->form_validation->set_rules('content','Article Content','required|trim|xss_clean|callback_check_article_length');
			
			if(!$this->form_validation->run())
			{
			
				$this->create_new();
			
			}
			else
			{
			
				$insert = array();
				$insert['category_id'] = set_value('category');
				$insert['subcategory_id'] = set_value('subcategory');
				$insert['title'] = set_value('title');
				$insert['content'] = set_value('content');
				
				$this->db->where('id', $article_id);
				$this->db->update('articles', $insert);
				
				redirect("/my_account/articles");
			
			}
		
		}
		
		function delete($article_id)
		{
		
			$this->db->where('id', $article_id);
			$this->db->where('member_id', $this->member['id']);
			$this->db->delete('articles');
			
			redirect('/my_account/articles');
		
		}
	
	}