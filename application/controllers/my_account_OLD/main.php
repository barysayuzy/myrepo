<?

	class main extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$this->settings = $this->system_vars->get_settings();
			
			if(!$this->session->userdata('member_logged'))
			{
			
				$this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
				redirect('/register/login');
				exit;
			
			}
			else
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
			
			}
			
		}
		
		function index($profile_id = null)
		{
		
			$this->load->view('header');
			$this->load->view('my_account/main');
			$this->load->view('footer');
		
		}
	
		function create_new_profile()
		{
		
			$t['categories'] = $this->system_vars->get_categories('all');
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/create_new_profile', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function load_subcategories($category_id)
		{
		
			echo json_encode($this->system_vars->get_subcategories($category_id));
		
		}
		
		function check_chat_price($string = null)
		{
		
			if($this->input->post('available_for_chat')=='1')
			{
			
				if($string)
				{
				
					return true;
				
				}
				else
				{
				
					$this->form_validation->set_message('check_chat_price','Please enter a chat cost per minute');
					return false;
				
				}
			
			}
			else
			{
			
				return true;
			
			}
		
		}
		
		function check_email_price($string = null)
		{
		
			if($this->input->post('available_for_email')=='1')
			{
			
				if($string)
				{
				
					return true;
				
				}
				else
				{
				
					$this->form_validation->set_message('check_email_price','Please enter a cost per email');
					return false;
				
				}
			
			}
			else
			{
			
				return true;
			
			}
		
		}
		
		function check_subcategories($string)
		{
		
			if($string)
			{
			
				return true;
			
			}
			else
			{
			
				$this->form_validation->set_message('check_subcategories',"Please select one or more subcategories");
				return false;
			
			}
		
		}
		
		function check_article_length($string)
		{
		
			if(strlen($string) >= 200)
			{
			
				return true;
			
			}
			else
			{
			
				$this->form_validation->set_message('check_article_length','Your article must be at least 200 characters long');
				return false;
			
			}
		
		}
		
		function submit_new_profile()
		{
		
			$this->form_validation->set_rules('category','Category','required|trim|xss_clean');
			$this->form_validation->set_rules('subcategories[]','Subcategories','trim|xss_clean|callback_check_subcategories');
			$this->form_validation->set_rules('brief_description','Brief Description','required|trim|xss_clean');
			$this->form_validation->set_rules('detailed_description','Detailed Description','required|trim|xss_clean');
			$this->form_validation->set_rules('degrees','Degrees','required|trim|xss_clean');
			$this->form_validation->set_rules('experience','Experience','required|trim|xss_clean');
			$this->form_validation->set_rules('available_for_chat','Available For Chat','trim|xss_clean');
			$this->form_validation->set_rules('price_per_minute','Chat Cost Per Minute','trim|xss_clean|callback_check_chat_price');
			$this->form_validation->set_rules('available_for_email','Available For Email','trim|xss_clean');
			$this->form_validation->set_rules('price_per_email','Price Per Email','trim|xss_clean|callback_check_email_price');
			$this->form_validation->set_rules('article_title','Article Title','required|trim|xss_clean');
			$this->form_validation->set_rules('article','Article','required|trim|xss_clean|callback_check_article_length');
			
			if(!$this->form_validation->run())
			{
			
				$this->create_new_profile();
			
			}
			else
			{
			
				$subcategory_array = $this->input->post('subcategories');
			
				// Insert Article First
				$insert = array();
				$insert['member_id'] = $this->member['id'];
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['category_id'] = set_value('category');
				$insert['subcategory_id'] = $subcategory_array[0];
				$insert['title'] = set_value('article_title');
				$insert['content'] = set_value('article');
				
				$this->db->insert('articles', $insert);
				$article_id = $this->db->insert_id();
			
				// Insert member
				$insert = array();
				$insert['member_id'] = $this->member['id'];
				$insert['category_id'] = set_value('category');
				$insert['brief_description'] = set_value('brief_description');
				$insert['detailed_description'] = set_value('detailed_description');
				$insert['degrees'] = set_value('degrees');
				$insert['experience'] = set_value('experience');
				$insert['available_for_chat'] = set_value('available_for_chat');
				$insert['price_per_minute'] = set_value('price_per_minute');
				$insert['available_for_email'] = set_value('available_for_email');
				$insert['price_per_email'] = set_value('price_per_email');
				$insert['article_id'] = $article_id;
				
				$this->db->insert('profiles', $insert);
				$profile_id = $this->db->insert_id();
				
				// Insert subcategories
				foreach($this->input->post('subcategories') as $subcategory_id)
				{
				
					if(trim($subcategory_id)&&is_numeric($subcategory_id))
					{
				
						$insert = array();
						$insert['profile_id'] = $profile_id;
						$insert['subcategory_id'] = $subcategory_id;
						
						$this->db->insert('profile_subcategories', $insert);
					
					}
				
				}
				
				// Redirect with success
				redirect("/profile-submitted");
				
			}
		
		}
		
		function edit_profile($profile_id)
		{
		
			$getProfile = $this->db->query("SELECT * FROM profiles WHERE id = {$profile_id} AND member_id = {$this->member['id']} LIMIT 1");
			$t = $getProfile->row_array();
			
			$getSubcategories = $this->db->query("SELECT * FROM profile_subcategories WHERE profile_id = {$t['id']} ");
			$subcategory_array = array();
			
			foreach($getSubcategories->result_array() as $s)
			{
			
				$subcategory_array[] = $s['subcategory_id'];
			
			}
			
			$t['subcategories'] = $subcategory_array;
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/edit_profile', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function save_profile($profile_id)
		{
		
			$this->form_validation->set_rules('subcategories[]','Subcategories','trim|xss_clean|callback_check_subcategories');
			$this->form_validation->set_rules('brief_description','Brief Description','required|trim|xss_clean');
			$this->form_validation->set_rules('detailed_description','Detailed Description','required|trim|xss_clean');
			$this->form_validation->set_rules('degrees','Degrees','required|trim|xss_clean');
			$this->form_validation->set_rules('experience','Experience','required|trim|xss_clean');
			$this->form_validation->set_rules('available_for_chat','Available For Chat','trim|xss_clean');
			$this->form_validation->set_rules('price_per_minute','Chat Cost Per Minute','trim|xss_clean|callback_check_chat_price');
			$this->form_validation->set_rules('available_for_email','Available For Email','trim|xss_clean');
			$this->form_validation->set_rules('price_per_email','Price Per Email','trim|xss_clean|callback_check_email_price');
			
			if(!$this->form_validation->run())
			{
			
				$this->edit_profile($profile_id);
			
			}
			else
			{
			
				// Insert member - profile_subcategories
				//
				$update = array();
				$update['brief_description'] = set_value('brief_description');
				$update['detailed_description'] = set_value('detailed_description');
				$update['degrees'] = set_value('degrees');
				$update['experience'] = set_value('experience');
				$update['available_for_chat'] = set_value('available_for_chat');
				$insert['price_per_minute'] = set_value('price_per_minute');
				$update['available_for_email'] = set_value('available_for_email');
				$update['price_per_email'] = set_value('price_per_email');
				
				$this->db->where('id', $profile_id);
				$this->db->where('member_id', $this->member['id']);
				$this->db->update('profiles', $insert);
				
				// Delete ALL current subcategories before adding new ones so we dont' create duplicates
				$this->db->where('profile_id', $profile_id);
				$this->db->delete('profile_subcategories');
				
				// Insert subcategories
				foreach($this->input->post('subcategories') as $subcategory_id)
				{
				
					if(trim($subcategory_id)&&is_numeric($subcategory_id))
					{
				
						$insert = array();
						$insert['profile_id'] = $profile_id;
						$insert['subcategory_id'] = $subcategory_id;
						
						$this->db->insert('profile_subcategories', $insert);
					
					}
				
				}
				
				// Redirect with success
				$this->session->set_flashdata('response', "Your profile has been saved!");
				
				redirect("/my_account/main/index/{$profile_id}");
				
			}
		
		}
		
		function become_featured($profile_id)
		{
		
			$get = $this->db->query("SELECT * FROM featured_packages ORDER BY sort");
			$t['packages'] = $get->result_array();
			$t['profile_id'] = $profile_id;
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/become_featured', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function feature_profile($profile_id,$package_id)
		{
		
			$getPackage = $this->db->query("SELECT * FROM featured_packages WHERE id = {$package_id} LIMIT 1");
			$package = $getPackage->row_array();
			$balance = $this->system_vars->member_balance($this->member['id']);
			
			if($balance < $package['price'])
			{
			
				$this->session->set_flashdata('error', "You do not have enough funds in your account to place this order. Please fund your account and try again.");
				redirect("/my_account/main/become_featured/{$profile_id}");
			
			}
			else
			{
				
				// Insert transaction into db
				$insert = array();
				$insert['member_id'] = $this->member['id'];
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['type'] = 'purchase';
				$insert['amount'] = $package['price'];
				$insert['summary'] = "Purchase: {$package['title']}";
				
				$this->db->insert('transactions', $insert);
				
				// Create a "featured" record
				$insert = array();
				$insert['profile_id'] = $profile_id;
				$insert['type'] = $package['type'];
				$insert['end_date'] = date("Y-m-d H:i:s", strtotime("+{$package['weeks']} weeks"));
				
				$this->db->insert('featured', $insert);
				
				// send confirmation email
				$this->system_vars->omail($this->member['email'], 'featured_profile_confirmation', $this->member);
				
				// redirect and confirm
				$this->session->set_flashdata('response', "Your order has been placed and your profile is now featured!");
				redirect("/my_account");
			
			}
		
		}
	
	}