<?

	class messages extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$this->settings = $this->system_vars->get_settings();
			
			if(!$this->session->userdata('member_logged'))
			{
			
				$this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
				redirect('/register/login');
				exit;
			
			}
			else
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
			
			}
			
		}
		
		function index()
		{
		
			$getMessages = $this->db->query("SELECT * FROM messages WHERE ricipient_id = {$this->member['id']} ");
			$t['messages'] = $getMessages->result_array();
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/messages', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function view($message_id)
		{
		
			$getMessages = $this->db->query("SELECT * FROM messages WHERE id = {$message_id} AND (ricipient_id = {$this->member['id']} OR sender_id = {$this->member['id']}) LIMIT 1");
			$t = $getMessages->row_array();
			
			$t['from'] = $this->system_vars->get_member($t['sender_id']);
			$t['to'] = $this->system_vars->get_member($t['ricipient_id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/messages_view', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function delete($message_id)
		{
		
			$this->db->where('id', $message_id);
			$this->db->where('ricipient_id', $this->member['id']);
			$this->db->delete('messages');
			
			$this->session->set_flashdata('response', "Message has been deleted");
			
			redirect("/my_account/messages");
		
		}
		
		function outbox()
		{
		
			$getMessages = $this->db->query("SELECT * FROM messages WHERE sender_id = {$this->member['id']} ");
			$t['messages'] = $getMessages->result_array();
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/messages_outbox', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function compose($message_reply_id = null)
		{
		
			$getAllMembers = $this->db->query("SELECT id,username FROM members ORDER BY username");
			$t['users'] = $getAllMembers->result_array();
			
			$t['to'] = "";
			$t['subject'] = "";
			$t['message'] = "";
			
			// If a reply id was passed… Then
			// prepopulate the fields
			if($message_reply_id)
			{
			
				$getMessage = $this->db->query("SELECT * FROM messages WHERE id = {$message_reply_id} AND (ricipient_id = {$this->member['id']} OR sender_id = {$this->member['id']}) LIMIT 1");
				
				if($getMessage->num_rows()==1)
				{

					$message = $getMessage->row_array();
					
					$sender = $this->system_vars->get_member($message['sender_id']);
					
					$t['to'] = $message['sender_id'];
					$t['subject'] = "Re: ".$message['subject'];
					$t['message'] = "\n\n\n+++ Original Message: {$sender['first_name']} {$sender['last_name']} - {$sender['username']} on ".date("m/d/y h:i:s", strtotime($message['datetime']))." +++\n\n".$message['message'];
				
				}
			
			}
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/messages_compose', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function compose_submit()
		{
		
			$this->form_validation->set_rules("to","Recipient","xss_clean|trim|required");
			$this->form_validation->set_rules("subject","Subject","xss_clean|trim|required");
			$this->form_validation->set_rules("message","Message","xss_clean|trim|required");
			
			if(!$this->form_validation->run())
			{
				
				$this->compose();
			
			}
			else
			{
			
				$insert = array();
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['sender_id'] = $this->member['id'];
				$insert['ricipient_id'] = set_value('to');
				$insert['subject'] = set_value('subject');
				$insert['message'] = set_value('message');
				
				$this->db->insert('messages', $insert);
				
				$this->session->set_flashdata('response', "Your message has been sent");
				
				redirect('/my_account/messages');
			
			}
		
		}
	
	}