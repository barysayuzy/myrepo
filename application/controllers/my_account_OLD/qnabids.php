<?

	class qnabids extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$this->settings = $this->system_vars->get_settings();
			
			if(!$this->session->userdata('member_logged'))
			{
			
				$this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
				redirect('/register/login');
				exit;
			
			}
			else
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
			
			}
			
		}
		
		function index($status = 'new')
		{
		
			switch($status)
			{
			
				case "new":
				
					$t['status'] = "Bid History";
					$t['status_description'] = "Here is a list of all your active bids. These bids are pending, the client is still in the process of selecting an expert.";
					
					$getNewQuestions = $this->db->query("SELECT b.*,q.title as question_title FROM qna_bids b,qna_questions q WHERE q.id = b.question_id AND b.expert_id = {$this->member['id']} AND b.awarded=0 GROUP BY b.id ");
					$t['bids'] = $getNewQuestions->result_array();
				
				break;
				
				case "awarded":
				
					$t['status'] = "Awarded Questions";
					$t['status_description'] = "The following questions have been awarded to you, please go through and submit you answer.";
					
					$getNewQuestions = $this->db->query("SELECT b.*,q.title as question_title FROM qna_bids b,qna_questions q WHERE q.id = b.question_id AND b.expert_id = {$this->member['id']} AND b.awarded=1 AND answer IS NULL GROUP BY b.id ");
					$t['bids'] = $getNewQuestions->result_array();
					
				break;
				
				case "answered":
				
					$t['status'] = "Answered Questions";
					$t['status_description'] = "The following questions have already been answered and are just for reference. No action is required on your part.";
					
					$getNewQuestions = $this->db->query("SELECT b.*,q.title as question_title FROM qna_bids b,qna_questions q WHERE q.id = b.question_id AND b.expert_id = {$this->member['id']} AND b.awarded=1 AND answer IS NOT NULL GROUP BY b.id ");
					$t['bids'] = $getNewQuestions->result_array();
					
				break;
			
			}
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/qnabids', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function view($bid_id)
		{
		
			$get = $this->db->query("SELECT b.awarded, b.answer, b.datetime as bid_date,b.cover_letter as cover_letter, q.*, c.title as category_title, c.url as category_url, s.title as subcategory_title, s.url as subcategory_url FROM qna_bids b,qna_questions q, categories c, subcategories s WHERE q.id = b.question_id AND c.id = q.category_id AND s.id = q.subcategory_id AND b.expert_id = {$this->member['id']} AND b.id={$bid_id} GROUP BY b.id ");
			$t = $get->row_array();
			
			$t['member'] = $this->system_vars->get_member($t['member_id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/qnabid_view', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
				
		function submit_answer($bid_id)
		{
		
			$this->form_validation->set_rules("answer","Answer","required|trim|xss_clean");
			
			if(!$this->form_validation->run())
			{
			
				$this->view($bid_id);
			
			}
			else
			{
			
				// Update database
				$update = array();
				$update['answer'] = set_value("answer");
				
				$this->db->where('id', $bid_id);
				$this->db->update('qna_bids', $update);
				
				// Get Question Info
				$getQuestion = $this->db->query("SELECT b.*,q.title as question_title, q.member_id as member_id FROM qna_bids b,questions q WHERE b.id = {$bid_id} AND q.id = b.question_id GROUP BY b.id LIMIT 1");
				$bid = $getQuestion->row_array();
				
				// Get client email
				$client = $this->system_vars->get_member($bid['member_id']);
				$expert = $this->member;
				
				$question['question_title'] = $bid['question_title'];
				$question['answer'] = set_value("answer");
				$question['client_name'] = $client['first_name']." ".$client['last_name'];
				$question['expert_name'] = $expert['first_name']." ".$expert['last_name'];
				
				// Send Email
				$this->system_vars->omail($client['email'], 'qna_question_answered', $question);
				
				// Set response
				$this->session->set_flashdata("response", "Your answer has been sent to the client. This question has been marked as complete and can be found in your \"Answered\" questions archive.");
				
				// redirect
				redirect("/my_account/qnabids");
			
			}
		
		}
	
	}