<?

	class transactions extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$this->settings = $this->system_vars->get_settings();
			
			if(!$this->session->userdata('member_logged'))
			{
			
				$this->session->set_flashdata('error', "You must login before you can gain access to secured areas");
				redirect('/register/login');
				exit;
			
			}
			else
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
				$this->settings = $this->system_vars->get_settings();
				
				$this->load->library('authcim');
			
			}
			
		}
		
		function index()
		{
		
			$getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'earning' AND member_id = {$this->member['id']} ORDER BY datetime DESC");
			$t['transactions'] = $getTransactions->result_array();
			$t['title'] = "Transactions - Earnings";
			
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transactions', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function payments()
		{
		
			$getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'payment' AND member_id = {$this->member['id']} ORDER BY datetime DESC");
			$t['transactions'] = $getTransactions->result_array();
			$t['title'] = "Transactions - Payments";
			
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transactions', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function deposits()
		{
		
			$getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'deposit' AND member_id = {$this->member['id']} ORDER BY datetime DESC");
			$t['transactions'] = $getTransactions->result_array();
			$t['title'] = "Transactions - Deposits";
			
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transactions', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function purchases()
		{
		
			$getTransactions = $this->db->query("SELECT * FROM transactions WHERE type = 'purchase' AND member_id = {$this->member['id']} ORDER BY datetime DESC");
			$t['transactions'] = $getTransactions->result_array();
			$t['title'] = "Transactions - Purchases";
			
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transactions', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function request_payout()
		{
		
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
			
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transaction_request_payout', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function check_amount($amount)
		{
		
			if($amount)
			{
			
				if(is_numeric($amount))
				{
				
					if($amount > 0)
					{
					
						$balance = $this->system_vars->member_balance($this->member['id']);
					
						if($amount <= $balance)
						{
						
							return true;
						
						}
						else
						{
						
							$this->form_validation->set_message('check_amount', "You cannot request a payout greater than your balance.");
							return false;
						
						}
					
					}
					else
					{
					
						$this->form_validation->set_message('check_amount', "Your amount must be greater than 0");
						return false;
					
					}
				
				}
				else
				{
				
					$this->form_validation->set_message('check_amount', "Your amount must be a number greater than 0");
					return false;
				
				}
			
			}
			else
			{
			
				return true;
			
			}
		
		}
		
		function submit_payout_request()
		{
		
			$this->form_validation->set_rules('amount','Amount','required|trim|xss_clean|callback_check_amount');
			
			if(!$this->form_validation->run())
			{
			
				$this->request_payout();
			
			}
			else
			{
			
				$insert = array();
				$insert['member_id'] = $this->member['id'];
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['type'] = 'payment';
				$insert['amount'] = set_value('amount');
				$insert['summary'] = "Payout Request";
				
				$this->db->insert('transactions', $insert);
				
				$this->session->set_flashdata('response', "Your payout request has been submitted. Please allow up to 48 hours for your request to be processed.");
				
				redirect("/my_account/transactions");
			
			}
		
		}
		
		function fund_your_account()
		{
		
			$t['balance'] = $this->system_vars->member_balance($this->member['id']);
			
			$getBillingProfiles = $this->db->query("SELECT * FROM billing_profiles WHERE member_id = {$this->member['id']} ");
			$t['billing_profiles'] = $getBillingProfiles->result_array();
			
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transaction_fund_account', $t);
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function fund_with_paypal()
		{
		
			$this->form_validation->set_rules('amount', 'Amount','xss_clean|trim|required|greater_than[0]');
			
			if(!$this->form_validation->run())
			{
			
				$this->fund_your_account();
			
			}
			else
			{
			
				// Set paypal variables
				$t['item'] = "IYA Deposit";
				$t['item_number'] = "1";
				$t['custom'] = $this->member['id'];
				$t['return_url'] = site_url("my_account/transactions/paypal_return");
				$t['cancel_url'] = site_url("my_account/transactions/paypal_cancel");
				$t['notify_url'] = site_url("paypal_ipn/deposit");
				$t['amount'] = set_value('amount');
				
				// Load paypal module
				$this->load->view('pages/paypal', $t);
			
			}
		
		}
		
		function paypal_return()
		{
		
			$this->session->set_flashdata('response', "Your PayPal payment has been processed. Please allow a little time for the changes to take affect.");
			redirect("/my_account/transactions");
			
		}
		
		function paypal_cancel()
		{
		
			$this->session->set_flashdata('error', "Your PayPal payment has been canceled. ");
			redirect("/my_account/transactions");
		
		}
	
		function add_billing_profile()
		{
		
			$this->load->view('header');
			$this->load->view('my_account/header');
			$this->load->view('my_account/transaction_add_billing_profile');
			$this->load->view('my_account/footer');
			$this->load->view('footer');
		
		}
		
		function submit_billing_profile()
		{
		
			$this->form_validation->set_rules("cc_num","Credit Card Number","required|trim|xss_clean");
			$this->form_validation->set_rules("exp_month","Expiration Month","required|trim|xss_clean");
			$this->form_validation->set_rules("exp_year","Expiration Year","required|trim|xss_clean");
			$this->form_validation->set_rules("first_name","First Name","required|trim|xss_clean");
			$this->form_validation->set_rules("last_name","Last Name","required|trim|xss_clean");
			$this->form_validation->set_rules("address","Address","required|trim|xss_clean");
			$this->form_validation->set_rules("city","City","required|trim|xss_clean");
			$this->form_validation->set_rules("state","State","required|trim|xss_clean");
			$this->form_validation->set_rules("zip","Zip","required|trim|xss_clean");
			
			if(!$this->form_validation->run())
			{
			
				$this->add_billing_profile();
			
			}
			else
			{
			
				$array = array();
				$array['customerId'] = time();
				$array['email'] = $this->member['email'];
				$array['firstName'] = set_value('first_name');
				$array['lastName'] = set_value('last_name');
				$array['address'] = set_value('address');
				$array['city'] = set_value('city');
				$array['state'] = set_value('state');
				$array['zip'] = set_value('zip');
				$array['country'] = "US";
				$array['cardNumber'] = set_value('cc_num');
				$array['expirationDate'] = set_value('exp_year').'-'.set_value('exp_month');
				
				$profile = $this->authcim->create_profile($array);
				
				if(!$profile['status'])
				{
				
					$this->error = $profile['message'];
					$this->add_billing_profile();
				
				}
				else
				{
				
					$insert = array();
					$insert['member_id'] = $this->member['id'];
					$insert['customer_id'] = $profile['customer_id'];
					$insert['payment_id'] = $profile['payment_id'];
					$insert['card_number'] = substr(set_value('cc_num'), -4,4);
					$insert['card_name'] = set_value('first_name')." ".set_value('last_name');
					
					$this->db->insert("billing_profiles", $insert);
					
					redirect("/my_account/transactions/fund_your_account");
				
				}
				
			
			}
		
		}
		
		function delete_billing_profile($billing_id)
		{
		
			$getBP = $this->db->query("SELECT * FROM billing_profiles WHERE id = {$billing_id} AND member_id = {$this->member['id']} LIMIT 1");
			$b = $getBP->row_array();
			
			$delete = $this->authcim->delete_profile($b['customer_id']);
			
			if(!$delete['status'])
			{
			
				$this->session->set_flashdata('error', $delete['message']);
				redirect("/my_account/transactions/fund_your_account");
			
			}
			else
			{
		
				$this->db->where('id', $billing_id);
				$this->db->where('member_id', $this->member['id']);
				$this->db->delete('billing_profiles');
				
				redirect("/my_account/transactions/fund_your_account");
			
			}
		
		}
		
		function submit_deposit()
		{
		
			$this->form_validation->set_rules('profile','Billing Profile','required|trim|xss_clean');
			$this->form_validation->set_rules('amount','Amount','required|trim|xss_clean|greater_than[0]');
			
			if(!$this->form_validation->run())
			{
			
				$this->fund_your_account();
			
			}
			else
			{
			
				$getBP = $this->db->query("SELECT * FROM billing_profiles WHERE id = ".set_value('profile')." AND member_id = {$this->member['id']} LIMIT 1");
				$b = $getBP->row_array();
			
				$charge = $this->authcim->charge_card($b['customer_id'], $b['payment_id'], set_value('amount'), time(), 'IYA Account Fund');
				
				if(!$charge['status'])
				{
				
					$this->session->set_flashdata('error', $charge['message']);
					redirect("/my_account/transactions/fund_your_account");
				
				}
				else
				{
				
					$transaction_id = $charge['transaction_id'];
				
					$insert = array();
					$insert['member_id'] = $this->member['id'];
					$insert['datetime'] = date("Y-m-d H:i:s");
					$insert['type'] = 'deposit';
					$insert['amount'] = set_value('amount');
					$insert['summary'] = "Credit/Debit Deposit #{$transaction_id}";
					$insert['transaction_id'] = $transaction_id;
					
					$this->db->insert('transactions', $insert);
					
					$member = $this->member;
					$member['transaction_id'] = $transaction_id;
					$member['amount'] = "$".number_format(set_value('amount'), 2);
					
					$this->system_vars->omail($member['email'],'cc_account_funded', $member);
					
					$this->session->set_flashdata('response', "Your account has been funded");
					redirect("/my_account/transactions");
				
				}
			
			}
		
		}
	
	}