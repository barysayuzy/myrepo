<?

	class paypal_ipn extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
		}
		
		function deposit()
		{
		
			$status = $this->input->post('payment_status');
			$custom = $this->encrypt->decode(base64_decode($this->input->post("custom")));
			$transaction_id = $this->input->post("txn_id");
			$fund_amount = $this->input->post('mc_gross');
			
			if($status=='Completed')
			{
			
				$insert = array();
				$insert['member_id'] = $custom;
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['type'] = 'deposit';
				$insert['amount'] = $fund_amount;
				$insert['summary'] = "PayPal Deposit #{$transaction_id}";
				$insert['transaction_id'] = $transaction_id;
				
				$this->db->insert('transactions', $insert);
				
				$member = $this->system_vars->get_member($custom);
				$member['transaction_id'] = $transaction_id;
				$member['amount'] = "$".number_format($fund_amount, 2);
				
				$this->system_vars->omail($member['email'],'paypal_account_funded', $member);
			
			}
		
		}
	
	}