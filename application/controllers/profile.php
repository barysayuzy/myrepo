<?

	class profile extends CI_Controller
	{
	
		function __construct()
		{
			
			parent :: __construct();
			
			$getProfile = $this->db->query("SELECT * FROM profiles WHERE id = {$this->uri->segment('3')} LIMIT 1");
			$this->profile = $getProfile->row_array();
			
			$this->profile['member'] = $this->system_vars->get_member($this->profile['member_id']);
			
			// Check communication types
			if($this->profile['available_for_chat'] && $this->profile['available_for_email']) $communication_type = "Chat & Email";
			elseif($this->profile['available_for_chat']) $communication_type = "Chat Only";
			elseif($this->profile['available_for_email']) $communication_type = "Email Only";
			else $communication_type = "None";
			
			$this->profile['communication_type'] = $communication_type;
			
			// Subcategories
			$getSubcategories = $this->db->query("SELECT subcategories.title,subcategories.url FROM profile_subcategories,subcategories WHERE profile_subcategories.profile_id = {$this->profile['id']} AND subcategories.id = profile_subcategories.subcategory_id GROUP BY profile_subcategories.id ");
			$total_subcategories = $getSubcategories->num_rows();
			$subcategory_string = "";
			
			foreach($getSubcategories->result_array() as $i=>$p)
			{
			
				$subcategory_string .= "{$p['title']}";
				
				if($total_subcategories!=($i+1))
				{
					$subcategory_string .= "<br />";
				}
			
			}
			
			$this->profile['subcategories'] = $subcategory_string;
			
			// Logged in
			if($this->session->userdata('member_logged'))
			{
			
				$this->member = $this->system_vars->get_member($this->session->userdata('member_logged'));
			
			}
			
		}
		
		function view($profile_id)
		{
		
			$this->load->view('header');
			$this->load->view('profile/profile_view');
			$this->load->view('footer');
		
		}
		
		function send_question($profile_id)
		{
		
			if(!isset($this->member['id']))
			{
			
				$this->session->set_flashdata('response', "You must be logged in before you can send a question to any expert.");
				$this->session->set_userdata('redirect', $this->uri->uri_string());
				
				redirect('/register/login');
			
			}
			else
			{
		
				$this->load->view('header');
				$this->load->view('profile/send_question');
				$this->load->view('footer');
			
			}
		
		}
		
		function submit_question($profile_id)
		{
		
			$this->form_validation->set_rules("title","","required|trim|xss_clean");
			$this->form_validation->set_rules("question","","required|trim|xss_clean");
			$this->form_validation->set_rules("deadline","","required|trim|xss_clean");
			$this->form_validation->set_rules("date","","required|trim|xss_clean");
			$this->form_validation->set_rules("price","","required|trim|xss_clean");
			
			if(!$this->form_validation->run())
			{
			
				$this->send_question($profile_id);
			
			}
			else
			{
			
				// Insert into db
				$insert = array();
				$insert['expert_id'] = $this->profile['member']['id'];
				$insert['member_id'] = $this->member['id'];
				$insert['datetime'] = date("Y-m-d H:i:s");
				$insert['title'] = set_value('title');
				$insert['question'] = set_value('question');
				if(set_value('deadline')=='1') $insert['deadline'] = date("Y-m-d H:i:s", strtotime(str_replace("@","", set_value('date'))));
				$insert['price_range'] = set_value('price');
				
				$this->db->insert('questions', $insert);
				
				// Send confirmation email to expert
				$question = $insert;
				
				// Get client email
				$client = $this->member;
				$expert = $this->profile['member'];
				
				$question['client_name'] = $client['first_name']." ".$client['last_name'];
				$question['expert_name'] = $expert['first_name']." ".$expert['last_name'];
				
				// Send Email
				$this->system_vars->omail($client['email'], 'expert_new_question', $question);
				
				// redirect
				$this->session->set_flashdata("response", "Your question has been submitted to the expert. You will get an email when the expert has bid on your question.");
				redirect("/profile/view/{$this->profile['id']}");
			
			}
		
		}
	
	}