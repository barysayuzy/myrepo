<?

    class purchase extends CI_Controller
    {

        var $memberObject;

        function __construct()
        {

            parent :: __construct();

            $this->settings = $this->system_vars->get_settings();

            if(!$this->session->userdata('member_logged'))
            {

                $this->session->set_flashdata('error', "Please login to make a purchase");
                $this->session->set_userdata('redirect', $this->uri->uri_string());
                redirect('/register/login');
                exit;

            }
            else
            {

                $this->memberObject = $this->member_model->fetch($this->session->userdata('member_logged'));

                $this->member = $this->memberObject->member;

            }

        }

        //--- Buy a film functionality
        //--- $film_id: int
        //--- $definition: sd or hd
        function buy($film_id, $definition = 'sd')
        {

            if($this->member_model->checkFilmPurchase($film_id))
            {

                redirect("/movie/view/{$film_id}");
                exit;

            }

            $data = $this->films->process_price($film_id, $definition, 'buy', $this->memberObject->isPremiumMember());

            $paramaters = array();
            $paramaters['title'] = "You are about to purchase \"{$data['film']['title']}\" ({$data['definition']}) for $".$data['grand_total'];
            $paramaters['image'] = "/assets/uploads/{$data['film']['poster']}";
            $paramaters['caption'] = "If you agree, click the \"Confirm Purchase\" button below.";
            $paramaters['price'] = $data['grand_total'];
            $paramaters['definition'] = $definition;
            $paramaters['type'] = 'buy';
            $paramaters['film_id'] = $film_id;

            //--- Check if this is a premium member
            if($this->memberObject->isPremiumMember())
            {
                $paramaters['title'] = "You are about to purchase \"{$data['film']['title']}\" ({$data['definition']}) for $".$data['grand_total'];
                $paramaters['premium_discount'] = "You are receiving a premium member discount of $".$data['discount_total'];
            }

            $this->init($paramaters);

        }

        //--- Rent a film functionality
        //--- $film_id: int
        //--- $definition: sd or hd
        function rent($film_id, $definition = 'sd')
        {

            if($this->member_model->checkFilmPurchase($film_id))
            {

                redirect("/movie/view/{$film_id}");
                exit;

            }

            $data = $this->films->process_price($film_id, $definition, 'rent', $this->memberObject->isPremiumMember());

            $paramaters = array();
            $paramaters['title'] = "You are about to rent \"{$data['film']['title']}\" ({$data['definition']}) for $".$data['grand_total'];
            $paramaters['image'] = "/assets/uploads/{$data['film']['poster']}";
            $paramaters['caption'] = "If you agree, click the \"Confirm Purchase\" button below.";
            $paramaters['price'] = $data['grand_total'];
            $paramaters['definition'] = $definition;
            $paramaters['type'] = 'rent';
            $paramaters['film_id'] = $film_id;

            //--- Check if this is a premium member
            if($data['discount_total'] > 0)
            {
                $paramaters['title'] = "You are about to rent \"{$data['film']['title']}\" ({$data['definition']}) for $".$data['grand_total'];
                $paramaters['premium_discount'] = "You are receiving a premium member discount of $".$data['discount_total'];
            }

            $this->init($paramaters);

        }

        //--- Purchase a group pass to the season
        function group($season_id, $definition = 'sd', $episode_film_id = null)
        {

            if($this->member_model->checkFilmPurchase($season_id, 1))
            {

                die('You have already purchased this season');

            }

            $data = $this->films->process_price($season_id, $definition, 'group', $this->memberObject->isPremiumMember());

            $paramaters = array();
            $paramaters['title'] = "You are about to purchase a season pass to \"{$data['season']['title']}\" for $".number_format($data['season']['group_price'], 2);
            $paramaters['image'] = "/assets/uploads/{$data['season']['poster']}";
            $paramaters['caption'] = "If you agree, click the \"Confirm Purchase\" button below.";
            $paramaters['price'] = $data['season']['group_price'];
            $paramaters['definition'] = $definition;
            $paramaters['type'] = 'group';
            $paramaters['film_id'] = $season_id;

            //--- Check if this is a premium member
            if($data['discount_total'] > 0)
            {
                $paramaters['title'] = "You are about to purchase the season \"{$data['season']['title']}\" ({$data['definition']}) for $".$data['grand_total'];
                $paramaters['premium_discount'] = "You are receiving a premium member discount of $".$data['discount_total'];
            }

            $paramaters['episode_film_id'] = $episode_film_id;

            $this->init($paramaters);

        }

        function init($params)
        {

            $this->template_title = "Review & Confirm Purchase";
            $this->load->template('movie/checkout', $params);

        }

        //--- Change billing profile
        //--- Save URI Link to redirect back here after finished updating
        function change_billing_profile($type,$film_id,$definition)
        {

            $this->session->set_userdata('redirect', "/purchase/{$type}/{$film_id}/{$definition}");
            redirect("/account/billing");

        }

        //--- Confirm Purchase
        function confirm($film_id, $definition = 'sd', $purchase_type = 'buy', $episode_film_id = null)
        {

            $purchase = $this->member_model->purchase_film($film_id, $definition, $purchase_type);

            print_r($purchase);
            exit;

            if(!$purchase['success'])
            {

                $this->session->set_flashdata('error', $purchase['message']);
                redirect("/purchase/{$purchase_type}/{$film_id}/{$definition}");

            }
            else
            {

                $this->session->set_flashdata("response", "Your purchase was successful and has been added to your \"My StoryBox\" section. To start watching your newly purchase film(s), simply click the \"Play Now\" button.");

                if($purchase_type != 'group')
                {

                        redirect("/movie/view/{$film_id}");

                }
                else
                {

                    //$seasonInfo = $this->films->checkForSeason($film_id);
                    //redirect("/movie/view/{$seasonInfo['films'][0]['id']}");

                    redirect("/movie/view/{$episode_film_id}");

                }

            }

        }

    }