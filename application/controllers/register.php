<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class register extends CI_Controller {

	function __construct()
	{

		parent :: __construct();

		$this->settings = $this->system_vars->get_settings();

		parse_str($_SERVER['QUERY_STRING'], $_GET);
	}

	function index()
	{

		if ($this->session->userdata('member_logged'))
		{

			redirect('/register/confirmation');
			exit;
		}

		$this->load->view('header');
		$this->load->view('/registration/register_form');
		$this->load->view('footer');
	}

	function login()
	{

		if ($this->session->userdata('member_logged'))
		{

			redirect('/register/confirmation');
			exit;
		}

		$this->hide_nav = true;

		$this->load->view('header');
		$this->load->view('/registration/login_form');
		$this->load->view('footer');
	}

	function login_submit()
	{

		$this->form_validation->set_rules('email_address', "Email Address", "xss_clean|trim|valid_email|required");
		$this->form_validation->set_rules('password', "Password", "xss_clean|trim|required");
		$this->form_validation->set_rules('remember', "Remember Me", "xss_clean|trim");

		if (!$this->form_validation->run())
		{

			$this->login();
		}
		else
		{

			$getUser = $this->db->query("SELECT * FROM members WHERE email = '" . set_value('email_address') . "' AND password = '" . md5(set_value('password')) . "' LIMIT 1");

			if ($getUser->num_rows() == 0)
			{

				$this->error = "Invalid email address/password combination.";
				$this->login();
			}
			else
			{

				$user = $getUser->row_array();

				$this->session->set_userdata('member_logged', $user['id']);

				$redirect = $this->session->userdata('redirect');

				if (trim($redirect))
				{

					redirect($redirect);
					exit;
				}

                //--- If premium member, take to homepage
                if($user['premium'])
                {

                    redirect('/');

                }

                //--- If regular member, take to my storybox page
                else
                {

                    redirect('/account');

                }
			}
		}
	}

	function submit()
	{

		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean|trim');
		$this->form_validation->set_rules('email_address', 'Email Address', 'required|xss_clean|trim|valid_email|callback_check_email');
		$this->form_validation->set_rules('email_address2', 'Confirm Email Address', 'required|xss_clean|trim|valid_email|matches[email_address]');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean|trim|matches[password2]');
		$this->form_validation->set_rules('password2', 'Re-Type Password', 'required|xss_clean|trim|matches[password]');

		if (!$this->form_validation->run())
		{

			// Show error
			$this->index();
		}
		else
		{

			$code = rand(1000000, 9999999);

			// Create record
			$insert = array();
			$insert['registration_date'] = date("Y-m-d H:i:s");
			$insert['email'] = set_value('email_address');
			$insert['password'] = md5(set_value('password'));
			$insert['first_name'] = set_value('first_name');
			$insert['last_name'] = set_value('last_name');
			$insert['code'] = $code;
			$insert['validated'] = 1;

			$this->db->insert('members', $insert);
			$member_id = $this->db->insert_id();

			// Send registration email
			// $insert['password'] = set_value('password');
			// $insert['verification_url'] = "<a href='".site_url("/register/verify_registration/{$member_id}/{$code}")."'>Click here to validate your account</a>";

			$this->system_vars->omail(set_value('email'), 'registration-confirmation', $insert);

			$this->session->set_userdata('member_logged', $member_id);

			// Log the user in
			redirect('/register/confirmation');
		}
	}

	function confirmation()
	{

		$this->load->view('header');
		$this->load->view('/registration/confirmation');
		$this->load->view('footer');
	}

	function check_email($CheckString = '')
	{

		if ($CheckString)
		{

			$checkEmail = $this->db->query("SELECT `id` FROM `members` WHERE `email`=\"{$CheckString}\" LIMIT 1");

			if ($checkEmail->num_rows() == 0)
			{

				return true;
			}
			else
			{

				$this->form_validation->set_message('check_email', "That email address is already associated with another account.");
				return false;
			}
		}
		else
		{

			return true;
		}
	}

	function verify_registration($user_id, $code)
	{
		if (!$user_id || !$code)
		{

			die('invalid attempt to validate account!');
		}
		else
		{

			$checkUser = $this->db->query("SELECT * FROM members WHERE id = {$user_id} AND  code = '{$code}'  LIMIT 1");

			if ($checkUser->num_rows() == 0)
			{

				$this->session->set_flashdata('error', "Invalid attempt to validate your account");
				redirect('/register/login');
			}
			else
			{
				$user = $checkUser->row_array();

				$array['code'] = '';
				$array['validated'] = 1;

				$this->db->where('id', $user['id']);
				$this->db->update('members', $array);

				$this->session->set_userdata('member_logged', $user['id']);

				// Send confirmaiton email
				$this->system_vars->omail($user['email'], "registration_confirmation", $user);

				// Redirect
				$redirect = $this->session->userdata('redirect');

				if (trim($redirect))
				{

					redirect($redirect);
					exit;
				}
			}
		}
	}

	function forgot_password()
	{

		$this->load->view("header");
		$this->load->view("registration/forgot_password");
		$this->load->view("footer");
	}

	function forgot_password_submit()
	{

		$this->form_validation->set_rules('email', 'Email Address', 'required|xss_clean|trim|valid_email');
		$this->form_validation->set_rules('password', 'Choose A Password', 'required|xss_clean|trim|matches[password2]');
		$this->form_validation->set_rules('password2', 'Re-Type Password', 'required|xss_clean|trim|matches[password]');

		if (!$this->form_validation->run())
		{

			$this->forgot_password();
		}
		else
		{

			// Get user info
			$get_user = $this->db->query("SELECT * FROM members WHERE email = '" . set_value('email') . "' LIMIT 1");

			if ($get_user->num_rows() == 0)
			{

				$this->session->set_flashdata('error', "That email address has not been registered to an account holder.");
				redirect("/register/forgot_password");
			}
			else
			{

				$user = $get_user->row_array();
				$array = $user;

				// Update DB Record
				$array['code'] = rand(100000000, 9999999999);
				$array['new_password'] = md5(set_value('password'));

				$this->db->where('id', $user['id']);
				$this->db->update('members', $array);

				$link = site_url("register/confirm_password_reset/" . $user['id'] . "/" . $array['code']);
				$user['link'] = $link;

				// Send Email
				$user['code'] = $array['code'];
				$this->system_vars->omail($user['email'], 'reset_password', $user);

				// redirect
				$this->session->set_flashdata('response', "Please check your email account to complete your password reset.");
				redirect("/register/login");
			}
		}
	}

	function confirm_password_reset($user_id = null, $reset_code = null)
	{

		if (!$user_id || !$reset_code)
		{

			die('invalid attempt to reset password');
		}
		else
		{

			$checkUser = $this->db->query("SELECT * FROM members WHERE id = {$user_id} AND code = '{$reset_code}' AND new_password != '' LIMIT 1");

			if ($checkUser->num_rows() == 0)
			{

				die('invalid attempt to reset password');
			}
			else
			{

				$user = $checkUser->row_array();

				$array['code'] = '';
				$array['new_password'] = '';
				$array['password'] = $user['new_password'];

				$this->db->where('id', $user['id']);
				$this->db->update('members', $array);

				$this->session->set_userdata('member_logged', $user['id']);

				// Redirect
				$this->session->set_flashdata('response', "Your password has been reset successfully");
				redirect('/my_account');
			}
		}
	}

    public function facebook()
    {

        $facebookId = $this->input->post('id');
        $firstName = $this->input->post('first_name');
        $lastName = $this->input->post('last_name');
        $emailAddress = $this->input->post('email');

        if(!$emailAddress)
        {

            $array = array();
            $array['error'] = '1';
            $array['message'] = "We are not able to connect you via Facebook, you are not sharing your email address/";

        }

        elseif(!$facebookId)
        {

            $array = array();
            $array['error'] = '1';
            $array['message'] = "We are not able to connect you via Facebook, we cannot obtain your Facebook ID";

        }
        else
        {

            //--- Check for a user in the DB
            $find = $this->db->query("SELECT * FROM members WHERE facebook_id = '{$facebookId}' OR email = '{$emailAddress}' LIMIT 1");

            if($find->num_rows() == 1)
            {

                $member = $find->row_array();

                $this->session->set_userdata('member_logged', $member['id']);

                $update = array();
                $update['facebook_id'] = $facebookId;

                $this->db->where('id', $member['id']);
                $this->db->update('members', $update);

                $array = array();
                $array['error'] = '0';
                $array['redirect'] = "/account";

            }
            else
            {

                $code = rand(1000000, 9999999);

                // Create record
                $insert = array();
                $insert['registration_date'] = date("Y-m-d H:i:s");
                $insert['email'] = $emailAddress;
                $insert['password'] = md5($code);
                $insert['first_name'] = $firstName;
                $insert['last_name'] = $lastName;
                $insert['code'] = $code;
                $insert['validated'] = 1;
                $insert['facebook_id'] = $facebookId;

                $this->db->insert('members', $insert);
                $member_id = $this->db->insert_id();

                $insert['password'] = $code;

                $this->system_vars->omail($emailAddress, 'registration-confirmation-facebook', $insert);

                $this->session->set_userdata('member_logged', $member_id);

                // Log the user in
                $array = array();
                $array['error'] = '0';
                $array['redirect'] = "/register/confirmation";

            }

        }

        echo json_encode($array);

    }

}