<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class amazon extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $administrator = $this->session->userdata('admin_is_logged');

        // check for superadmin only
        if(!$this->session->userdata('admin_is_logged'))
        {
            redirect('/vadmin/login');
            exit;
        }

        $this->results_per_page = 100;
        $this->response = null;
        $this->error = null;
        $this->admin = $this->session->userdata('admin_is_logged');
        $this->open_nav = null;

    }

    function index()
    {

        $getFiles = $this->db->query("SELECT * FROM video_files ");
        $t['files'] = $getFiles->result_array();

        $this->load->view('vadmin/header');
        $this->load->view('vadmin/amazon/main', $t);
        $this->load->view('vadmin/footer');

    }

    function stream($videoId = null)
    {

        $getFiles = $this->db->query("SELECT * FROM video_files WHERE id = {$videoId} LIMIT 1 ");
        $t = $getFiles->row_array();

        $this->load->view('vadmin/header');
        $this->load->view('vadmin/amazon/stream_video', $t);
        $this->load->view('vadmin/footer');

    }

    function delete($videoId = null)
    {

        $file = $this->db->query("SELECT * FROM video_files WHERE id = {$videoId} LIMIT 1 ")->row_array();

        if(trim($file['filename'])&&file_exists("./media/films/{$file['filename']}")) unlink("./media/films/{$file['filename']}");

        $this->db->where('id', $videoId);
        $this->db->delete('video_files');

        redirect("/vadmin/amazon");

    }

}
