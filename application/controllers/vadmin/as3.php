<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class as3 extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $this->load->library('s3');

    }

    function index()
    {

        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        /*
        $inputFile = $this->s3->inputFile("./media/assets/1355319990.jpg");
        $data = $this->s3->putObject($inputFile, 'storybox-videos', '1355319990.jpg');
        print_r($data);
        */

        $bucketStuffs = $this->s3->getBucket('storybox-videos');
        print_r($bucketStuffs);

    }

    function upload()
    {

        ini_set('upload_max_filesize','4096M');
        ini_set('post_max_size','4096M');
        ini_set('memory_limit','4096M');

        ini_set('max_execution_time','600000');
        ini_set('max_input_time','600000');

        set_time_limit(0);

        if(isset($_FILES['file_upload']['tmp_name']))
        {

            //--- Rename file
            $tmpName = $_FILES['file_upload']['tmp_name'];
            $extension = trim( str_replace( ".", "", substr($_FILES['file_upload']['name'], -4, 4) ) );
            $newFilename = time() . "." . $extension;

            //--- Save film to disk temporarily
            //--- Delete when uploaded to S3
            move_uploaded_file($tmpName, "./media/films/{$newFilename}");

            //--- Store File in DB
            $insert = array();
            $insert['date'] = date("Y-m-d H:i:s");
            $insert['title'] = $_POST['videoTitle'];
            $insert['filename'] = $newFilename;
            $insert['orig_file_name'] = $_FILES['file_upload']['name'];

            //--- Insert into DB for tracking
            $this->db->insert('video_files', $insert);
            $fileid = $this->db->insert_id();

            //--- Execute a BACKGROUND Shell request to transmit the file to S3
            $URL = site_url("/vadmin/as3/process_files/{$fileid}");
            shell_exec("wget {$URL} >>/dev/null 2>>/dev/null &");

            //--- Redirect
            redirect("/vadmin/amazon");

        }
        else
        {

            echo "failed upload...";

        }

    }

    function process_files($fileid)
    {

        //--- Get Settings
        $settings = $this->system_vars->get_settings();

        //--- Obtain video information
        $file = $this->db->query("SELECT * FROM video_files WHERE id = {$fileid} LIMIT 1")->row_array();

        //--- Transmit file to AS3
        $inputFile = $this->s3->inputFile("./media/films/".$file['filename']);
        $putObject = $this->s3->putObject($inputFile, 'storybox-videos', $file['filename']);

        if($putObject)
        {

            //--- Update file
            $this->db->where('id', $fileid);
            $this->db->update('video_files', array('s3'=>1));

            //--- Unlink main file
            unlink("./media/films/{$file['filename']}");

            //--- Send email to administrator
            $this->system_vars->omail($settings['admin_email'], "s3_upload", $file);

        }
        else
        {

            mail("rckehoe@gmail.com","failed","video failed to upload to s3","from:info@owws.com");

        }

    }

}
