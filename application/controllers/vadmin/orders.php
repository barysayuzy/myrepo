<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class orders extends CI_Controller
	{
	
		function __construct()
		{
		
			parent::__construct();
			
			$administrator = $this->session->userdata('admin_is_logged');
			
			// check for superadmin only
			if(!$this->session->userdata('admin_is_logged'))
			{
				redirect('/vadmin/login');
				exit;
			}
			
			$this->results_per_page = 100;
			$this->response = null;
			$this->error = null;
			$this->admin = $this->session->userdata('admin_is_logged');
			$this->open_nav = null;
			
		}
		
		function index()
		{
		
			# get date range for last 24 hours
			$from_date = date("Y-m-d H:i:s", strtotime("-24 hours"));
			$to_date = date("Y-m-d H:i:s");
		
			# perform search
			$this->search($from_date, $to_date);
		
		}
		
		function upload_tracking_numbers()
		{
		
			$trackingArray = array();
			$row = 1;
			
			if(($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE)
			{
			   
			    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
			    {
			    
			    	if($row>1 && trim($data[0]) && trim($data[1]))
			    	{
			    
				    	$order_number = $data[0];
				    	$tracking_number = $data[1];
				    	
				    	//-----
				    	// Build an array of tracking numbers for each order
				    	// Comma seperated for multiple tracking numbers
				    	// Insert list into the orders_products table
				    	// redirect back to main screen so admin can send out shipping confirmation
				    	//-----
				    	
				    	$trackingArray[$order_number] = $tracking_number.",".$trackingArray[$order_number];
			        
			        }
			     
			    	$row++;
			        
			    }
			    
			    fclose($handle);
			    
			}
			
			//-----
			// Insert into DB
			//-----
			$totalAffected = 0;
			
			foreach($trackingArray as $order_id => $tracking_numbers)
			{
			
				$this->db->where('order_id', $order_id);
				$this->db->update('orders_products', array('tracking_numbers'=>$tracking_numbers));
				
				$totalAffected += $this->db->affected_rows();
			
			}
			
			$this->session->set_flashdata('response', "{$totalAffected} orders have been saved.");
			
			redirect($this->input->post('redirect'));
		
		}
		
		function delete($order_id = null)
		{
		
			# delete main order
			$this->db->where('id', $order_id);
			$this->db->delete('orders');
			
			# delete products from order
			$this->db->where('order_id', $order_id);
			$this->db->delete('orders_products');
			
			# delete vouchers from order
			$this->db->where('order_id', $order_id);
			$this->db->delete('orders_vouchers');
			
			# redirect
			redirect('/vadmin/orders');
		
		}
		
		function search($from_date = null, $to_date = null)
		{
		
			# remote @ symbol from date string
			$to = ($to_date ? $to_date : str_replace("@"," ",$this->input->post('to')));
			$from = ($from_date ? $from_date : str_replace("@"," ",$this->input->post('from')));
			
			# convert date strings to actual usable date
			$to_time = date("Y-m-d H:i:s", strtotime($to));
			$from_time = date("Y-m-d H:i:s", strtotime($from));
			
			# get orders
			$search_sql = "
			
			SELECT orders.*
			
			FROM orders, orders_products
			
			WHERE   (orders.order_date BETWEEN '{$from_time}' AND '{$to_time}') AND
					(orders_products.order_id = orders.id)
					".($this->input->post('deal_id') ? "AND orders_products.deal_id = {$this->input->post('deal_id')}" : "")."
					
			GROUP BY orders.id		
			";
			
			$getOrders = $this->db->query($search_sql);
			$totalOrders = $getOrders->num_rows();
			
			# create transfer variables
			$transfer['from_string'] = date("m/d/Y @ h:i a", strtotime($from_time));
			$transfer['to_string'] = date("m/d/Y @ h:i a", strtotime($to_time));
			$transfer['from'] = date("m/d/Y h:i a", strtotime($from_time));
			$transfer['to'] = date("m/d/Y h:i a", strtotime($to_time));
			$transfer['total_results'] = $totalOrders;
			$transfer['results'] = $getOrders->result_array();
			$transfer['deal_id'] = $this->input->post('deal_id');
		
			if(isset($_POST['download_csv']))
			{
			
				# compile CSV
				/*
					id
					date
					user_id
					deal_id
					qty
					attribute_value ??
					name
					address
					address2
					city
					state
					zip
					country
					transaction_id
					authorization_code
					charge_amount
					charge_indiv
					charity_id
					chatrity_title
					donation_amount
					credits_used
					coupon_code
					group_id
					status
				*/
				
				$csv_fields = array('id','date','user_id','deal_id','qty','attribute_value','name','address','address2','city','state','zip','country','transaction_id','authorization_code','charge_amount','charge_indiv','charity_id','charity_title','donation_amount','credits_used','shipping','coupon_code','status');
				$row_array = array();
				
				# get products
				foreach($transfer['results'] as $order)
				{
				
					$charity = array("id"=>"","title"=>"");
								
					if($order['charity_id'])
					{
					
						$getCharity = $this->db->query("SELECT * from charities WHERE id = {$order['charity_id']} LIMIT 1");
						$charity = $getCharity->row_array();
						
						$charity = array("id"=>$charity['id'], "title"=>$charity['title']);
					
					}
					
					// Loop through products
					$getProducts = $this->db->query("SELECT * FROM orders_products WHERE order_id = {$order['id']} ");
					$customer = $this->dailydeals->get_customer($order['user_id']);
					
					foreach($getProducts->result_array() as $product)
					{
					
						$deal = $this->dailydeals->get_deal($product['deal_id']);
						
						if($deal['deal_type']=='Physical Product')
						{
						
							$version = ($product['deal_version_id'] > 0 ? $this->dailydeals->get_deal_version($product['deal_version_id']) : FALSE);
							
							$deal_price = (isset($version['price']) ? $version['price'] : $deal['sale_price']);
							$deal_title = (isset($version['title']) ? $version['title'] : $deal['title']);
							
							$row_array[] = array
							(
								'id'=>$order['id'],
								'date'=>$order['order_date'],
								'user_id'=>$order['user_id'],
								'deal_id'=>$deal['id'],
								'qty'=>$product['qty'],
								'attribute_value'=>'',
								'name'=>$product['shipping_name'],
								'address'=>$product['shipping_address'],
								'address2'=>$product['shipping_address2'],
								'city'=>$product['shipping_city'],
								'state'=>$product['shipping_state'],
								'zip'=>$product['shipping_zip'],
								'country'=>$product['shipping_country'],
								'transaction_id'=>$order['transaction_id'],
								'authorization_code'=>$order['authorization_code'],
								'charge_amount'=>$order['total_order_charge'],
								'pre_total'=>$product['pre_total'],
								'charity_id'=>$charity['id'],
								'charity_title'=>$charity['title'],
								'donation_amount'=>$order['total_donation'],
								'credits_used'=>$order['total_credits'],
								'shipping'=>$order['total_shipping'],
								'coupon_code'=>"",
								'status'=>(isset($product['shipping_date'])&&$product['shipping_date'] ? "Shipped" : "Pending")
							);	
						
						}
					
					}
					
					// Loop through vouchers
					$getVoucher = $this->db->query("SELECT * FROM orders_vouchers WHERE order_id = {$order['id']} ");
					
					foreach($getVoucher->result_array() as $voucher)
					{
					
						// Check if this deal version is a voucher deal, if not… don't do this.. it will create duplicates
						
						$deal = $this->dailydeals->get_deal($voucher['deal_id']);
						
						if($deal['deal_type']!='Physical Product')
						{
						
							$version = ($voucher['deal_version_id'] > 0 ? $this->dailydeals->get_deal_version($voucher['deal_version_id']) : FALSE);
							
							$deal_price = (isset($version['price']) ? $version['price'] : $deal['sale_price']);
							$deal_title = (isset($version['title']) ? $version['title'] : $deal['title']);
							
							$row_array[] = array
							(
								'id'=>$order['id'],
								'date'=>$order['order_date'],
								'user_id'=>$order['user_id'],
								'deal_id'=>$deal['id'],
								'qty'=>'1',
								'attribute_value'=>'',
								'name'=>$customer['first_name']." ".$customer['last_name'],
								'address'=>"",
								'address2'=>"",
								'city'=>"",
								'state'=>"",
								'zip'=>"",
								'country'=>"",
								'transaction_id'=>$order['transaction_id'],
								'authorization_code'=>$order['authorization_code'],
								'charge_amount'=>$order['total_order_charge'],
								'pre_total'=>$product['pre_total'],
								'charity_id'=>$charity['id'],
								'charity_title'=>$charity['title'],
								'donation_amount'=>$order['total_donation'],
								'credits_used'=>$order['total_credits'],
								'shipping'=>$order['total_shipping'],
								'coupon_code'=>$voucher['voucher_code'],
								'status'=>($voucher['used']=='1' ? "Used" : "Pending")
							);
						
						}
					
					}
				
				}
				
				# compile CSV
				$field_array[] = $csv_fields;
				$data = array_merge($field_array,$row_array);
				
				/*
				print_r($data);
				exit;
				*/
				
				header("Content-type: application/csv");
				header("Content-Disposition: attachment; filename=orders.csv");
				header("Pragma: no-cache");
				header("Expires: 0");
			
				// Compule Into CSV
				$fp = fopen('php://output', 'w');
	
				foreach ($data as $fields)
				{
				    fputcsv($fp, $fields);
				}
				
				fclose($fp);
			
			}
			else
			{
			
				# views
				$this->load->view('vadmin/header');
				$this->load->view('vadmin/modules/orders/main', $transfer);
				$this->load->view('vadmin/footer');
			
			}
		
		}
		
		function details($order_id = null)
		{
		
			# get order
			$getOrder = $this->db->query("SELECT * FROM orders WHERE id = {$order_id} LIMIT 1");
			$transfer = $getOrder->row_array();
			
			# get customer
			$transfer['user'] = $this->dailydeals->get_customer($transfer['user_id']);
			
			# get products
			$getOrderProducts = $this->db->query("SELECT * FROM orders_products WHERE order_id={$order_id} ");
			$transfer['products'] = $getOrderProducts->result_array();
			
			# views
			$this->load->view('vadmin/header');
			$this->load->view('vadmin/modules/orders/details', $transfer);
			$this->load->view('vadmin/footer');
		
		}
		
		function download_label($order_id = null)
		{
		
			$this->load->library('usps');
			
			# get order
			$getOrder = $this->db->query("SELECT members.*,orders.id as order_id FROM orders,members WHERE orders.id = {$order_id} AND members.id = orders.user_id GROUP BY orders.user_id LIMIT 1");
			$user = $getOrder->row_array();
			
			# get package info (for weight)
			$getOrderProducts = $this->db->query("SELECT id FROM orders_products WHERE order_id={$order_id} ");
			
			$weight = $getOrderProducts->num_rows()*0.5;
			$weight = ($weight < 1 ? 1 : $weight);
			
			$params['from_name'] = "Robert";
			$params['from_address1'] = "4940 Barcelona Way";
			$params['from_city'] = "Colorado Springs";
			$params['from_state'] = "CO";
			$params['from_zip'] = "80917";
			
			$params['to_name'] = $user['shipping_name'];
			$params['to_address1'] = $user['shipping_address'];
			$params['to_address2'] = $user['shipping_address2'];
			$params['to_city'] = $user['shipping_city'];
			$params['to_state'] = $user['shipping_state'];
			$params['to_zip'] = $user['shipping_zip'];
			
			$params['package_weight'] = $weight;
			
			$label_data = $this->usps->generate_label($params);
			
			$delivery_confirmation_number = $label_data['DeliveryConfirmationV3.0Response']['DeliveryConfirmationNumber'];
			$label_data = $label_data['DeliveryConfirmationV3.0Response']['DeliveryConfirmationLabel'];
			
			header("Content-type:application/pdf"); 
			// header("Content-Disposition:attachment;filename='usps_label_{$delivery_confirmation_number}.pdf'");
			
			echo base64_decode($label_data);
		
		}
		
		function send_shipping_confirmations($deal_id)
		{
		
			# get order
			$getOrders = $this->db->query("SELECT orders_products.*,orders.user_id FROM orders,orders_products WHERE orders_products.deal_id = {$deal_id} AND orders.id = orders_products.order_id");
			$orders = $getOrders->result_array();
			
			# Get Deal
			$getDeals = $this->db->query("SELECT * FROM deals WHERE id = '{$deal_id}' LIMIT 1");
			$deal = $getDeals->row_array();
			
			foreach($orders as $o)
			{
			
				$getUser = $this->db->query("SELECT * FROM members WHERE id = {$o['user_id']} LIMIT 1");
				$user = $getUser->row_array();
				
				if($o['tracking_numbers'])
				{
				
					$user['deal_title'] = $deal['title'];
					$user['tracking_numbers'] = str_replace(",","<br />", $o['tracking_numbers']);
					
					$this->system_vars->omail($user['email_address'],'shipping_confirmation',$user);
					
					$this->session->set_flashdata('response', "Shipping confirmations have been sent");
					
					redirect('/vadmin/main/edit_record/2/1/'.$deal_id);
				
				}
			
			}
		
		}
		
		function download_shipping_labels($deal_id)
		{
		
			set_time_limit(0);
		
			$this->load->library('zip');
			$this->load->library('usps');
			
			# get orders
			switch($this->input->post('version'))
			{
			
				case "all":
				
					$getOrders = $this->db->query("SELECT orders_products.*,orders.user_id FROM orders,orders_products WHERE orders_products.deal_id = {$deal_id} AND orders.id = orders_products.order_id");
					$orders = $getOrders->result_array();
				
				break;	
				
				default:
				
					$getOrders = $this->db->query("SELECT orders_products.*,orders.user_id FROM orders,orders_products WHERE orders_products.deal_id = {$deal_id} AND orders_products.deal_version_id = {$this->input->post('version')} AND orders.id = orders_products.order_id");
					$orders = $getOrders->result_array();
				
				break;
			
			}
			
			$totalError = 0;
			$erroredAddressesArray = array();
			
			if($getOrders->num_rows()==0) echo "There were no orders found";
			
			foreach($orders as $o)
			{
			
				if(!$o['tracking_numbers'])
				{
				
					$deal = $this->dailydeals->get_deal($o['deal_id']);
			
					$getUser = $this->db->query("SELECT * FROM members WHERE id = {$o['user_id']} LIMIT 1");
					$user = $getUser->row_array();
					
					// Generate shipping label
					$params = array();
					$params['to_name'] = $user['shipping_name'];
					$params['to_address1'] = $user['shipping_address'];
					$params['to_address2'] = $user['shipping_address2'];
					$params['to_city'] = $user['shipping_city'];
					$params['to_state'] = $user['shipping_state'];
					$params['to_zip'] = $user['shipping_zip'];
					
					$params['package_weight'] = $deal['weight'];
					
					// Generate 1 label for each QTY in this order
					$trackingNumbers = "";
					
					for ($i = 1; $i <= $o['qty']; $i++)
					{
					
						$response_data = $this->usps->generate_label($params);
					
						$delivery_confirmation_number = $response_data['DeliveryConfirmationV3.0Response']['DeliveryConfirmationNumber'];
						$label_data = $response_data['DeliveryConfirmationV3.0Response']['DeliveryConfirmationLabel'];
						
						if($delivery_confirmation_number)
						{
						
							$filename = "{$delivery_confirmation_number}.pdf";
						
							$labelData = base64_decode($label_data);
							file_put_contents("./media/shipping_labels/{$filename}",$labelData);
							
							$trackingNumbers .= "{$delivery_confirmation_number},";
							$shippingError = "";
							
							// Add data to archive
							$this->zip->add_data($filename, $labelData);
						
						}
						else
						{
						
							$totalError++;
							
							
							
							$trackingNumbers = "";
							$shippingError = $response_data['Error']['Description'];
							
							/*
							echo "<pre>";
							print_r($response_data);
							echo "</pre>";
							*/
							
							$params['user_id'] = $o['user_id'];
							$params['shipping_error'] = $shippingError;
							$erroredAddressesArray[] = $params;
						
						}
					
					}
					
					// Update order
					$updateArray = array();
					$updateArray['tracking_numbers'] = $trackingNumbers;
					$updateArray['shipping_error_notes'] = $shippingError;
					
					$this->db->where('id', $o['id']);
					$this->db->update('orders_products', $updateArray);
					
				}
				else
				{
				
					// Get all files for each of the tracking numbrs
					
					$trackingArray = explode(",",$o['tracking_numbers']);
					
					foreach($trackingArray as $tn)
					{
					
						if(trim($tn))
						{
						
							$readFile = file_get_contents("./media/shipping_labels/{$tn}.pdf");
							
							$this->zip->add_data("{$tn}.pdf", $readFile);
						
						}
					
					}
				
				}
			
			}
			
			if($totalError > 0)
			{
			
				echo "There were errors with {$totalError} addresses. Shipping labels did not print for errored addresses. Check billing logs.";
			
				echo "<pre>";
				
				print_r($erroredAddressesArray);
			
			}
			else
			{
			
				$this->zip->download("deal_{$deal_id}_shipping_labels.zip");
			
			}
		
		}
		
	}
	