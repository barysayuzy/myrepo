<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class payments extends CI_Controller
	{
	
		function __construct()
		{
		
			parent::__construct();
			
			$administrator = $this->session->userdata('admin_is_logged');
			
			// check for superadmin only
			if(!$this->session->userdata('admin_is_logged'))
			{
				redirect('/vadmin/login');
				exit;
			}
			
			$this->response = null;
			$this->error = null;
			$this->admin = $this->session->userdata('admin_is_logged');
			$this->open_nav = null;
			
		}
		
		function index()
		{
		
			$d = date("Y-m-d H:i:s");
			
			$getTransactions = $this->db->query
			("
			
				SELECT
					m.*,
					(
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='deposit' AND member_id = m.id AND dispute=0 AND processed=0 ), 0) - 
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='withdrawal' AND member_id = m.id AND dispute=0 AND processed=0 ),0) 
					) as balance
					
				FROM
					members m
					
				WHERE
					m.id != 2 AND
					(
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='deposit' AND member_id = m.id AND dispute=0 AND processed=0 ), 0) - 
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='withdrawal' AND member_id = m.id AND dispute=0 AND processed=0 ),0) 
					) > 0 AND
					m.paypal != \"\"
					
			");
			
			$t['transactions'] = $getTransactions->result_array();
		
			$this->load->view('vadmin/header');
			$this->load->view('vadmin/payments', $t);
			$this->load->view('vadmin/footer');
		
		}
		
		function init_payments()
		{
		
			// PayPal Post Data
			//$post_url = "https://api-3t.sandbox.paypal.com/nvp";
			$post_url = "https://api-3t.paypal.com/nvp";
			$post_string = "";
			
			// PayPal Account Credentials
			$send_arr['USER'] = 'felmadan_api1.berkeleyprep.org';
			$send_arr['PWD'] = 'VMLCUVEJUEKXPCNE';
			$send_arr['SIGNATURE'] = 'AcQjqvD5210GqX-VU4ELKqjza-5PAeXzTwlL4I1jYjjbWQtk5jyyAXjD';
			
			// PayPal MassPay Params
			$send_arr['VERSION'] = "51.0";
			$send_arr['METHOD'] = 'MassPay';
			$send_arr['RECEIVERTYPE'] = "EmailAddress";
			$send_arr['CURRENCYCODE']="USD";
			
			// List out the payees
			$d = date("Y-m-d H:i:s");
			
			$getTransactions = $this->db->query
			("
			
				SELECT
					m.*,
					(
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='deposit' AND member_id = m.id AND dispute=0 AND processed=0 ), 0) - 
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='withdrawal' AND member_id = m.id AND dispute=0 AND processed=0 ),0) 
					) as balance
					
				FROM
					members m
					
				WHERE
					m.id != 2 AND
					(
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='deposit' AND member_id = m.id AND dispute=0 AND processed=0 ), 0) - 
						COALESCE((SELECT SUM(amount) FROM transactions WHERE datetime <= '{$d}' AND type='withdrawal' AND member_id = m.id AND dispute=0 AND processed=0 ),0) 
					) > 0 AND
					m.paypal != \"\"
					
			");
			
			$csl = "";
			
			foreach($getTransactions->result_array() as $i=>$user)
			{
			
				if(trim($user['paypal']) && $user['balance'] > 0)
				{
			
					$send_arr['L_EMAIL'.$i] = $user['paypal'];
					$send_arr['L_AMT'.$i] = number_format($user['balance'], 2);
					
					// Update transaction
					$csl .= $user['id'].",";
					
				}
			
			}
			
			$csl = rtrim($csl, ",");
			
			// Loopo through and convert string to URL encoded string
			foreach($send_arr as $key => $value )
			{
			
				$post_string .= "$key=" . urlencode( $value ) . "&";
			
			}
			
			$post_string = rtrim($post_string, "& ");
			
			$request = curl_init($post_url); // initiate curl object
			
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
			$post_response = curl_exec($request);
			curl_close ($request);
			
			$rawr = explode("&", $post_response);
			$array = array();
			
			foreach($rawr as $string)
			{
			
				list($name,$value)=explode("=", $string);
				$array[trim(strtolower($name))] = trim(urldecode($value));
			
			}
			
			if($array['ack'] == 'Success')
			{
			
				// Send out emails
				foreach($getTransactions->result_array() as $i=>$user)
				{
				
					if(trim($user['paypal']) && $user['balance'] > 0)
					{
					
						$params = $user;
						$params['balance'] = number_format($user['balance']);
						$this->system_vars->omail($user['email'],'seller-payment',$params);
					
					}
				
				}
				
				// Update transactions
				$this->db->query("UPDATE transactions SET processed = 1 WHERE member_id IN ({$csl})");
				
				redirect("/vadmin/payments");
			
			}
			else
			{
			
				echo "<h1 style='color:red;'>Payment Failed!!</h1>";
				
				echo "<pre>";
				print_r($array);
				echo "</pre>";
			
			}

		}
		
	}
	