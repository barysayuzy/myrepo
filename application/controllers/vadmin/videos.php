<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class videos extends CI_Controller
{

    function __construct()
    {

        parent::__construct();

        $administrator = $this->session->userdata('admin_is_logged');

        // check for superadmin only
        if(!$this->session->userdata('admin_is_logged'))
        {
            redirect('/vadmin/login');
            exit;
        }

        $this->results_per_page = 100;
        $this->response = null;
        $this->error = null;
        $this->admin = $this->session->userdata('admin_is_logged');
        $this->open_nav = null;

    }

    function index()
    {

        $this->load->view('vadmin/header');
        $this->load->view('vadmin/videos/main');
        $this->load->view('vadmin/footer');

    }

}
