<?php

class MY_Loader extends CI_Loader
{
    var $template_title;
	public function ci_autoloader()
	{
		parent::ci_autoloader();

		$CI =& get_instance();

		// Populate some default global template variables
		$this->vars(array(
			'member_logged' => $CI->session->userdata('member_logged'),
			'body_class'	=> 'page',
		));
	}

	public function view($view, $vars = array(), $return = FALSE)
	{
		// Populate some special variables just for the header
		if ($view == 'header')
		{
			$CI =& get_instance();

			// Errors
			(isset($vars['errors']) and is_array($vars['errors'])) or $vars['errors'] = array();
			($e = validation_errors())				and $vars['errors'][] = '<p>'.$e.'</p>';
			($e = $CI->session->flashdata('error'))	and $vars['errors'][] = '<p>'.$e.'</p>';
			isset($CI->error)						and $vars['errors'][] = '<p>'.$CI->error.'</p>';

			// Response message
			$vars['message'] = $CI->session->flashdata('response');

			// Page Meta
			$vars['meta'] = $CI->system_vars->meta_tags($CI->uri->uri_string());
			empty($vars['meta']['title']) and $vars['meta'] = $CI->system_vars->meta_tags();
		}

		return parent::view($view, $vars, $return);
	}

	public function template($view, $vars = array(), $return = FALSE)
	{

		$content  = $this->view('header', $vars, $return);
		$content .= $this->view($view, $vars, $return);
		$content .= $this->view('footer', $vars, $return);

		return $content;
	}
}