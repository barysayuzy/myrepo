<?

	class bool
	{
	
		function config($name, $value, $params = null)
	    {
	    
	        $this->ci =& get_instance();
	        
	        $this->name = $name;
			$this->value = $value;
	        
	    }
		
		function field_view()
		{
		
			$returnString = "

				<input type='checkbox' name='{$this->name}' value='1' ".($this->value ? " checked" : "")." class='tb'> Yes
			
			";
		
			return $returnString;
		
		}
		
		function display_view()
		{
		
			if($this->value) return "Yes";
			else return "";
		
		}
		
		function process_form()
		{
			
			return $this->value;
			
		}
	
	}

?>