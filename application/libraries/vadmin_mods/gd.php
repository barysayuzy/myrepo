<?
class gd 
{

	//NEW
	function config($name, $value, $params = null)
	{
	    
        $this->ci =& get_instance();

        $this->name = $name;
        $this->value = $value;
        $this->Width = $params[1];
        $this->Height =$params[2];

        $this->upload_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/";
        $this->upload_url = "/assets/uploads/";

	}
	//NEW
	function field_view()
	{
	
		$return = "";
	
		// Show Image If One Has Been Uploaded
		if($this->value)
		{
		
			$return .= "
			<div style='margin-bottom:20px;'>
				<div><img src='{$this->upload_url}{$this->value}' border='0' class='img img-polaroid' width='200' /></div>
				<div align='left'><input type='checkbox' name='{$this->name}_delete' value='yes'> Delete Current Image</div>
			</div>";
		
		}
		
		// Show an upload form
		$return .= "
		<input type='file' name='{$this->name}' class='tb' >
		<input type='hidden' name='{$this->name}_hidden' >
		<div>Image will be resized to {$this->Width} x {$this->Height}</div>
		";
		
		return $return;
	
	}

	function display_view()
	{
		return "<img src='{$this->upload_url}{$this->value}' border='0' class='img img-polaroid' width='50' />";
	}
	
	function process_form()
	{
		$checktodelete = true;
		$deletedold = false;

	    if($this->ci->input->post($this->name.'_delete')=='yes')
		{

			unlink($this->upload_path . $this->ci->input->post($this->name.'_hidden'));
			$checktodelete = false;
			$deletedold = true;


	    }

		if($_FILES[$this->name]['tmp_name'])
		{

		    $CI =& get_instance();

		    $ImageExtension = trim(str_replace(".","", substr($_FILES[$this->name]['name'], -4,4)));
		    $NewImagename = rand(1000000000,9999999999)."_".time().".".$ImageExtension;

		    $config['image_library'] = 'gd2';
		    $config['source_image'] = $_FILES[$this->name]['tmp_name'];
		    $config['new_image'] = $this->upload_path . $NewImagename;
		    $config['maintain_ratio'] = FALSE;
		    $config['width'] = $this->Width;
		    $config['height'] = $this->Height;

		    $CI->load->library('image_lib', $config);
		    
		    if($CI->image_lib->resize())
		    {
				return $NewImagename;
		    }
		    else
		    {
				return "[%skip%]";		    
			}

		}
		else
		{
			if($deletedold) return "";
			else return "[%skip%]";
		}
	}
}

?>