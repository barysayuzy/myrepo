<?

class s3ref
{

    function __construct()
    {

        $this->ci =& get_instance();

        $this->ci->load->library('s3');
        $this->s3FileArray = $this->ci->s3->getBucket('storybox-videos');

    }

    function config($name, $value, $params = null)
    {

        // Define RF Configuration
        $this->name = $name;
        $this->value = $value;

    }

    function field_view()
    {

        $return = "<select name='{$this->name}' class='chzn-select'>";

        foreach($this->s3FileArray as $key=>$array)
        {

            $return .= "<option value='{$key}'".($this->value==$key ? " selected" : "").">{$key}</option>";

        }

        $return .= "</select>";

        return $return;

    }

    function display_view()
    {

        return $this->value;

    }

    function process_form()
    {

        return $this->value;

    }

}

?>