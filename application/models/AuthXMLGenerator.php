<?php
  /**
   * AuthXMLGenerator implements method to generate and sign authentication XML.
   *
   * @author Roman
   * @version 10.23.2012 1815
   */
  class authxmlgenerator extends CI_Model
  {

    private $_error = '';
    private $_kid_list = '';
    private $_kid = '';
    private $_exp_time = '';
    private $_external_id = '';
    private $_ip = '';
    private $_date_format = 'Y-m-d H:i:s.000';
    private $_policies = array();
    private $_play_enablers = array();
    private $_opl = array();
    private $_play_rights = array();
    private $_is_persistent = false;
    private $_pem = '';
    private $_custom_nodes = array();

    function pem($pem_key) {
      if (!file_exists($pem_key))
        $this->_throw('PEM file (' . $pem_key . ') doesn\'t exist. Plese ensure that you have passed valid file\'s location and/or filename.');

      $this->_pem = $pem_key;
    }

    public function addCustomNodes($data) {
      if (!is_array($data))
        $this->_throw('Invalid parameter type. Custom fields needs to be presented inside the associated array.');

      foreach ($data as $key => $value) {
        $this->_custom_nodes[$key] = $value;
      }
    }

    public function getCustomNodes() {
      return $this->_custom_nodes;
    }

    /**
     * AuthXMLGenerator::addPlayEnablers() - it may accept one string GUID or array of string GUIDs
     *
     * Example of usage:
     * addPlayEnablers('_PLAYENABLER_GUID_') or addPlayEnablers(array('_PLAYENABLER_GUID_#1', '_#2', '_#3'))
     *
     * @param mixed $guid_list (string or array of strings)
     * @return void
     */
    public function addPlayEnablers ($guid_list) {
      if (!is_array($guid_list)) $guid_list = array($guid_list);

      if (!KeyOSAuthXML_PlayEnablers::validateList($guid_list, $errmsg))
        $this->_throw($errmsg);

      foreach ($guid_list as $guid)
        $this->_play_enablers[] = strtoupper($guid);

      if (!KeyOSAuthXML_PlayEnablers::validateList($this->_play_enablers, $errmsg))
        $this->_throw($errmsg);
    }

    /**
     * AuthXMLGenerator::setPlayEnablers() - it may accept one string GUID or array of string GUIDs
     *
     * Example of usage:
     * setPlayEnablers('_PLAYENABLER_GUID_') or setPlayEnablers(array('_PLAYENABLER_GUID_#1', '_#2', '_#3'))
     *
     * Note: it replaces all GUIDs added before.
     *
     * @param mixed $codes (string or array of strings)
     * @return void
     */
    public function setPlayEnablers ($guid_list) {
      # Remove PlayEnablers if some exist
      $this->_play_enablers = array();

      # Add PlayEnablers if valid
      $this->addPlayEnablers($guid_list);
    }

    /**
     * AuthXMLGenerator::getPlayEnablers() Gets array of GUIDs
     *
     * @return array
     */
    public function getPlayEnablers () {
      return $this->_play_enablers;
    }

    /**
     * AuthXMLGenerator::setOPL() Sets Output Protection Levels
     *
     * @param mixed $opl
     * @param mixed $data
     * @return void
     */
    public function setOPL($opl, $data) {
      if (KeyOSAuthXML_OPL::exists($opl) === false)
        $this->_throw('Invalid output protection level passed');

      if (KeyOSAuthXML_OPL::validateOPL($opl, $data) === false)
        $this->_throw('Invalid `' . $opl . '` level value passed');

      if ($opl == KeyOSAuthXML_OPL::ANALOG_VIDEO_EXPLICIT || $opl == KeyOSAuthXML_OPL::DIGITAL_AUDIO_EXPLICIT) {
        // Explicit output protections may have multiple protection IDs set for them.
        foreach ($data as $key => $value) {
          $key = strtoupper($key);
          $this->_opl[$opl][$key] = $value;
        }
      }
      else {
        // OPL level can be only one.
        $this->_opl[$opl] = $data;
      }
    }

    /**
     * AuthXMLGenerator::setPolicy() Sets policy
     *
     * @param mixed $policy
     * @param mixed $data
     * @return void
     */
    public function setPolicy($policy, $data) {
      if (!KeyOSAuthXML_Policies::validate($policy, $data, $errmsg))
        $this->_throw($errmsg);

      $this->_policies[$policy] = $data;
    }

    /**
     * AuthXMLGenerator::isPersistent() Gets or sets license persistent status.
     *
     * @param status $bool
     * @return void
     */
    public function isPersistent($bool) {
      if ($bool !== true && $bool !== false)
        $this->_throw('\'isPersistent\' function takes bool parameter only.');

      $this->_is_persistent = $bool;
    }

    /**
     * AuthXMLGenerator::getError() Gets error message.
     *
     * @return string
     */
    public function getError() {
      return $this->_error;
    }

    /**
     * AuthXMLGenerator::setIP() Sets IP.
     *
     * @param string $ip
     * @return void
     */
    public function setIP($ip) {
      $this->_ip = $ip;
    }

    public function setKID($data) {
      if (is_array($data)) {
        if (count($data) <= 0)
          $this->_throw('KID list can not be empty.');

        foreach($data as $k => $kid) {
          if ($this->_checkGUID($kid))
            $data[$k] = $this->convertGUIDToBase64($kid);

          $tmp = base64_decode($data[$k]);

          if (!$tmp)
            $this->_throw($kid.' is invalid KID.');
        }

        $this->_kid_list = $data;
      }
      else {
        $kid = $data;

        if ($this->_checkGUID($data))
          $data = $this->convertGUIDToBase64($data);

        $tmp = base64_decode($data);

        if (!$tmp)
          $this->_throw($kid . ' is invalid KID.');

        $this->_kid = $data;
      }
    }

    /**
     * AuthXMLGenerator::setExpirationTime() Sets expiration time.
     *
     * @param unix timestamp $mktime
     */
    public function setExpirationTime($time) {
      if (preg_match('/^(?:[1-2]\d{3})\-(?:0[0-9]{1}|1[0-2]{1})\-(?:0[0-9]{1}|1[0-9]{1}|2\d{1}|3[0-1]{1}) (?:0[0-9]{1}|1[0-9]{1}|2[0-3]{1})\:(?:[0-5][0-9])\:(?:[0-5][0-9])(?:\.\d{3})?$/', $time) !== 1)
        $this->_throw('Invalid \'ExpirationTime\' set.');

      $this->_exp_time = $time;
    }

    /**
     * AuthXMLGenerator::setExternalID() Sets an external ID.
     *
     * @param string $id
     */
    public function setExternalID($id) {
      if (!preg_match('/^[a-zA-Z0-9_ -]+$/', $id))
        $this->_throw('External ID contains invalid charachters.');

      $this->_external_id = $id;
    }

    /**
     * AuthXMLGenerator::generateAuthenticationXML() Signs authentication XML with passed private key.
     *
     * @return string
     */
    public function generateAuthenticationXML() {
      try {
        $root = new SimpleXMLElement('<KeyOSAuthenticationXML/>');
        $data_element = $root->addChild('Data');

        if (count($this->_policies) <=0 && $this->_is_persistent) {
          $policy = $data_element->addChild('Policy');
          $policy->addAttribute('persistent', 'true');
        }
        else if (count($this->_policies) > 0) {
          $policy = $data_element->addChild('Policy');

          if ($this->_is_persistent === true)
            $policy->addAttribute('persistent', 'true');

          foreach($this->_policies as $key => $value)
            $policy->addChild($key, $value);
        }

        if (count($this->_opl) > 0 || count($this->_play_enablers) > 0) {
          $play = $data_element->addChild('Play');

          if (count($this->_opl) > 0) {
            $output_protections = $play->addChild('OutputProtections');
            $opl = $output_protections->addChild('OPL');

            $expl_rights = KeyOSAuthXML_OPL::getExplicitOPLRights();
            $opl_rights = KeyOSAuthXML_OPL::getOPLRights();

            foreach($this->_opl as $key => $value) {
              if (in_array($key, $opl_rights)) {
                $opl->addChild($key, $value);
              }
              else if (in_array($key, $expl_rights)) {
                // Explicit output protections may have multiple IDs set for them.
                $expl_id_node = $output_protections->addChild($key);

                foreach($value as $expl_id => $expl_value) {
                  $expl_id_node->addChild('Id', $expl_id)->addAttribute('ConfigData', $expl_value);
                }
              }
            }
          }

          if (count($this->_play_enablers) > 0) {
            $play_enablers = $play->addChild('Enablers');

            foreach($this->_play_enablers as $guid) {
              $play_enablers->addChild('Id', $guid);
            }
          }
        }

        if (count($this->_custom_nodes) > 0) {
          $custom_nodes = $data_element->addChild('CustomNodes');

          foreach ($this->_custom_nodes as $key => $value) { 
            $custom_nodes->addChild($key, $value);
          }
        }

        $data_element->addChild('GenerationTime', gmdate($this->_date_format));
        $data_element->addChild('ExpirationTime', $this->_exp_time);
        $data_element->addChild('UniqueId', $this->_generateGUID());

        if ($this->_ip != '')
          $data_element->addChild('IP', $this->_ip);

        if ($this->_kid_list != '') {
          $kid_list = $data_element->addChild('KeyIDList');

          foreach($this->_kid_list as $kid)
            $kid_list->addChild('KeyID', $kid);
        }
        else if ($this->_kid != '')
          $data_element->addChild('KeyID', $this->_kid);

        if ($this->_external_id != '')
          $data_element->addChild('ExternalID', $this->_external_id);
      }
      catch(Exception $e) {
        $this->_throw('Error during signature creation. Error: ' . $e->getMessage());
      }

      return $root->saveXML();
    }

    /**
     * AuthXMLGenerator::signAuthenticationXML() Signs authentication XML.
     *
     * @param string $xml
     * @return string
     */
    public function signAuthenticationXML($xml) {
      try {
        $resource = fopen($this->_pem, 'r');

        if (!$resource)
          $this->_throw('Unable to open ' . $this->_pem . ' for reading.');
      }
      catch(Exception $e) {
        $this->_throw('Unable to open ' . $this->_pem . ' Error: ' . $e->getMessage());
      }

      try {
        $filesize = filesize($this->_pem);

        if ($filesize <= 0)
          throw exception($this->_pem . ' is empty.');

        $certificate_contents = fread($resource, filesize($this->_pem));

        if (!$certificate_contents)
          $this->_throw('Unable to read ' . $this->_pem);

        fclose($resource);
      }
      catch(Exception $e) {
        $this->_throw('Unable to read ' . $this->_pem . '. Error: ' . $e->getMessage());
      }

      $root = simplexml_load_string($xml);
      $data_element = $root->Data;

      # Add element describing public key.
      $info = pathinfo($this->_pem);

      $data_element->addChild('RSAPubKeyId', $info['filename']);

      # Fetch private key from the certificate.
      $pkeid = openssl_get_privatekey($certificate_contents);

      if (!$pkeid)
        $this->_throw('Unable to get private key from the certificate.');

      # Generate signature based on the private key from the certificate and data we've got
      # from the input file.
      if (!openssl_sign($data_element->asXML(), $signature, $pkeid))
        $this->_throw('Unable to sign data with private key.');

      $root->addChild('Signature', base64_encode($signature));

      # Remove private key from the memory.
      openssl_free_key($pkeid);

      return $root->saveXML();
    }

    /**
     * AuthXMLGenerator::encodeAuthenticationXML() Encodes authentication data and prepares it for usage in SL player.
     *
     * @param string $xml
     * @return string
     */
    public function encodeAuthenticationXML($xml) {
      return base64_encode($xml);
    }

    /**
     * AuthXMLGenerator::_generateGUID() Generates GUID removing {} and - symbols.
     *
     * @return string
     */
    private function _generateGUID() {
      if (function_exists('com_create_guid'))
        return strtolower(preg_replace('/[{}-]/', '', com_create_guid()));

      # optional for php 4.2.0 and up.
      mt_srand((double)microtime() * 10000);

      $charid = strtolower(md5(uniqid(rand(), true)));

      $uuid = substr($charid, 0, 8).substr($charid, 8, 4).substr($charid, 12, 4).substr($charid, 16, 4).substr($charid, 20,12);

      return $uuid;
    }

    /**
     * AuthXMLGenerator::_checkGUID() Checks if GUID is valid.
     *
     * @return bool
     */
    private function _checkGUID($guid) {
      return preg_match('/^\{?[A-F\d]{8}-[A-F\d]{4}-[A-F\d]{4}-[A-F\d]{4}-[A-F\d]{12}\}?$/i', $guid) === 1;
    }

    /**
     * AuthXMLGenerator::convertGUIDToBase64() Converts and encodes GUID into Base64.
     *
     * @return string
     */
    public function convertGUIDToBase64($guid) {
      $guid_b64 = '';
      $guid_parts = explode('-', $guid);

      foreach ($guid_parts as $k => $part) {
        if ($k < 3)
          $part = join('', array_reverse(str_split($part, 2)));

        $guid_b64 .= pack('H*', $part);
      }

      return base64_encode($guid_b64);
    }

    /**
     * AuthXMLGenerator::_throw() Throws an exception and sets internal error.
     *
     * @param mixed $error
     * @return
     */
    protected function _throw($error) {
      $this->_error = $error;

      throw new Exception($error);
    }

    /**
     * AuthXMLGenerator::convertToGMT() Converts date into the prefered time zone.
     *
     * @param mixed $local_date
     * @param mixed $local_tz
     * @return
     */
    public function convertToGMT($local_date, $local_tz = 'GMT') {
      # Remove this if you want to use this method.
      throw new Exception('This method is deprecated, please use getUTC() instead.');

      if (is_numeric($local_date))
        $local_date = date($this->_date_format, $local_date);

      $system_timezone = date_default_timezone_get();

      date_default_timezone_set($local_tz);
      $local = date($this->_date_format);

      date_default_timezone_set('GMT');
      $gmt = date($this->_date_format);

      date_default_timezone_set($system_timezone);
      $diff = (strtotime($gmt) - strtotime($local));

      $date = new DateTime($local_date);
      $date->modify("+$diff seconds");
      $timestamp = $date->format($this->_date_format);

      return $timestamp;
    }

    /**
     * AuthXMLGenerator::getUTC() Returns/converts date into the GMT/UTC.
     *
     * @param mixed $str_time (string time or time in milleseconds)
     * @return
     */
    public function getUTC($str_time = 'now') {
      $time = is_numeric($str_time) ? $str_time : strtotime($str_time);
      return date($this->_date_format, $time - date('Z'));
    }
  }

  class KeyOSAuthXML_OPL {
    const COMPRESSED_DIGITAL_AUDIO = 'CompressedDigitalAudio';
    const UNCOMPRESSED_DIGITAL_AUDIO = 'UncompressedDigitalAudio';
    const COMPRESSED_DIGITAL_VIDEO = 'CompressedDigitalVideo';
    const UNCOMPRESSED_DIGITAL_VIDEO = 'UncompressedDigitalVideo';
    const ANALOG_VIDEO = 'AnalogVideo';

    const ANALOG_VIDEO_EXPLICIT = 'AnalogVideoExplicit';
    const DIGITAL_AUDIO_EXPLICIT = 'DigitalAudioExplicit';

    private static $_opl = array('CompressedDigitalAudio', 'UncompressedDigitalAudio', 'CompressedDigitalVideo', 'UncompressedDigitalVideo', 'AnalogVideo', 'AnalogVideoExplicit', 'DigitalAudioExplicit');

    // Allowsed analog video explicit policies and their values
    private static $_allowed_ave = array('C3FD11C6-F8B7-4D20-B008-1DB17D61F2DA' => array(0, 1, 2, 3), 
                                         '2098DE8D-7DDD-4BAB-96C6-32EBB6FABEA3' => array(0, 1, 2, 3), 
                                         '811C5110-46C8-4C6E-8163-C0482A15D47E' => array(520000), 
                                         'D783A191-E083-4BAF-B2DA-E69F910B3772' => array(520000));

    // Allowsed digital audio explicit policies and their values
    private static $_allowed_dae = array('6D5CFA59-C250-4426-930E-FAC72C8FCFA6' => array('00', '01', '10', '11'));

    public static function exists($opl) {
      return in_array($opl, self::$_opl);
    }

    public static function getExplicitOPLRights() {
      return array(KeyOSAuthXML_OPL::ANALOG_VIDEO_EXPLICIT, KeyOSAuthXML_OPL::DIGITAL_AUDIO_EXPLICIT);
    }

    public static function getOPLRights() {
      return array(KeyOSAuthXML_OPL::COMPRESSED_DIGITAL_AUDIO, KeyOSAuthXML_OPL::UNCOMPRESSED_DIGITAL_AUDIO, KeyOSAuthXML_OPL::COMPRESSED_DIGITAL_VIDEO, KeyOSAuthXML_OPL::UNCOMPRESSED_DIGITAL_VIDEO, KeyOSAuthXML_OPL::ANALOG_VIDEO);
    }

    public static function validateOPL($opl, $data) {
      switch ($opl) {
        case KeyOSAuthXML_OPL::COMPRESSED_DIGITAL_AUDIO:
          return in_array($data, array(100, 150, 200, 250, 300));
          break;
        case KeyOSAuthXML_OPL::UNCOMPRESSED_DIGITAL_AUDIO:
          return in_array($data, array(100, 150, 200, 250, 300));
          break;
        case KeyOSAuthXML_OPL::COMPRESSED_DIGITAL_VIDEO:
          return in_array($data, array(400, 500));
          break;
        case KeyOSAuthXML_OPL::UNCOMPRESSED_DIGITAL_VIDEO:
          return in_array($data, array(100, 250, 270, 300));
          break;
        case KeyOSAuthXML_OPL::ANALOG_VIDEO:
          return in_array($data, array(100, 150, 200));
          break;
        case KeyOSAuthXML_OPL::ANALOG_VIDEO_EXPLICIT:
          foreach ($data as $key => $value) {
            $key = strtoupper($key);

            if (!isset(self::$_allowed_ave[$key]))
              return false;
            
            if (!in_array($value, self::$_allowed_ave[$key]))
              return false;
          }
          return true;
          break;
        case KeyOSAuthXML_OPL::DIGITAL_AUDIO_EXPLICIT:
          foreach ($data as $key => $value) {
            $key = strtoupper($key);

            if (!isset(self::$_allowed_dae[$key]))
              return false;
            
            if (!in_array($value, self::$_allowed_dae[$key]))
              return false;
          }
          return true;
          break;
      }
    }
  }

  class KeyOSAuthXML_Policies {
    const BEGIN_DATE = 'BeginDate';
    const EXPIRATION_DATE = 'ExpirationDate';
    const EXPIRATION_AFTER_FIRST_PLAY = 'ExpirationAfterFirstPlay';
    const MINIMUM_SECURITY_LEVEL = 'MinimumSecurityLevel';

    private static $_policies = array('BeginDate'=>'', 'ExpirationDate'=>'', 'ExpirationAfterFirstPlay'=>'', 'MinimumSecurityLevel'=>'');

    public static function exists($policy, &$errmsg = null) {
      if (isset(self::$_policies[$policy])) return true;

      $errmsg = 'Policy \'' . $policy . '\' does not exist.';
      return false;
    }

    public static function validate($policy, $data, &$errmsg = null) {
      if (!self::exists($policy, $errmsg))
        return false;

      switch ($policy) {
        case self::BEGIN_DATE:
        case self::EXPIRATION_DATE:
          $fl_valid = preg_match('/^(?:[1-2]\d{3})\-(?:0[0-9]{1}|1[0-2]{1})\-(?:0[0-9]{1}|1[0-9]{1}|2\d{1}|3[0-1]{1}) (?:0[0-9]{1}|1[0-9]{1}|2[0-3]{1})\:(?:[0-5][0-9])\:(?:[0-5][0-9])(?:\.\d{3})?$/', $data) === 1;
          break;
        case self::EXPIRATION_AFTER_FIRST_PLAY:
          $fl_valid = preg_match('/^\d+$/', $data) === 1 || $data >= 0;
          break;
        case self::MINIMUM_SECURITY_LEVEL:
          $fl_valid = in_array($data, array(150, 2000));
        default:
          $fl_valid = true;
      }

      if (!$fl_valid) $errmsg = 'Policy \'' . $policy . '\' has invalid data/format.';

      return $fl_valid;
    }
  }

  class KeyOSAuthXML_PlayEnablers {
    private static $_list = array( # Key is GUID ID. Value array contains forbidden GUID IDs to use together with key's GUID ID.
      '786627D8-C2A6-44BE-8F88-08AE255B01A7' => array('B621D91F-EDCC-4035-8D4B-DC71760D43E9'),
      'B621D91F-EDCC-4035-8D4B-DC71760D43E9' => array('786627D8-C2A6-44BE-8F88-08AE255B01A7'),
      'D685030B-0F4F-43A6-BBAD-356F1EA0049A' => '',
      '002F9772-38A0-43E5-9F79-0F6361DCC62A' => ''
    );

    public static function validate($guid, &$errmsg = null) {
      $errmsg = isset(self::$_list[strtoupper($guid)]) ? null : 'PlayEnabler GUID ID does not exist: "' . $guid . '".';
      return empty($errmsg);
    }

    public static function validateList ($guid_list, &$errmsg = null) {
      if (!is_array($guid_list)) $guid_list = array($guid_list);

      if (count($guid_list) < 1) {
        $errmsg = 'No PlayEnablers passed';
        return false;
      }

      $guid_list2 = $guid_list;

      foreach ($guid_list as $guid) {
        if (!self::validate($guid, $errmsg)) return false;
        $guid = strtoupper($guid);

        if (is_array(self::$_list[$guid]) && count(self::$_list[$guid]) > 0)
          foreach ($guid_list2 as $guid2)
            if (in_array(strtoupper($guid2), self::$_list[$guid])) {
              $errmsg = 'The following PlayEnabler GUID IDs cannot be used together: ' . $guid . ' and ' . $guid2;
              return false;
            }
      }

      return true;
    }

    public static function getList () {
      return array_keys(self::$_list);
    }
  }
?>