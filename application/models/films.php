<?

    class Films extends CI_Model
    {

        //--- Store Film Object
        var $film = false;

        //--- Obtain Film Information
        //--- Including all related video files
        function fetch($film_id = null)
        {

            if(!$film_id)
            {
                return false;
            }
            else
            {
                $getFilm = $this->db->query
                ("
                    SELECT
                        films.*,
                        films.preview_file_id as preview_file,
                        films.sd_film_id as sd_file,
                        films.hd_film_id as hd_file,
                        (SELECT values.title FROM `values` WHERE values.id = films.value) as mainValue,
                        (SELECT educational_values.title FROM educational_values WHERE educational_values.id = films.educational_value) as mainEducationalValue

                    FROM
                        films

                    WHERE
                        films.id = {$film_id}

                    LIMIT 1

                ");

                if($getFilm->num_rows() == 0)
                {
                    return false;

                }
                else
                {
                    $this->film = $getFilm->row_array();
                    return $this;

                }

            }

        }

        //--- Get Film Associated Values
        function getValues()
        {

            if($this->film)
            {

                $getValues = $this->db->query
                ("
                    SELECT
                      values.id as id,
                      values.title as title

                    FROM
                      film_values

                    JOIN `values` ON values.id = film_values.value_id

                    WHERE
                      film_values.film_id = {$this->film['id']}

                    ORDER BY
                      values.title

                ");

                if($getValues->num_rows() > 0)
                {

                    return $getValues->result_array();

                }

            }

        }

        //--- Get Film Associated Values
        function getGenres()
        {

            if($this->film)
            {

                $getGenres = $this->db->query
                ("
                    SELECT
                      genres.id as id,
                      genres.title as title

                    FROM
                      film_genres

                    JOIN genres ON genres.id = film_genres.genre_id

                    WHERE
                      film_genres.film_id = {$this->film['id']}

                    ORDER BY
                      genres.sort, genres.title

                ");

                if($getGenres->num_rows() > 0)
                {

                    return $getGenres->result_array();

                }

            }

        }

        //--- Get Film Associated Educational Values
        function getEducationalValues()
        {

            if($this->film)
            {

                $getEducationalValues = $this->db->query
                    ("
                    SELECT
                      educational_values.id as id,
                      educational_values.title as title,
                      film_educational_values.description as description,
                      film_educational_values.title as post_title

                    FROM
                      film_educational_values

                    JOIN educational_values ON educational_values.id = film_educational_values.educational_value_id

                    WHERE
                      film_educational_values.film_id = {$this->film['id']}

                    ORDER BY
                      educational_values.title

                ");

                if($getEducationalValues->num_rows() > 0)
                {

                    return $getEducationalValues->result_array();

                }

            }

        }

        //--- Get Film Associated Family Time Values
        function getFamilyTimeEntries()
        {

            if($this->film)
            {

                $getFamilyTimeEntries = $this->db->query
                ("
                    SELECT
                      family_time.*

                    FROM
                      family_time

                    WHERE
                      family_time.film_id = {$this->film['id']}

                    ORDER BY
                      family_time.date DESC

                ");

                if($getFamilyTimeEntries->num_rows() > 0)
                {

                    return $getFamilyTimeEntries->result_array();

                }

            }

        }

        //--- Process Price for purchases
        //--- $film_id can also become season_id if the purchase type = group
        function process_price($film_id, $definition = 'sd', $purchase_type = 'buy', $premium_discount = false)
        {

            //--- Return Array
            $array = array();

            //--- Get Film Info
            if($purchase_type == 'group')
            {

                $season = $this->getSeasonInfo($film_id);
                $array['season'] = $season;
                $array['season_id'] = $season['id'];

            }
            else
            {

                $get_film = $this->fetch($film_id);
                $film = $get_film->film;
                $array['film'] = $film;
                $array['film_id'] = $film_id;

            }

            //--- Check for premium member discount
            $discount_percentage = 0;
            $discount_total = 0;

            if($premium_discount)
            {

                switch($purchase_type)
                {

                    case "group":
                        $discount_percentage = $this->settings['premium_member_group_discount'];
                        break;

                    case "rent":
                        $discount_percentage = $this->settings['premium_member_rental_discount'];
                        break;

                    case "buy":
                        $discount_percentage = $this->settings['premium_member_purchase_discount'];
                        break;

                }

            }

            //--- Calculate Total
            switch($definition)
            {

                case "sd":
                    switch($purchase_type)
                    {
                        case "group":
                            $price = $season['group_price'];
                            break;

                        case "rent":
                            $price = $film['rent_price_sd'];
                            break;

                        case "buy":
                            $price = $film['purchase_price_sd'];
                            break;
                    }
                    $discount_total = ($premium_discount ? ($price*$discount_percentage) : 0);
                    $price -= $discount_total;
                    break;

                case "hd":
                    switch($purchase_type)
                    {
                        case "group":
                            $price = $season['group_price'];
                            break;

                        case "rent":
                            $price = $film['rent_price_hd'];
                            break;

                        case "buy":
                            $price = $film['purchase_price_hd'];
                            break;
                    }
                    $discount_total = ($premium_discount ? ($price*$discount_percentage) : 0);
                    $price -= $discount_total;
                    break;

            }

            //--- Log Transaction
            $price = number_format($price, 2);

            //---
            $array['definition'] = strtoupper($definition);
            $array['type'] = $purchase_type;
            $array['discount_percentage'] = $discount_percentage;
            $array['discount_total'] = number_format($discount_total, 2);
            $array['grand_total'] = number_format($price, 2);

            return $array;

        }

        //--- Check to see if this film is associated with a season (series of films)
        //--- Return a list OR false if nothing
        function checkForSeason($season_id = null)
        {

            if($this->film || $season_id)
            {

                if($this->film['parent_series'] > 0 || $season_id > 0)
                {

                    $parentSeriesId = ($this->film['parent_series'] ? $this->film['parent_series'] : $season_id);

                    $seriesInfo = $this->getSeasonInfo($parentSeriesId);

                    $getAssociatedFilms = $this->db->query("SELECT * FROM films WHERE parent_series = {$seriesInfo['id']} ORDER BY episode_number ");

                    $array = array();
                    $array['series'] = $seriesInfo;
                    $array['films'] = $getAssociatedFilms->result_array();

                    return $array;

                }
                else
                {

                    return false;

                }

            }
            else
            {

                return false;

            }

        }

        //--- Get Series Info
        function getSeasonInfo($seasonId)
        {

            if($seasonId)
            {

                $get = $this->db->query("SELECT * FROM shows WHERE id = {$seasonId} LIMIT 1");

                if($get->num_rows() == 0)
                {

                    return false;

                }
                else
                {

                    return $get->row_array();

                }

            }
            else
            {

                return false;

            }

        }

        //--- Check the film purchase and track playback
        //--- Output the playback path

        function prepareToPlay($film_id = null)
        {

            $canPlay = $this->member_model->checkFilmPurchase($film_id);

            if($canPlay)
            {

                $this->trackPlayback($film_id);

                //--- Get Film info
                //--- $filmArray = $this->fetch($film_id)->film;

                return $canPlay;

            }
            else
            {

                return false;

            }

        }

        //--- Function to track playback per member
        function trackPlayback($film_id = null)
        {

            if($film_id&&$this->session->userdata('member_logged'))
            {

                $params = array();
                $params['datetime'] = date("Y-m-d H:i:s");
                $params['film_id'] = $film_id;
                $params['member_id'] = $this->session->userdata('member_logged');

                $doCheck = $this->db->query("SELECT id FROM member_film_playback WHERE film_id = {$film_id} AND member_id = {$this->session->userdata('member_logged')} LIMIT 1");

                if($doCheck->num_rows() == 0)
                {
                    $this->db->insert('member_film_playback', $params);
                }
                else
                {
                    $pbData = $doCheck->row_array();
                    $this->db->where('id', $pbData['id']);
                    $this->db->update('member_film_playback', $params);
                }

            }

        }

        //--- Get Popular Movies by Genre
        function obtainFilmsByGenre($sort = 'popular')
        {

            switch($sort)
            {

                case "popular":
                default:

                    //--- Get a list of films and sort them by Genre first
                    //--- Then sort them by most played (based on info from member_film_playback table)
                    $get = $this->db->query
                    ("
                        SELECT
                            genres.id as genre_id,
                            genres.title as genre_title,
                            films.*,
                            CASE WHEN films.type = 'show' AND shows.poster != \"\"
                              THEN shows.poster
                              ELSE films.poster
                            END as poster,
                            genres.sort as sort

                        FROM film_genres

                        LEFT JOIN films ON films.id = film_genres.film_id
                        LEFT JOIN genres ON genres.id = film_genres.genre_id
                        LEFT JOIN shows ON shows.id = films.parent_series

                        WHERE genre_id IS NOT NULL

                        ORDER BY
                            film_genres.genre_id,
                            genres.title,
                            (SELECT COUNT(id) FROM member_film_playback WHERE member_film_playback.film_id = film_genres.film_id ) DESC

                    ");

                    break;

            }

            //--- Re-Sort the array into a multi-dimensional array
            //--- Including the Genre information into each key
            $multiArray = array();
            foreach($get->result_array() as $i=>$film)
            {
                $multiArray[$film['genre_id']]['genre']['id'] = $film['genre_id'];
                $multiArray[$film['genre_id']]['genre']['title'] = $film['genre_title'];
                $multiArray[$film['genre_id']]['sort'] = $film['sort'];
                $multiArray[$film['genre_id']]['films'][$film['id']] = $film;
            }

            $multiArray = $this->system_vars->in_subval_sort($multiArray, 'sort');

            return $multiArray;

        }

        //--- Search for films based on a query string
        //--- Sort by popularity (most played)
        //--- Parameters
        //---   1) values (ID)
        //---   2) genres (ID)
        //---   3) educational_values (ID)

        function search($query = null, $type = 'all', $params = array())
        {

            //--- Query String Search
            $searchSQL = "";
            if($query)
            {

                $query = strip_tags(addslashes($query));
                $queryArray = explode(" ", $query);

                $searchSQL = "";
                foreach($queryArray as $q) $searchSQL .= " (films.title LIKE '%{$q}%' OR shows.title LIKE '%{$q}%') AND";

            }

            //--- Type Search
            $typeSql = "";
            switch($type)
            {

                case "movie":
                    $typeSql = " type = 'movie' AND";
                    break;

                case "show":
                    $typeSql = " type = 'show' AND";
                    break;

                default:
                    break;

            }

            //--- Get Genre
            $genreSql = "";
            if(isset($params['genre'])) $genreSql = " films.id IN (SELECT film_genres.film_id FROM film_genres WHERE film_genres.genre_id = {$params['genre']} ) AND ";

            //--- Get Value
            $valueSQL = "";
            if(isset($params['value'])) $valueSQL = " films.id IN (SELECT film_values.film_id FROM film_values WHERE film_values.value_id = {$params['value']} ) AND ";

            //--- Get Educational Value
            $valueSQL = "";
            if(isset($params['educational_value'])) $valueSQL = " films.id IN (SELECT film_educational_values.film_id FROM film_educational_values WHERE film_educational_values.educational_value_id = {$params['educational_value']} ) AND ";

            //--- Build SQL
            $sql ="
                SELECT
                    films.id,
                    films.title,
                    CASE WHEN films.type = 'show' AND shows.poster != \"\"
                      THEN shows.poster
                      ELSE films.poster
                    END as poster

                FROM films

                LEFT JOIN shows ON shows.id = films.parent_series

                WHERE
                  {$searchSQL}
                  {$typeSql}
                  {$genreSql}
                  {$valueSQL}
                  films.id > 0

                GROUP BY
                  CASE WHEN films.parent_series > 0
                    THEN films.parent_series
                    ELSE films.id
                  END

                ORDER BY
                    (SELECT COUNT(member_film_playback.id) FROM member_film_playback WHERE member_film_playback.film_id = films.id ) DESC
            ";

            //--- Run the SQL Statement
            $search = $this->db->query($sql);

            //--- Check for search results
            if($search->num_rows() == 0)
            {

                return false;

            }
            else
            {

                return $search->result_array();

            }

        }

        //--- Function to search/browser through members playable films

        function search_playable($query = null, $type = 'all', $params = array())
        {

            //--- Query String Search
            $searchSQL = "";
            if($query)
            {

                $query = strip_tags(addslashes($query));
                $queryArray = explode(" ", $query);

                $searchSQL = "";
                foreach($queryArray as $q) $searchSQL .= " (films.film_title LIKE '%{$q}%' OR films.show_title LIKE '%{$q}%') AND";

            }

            //--- Type Search
            $typeSql = "";
            switch($type)
            {

                case "movie":
                    $typeSql = " films.isShow = 0 AND";
                    break;

                case "show":
                    $typeSql = " films.isShow = 1 AND";
                    break;

                default:
                    break;

            }

            //--- Get Genre
            $genreSql = "";
            if(isset($params['genre'])) $genreSql = " films.film_id IN (SELECT film_genres.film_id FROM film_genres WHERE film_genres.genre_id = {$params['genre']} ) AND ";

            //--- Get Value
            $valueSQL = "";
            if(isset($params['value'])) $valueSQL = " films.film_id IN (SELECT film_values.film_id FROM film_values WHERE film_values.value_id = {$params['value']} ) AND ";

            //--- Get Educational Value
            $valueSQL = "";
            if(isset($params['educational_value'])) $valueSQL = " films.film_id IN (SELECT film_educational_values.film_id FROM film_educational_values WHERE film_educational_values.educational_value_id = {$params['educational_value']} ) AND ";

            //--- Build SQL
            $sql ="
                SELECT
                  films.film_id as id,
                  films.film_poster as poster,
                  CASE WHEN films.isShow = 1
                    THEN films.show_title
                    ELSE film_title
                  END as title

                FROM member_playable_films as films

                WHERE
                  {$searchSQL}
                  {$typeSql}
                  {$genreSql}
                  {$valueSQL}
                  films.member_id = {$this->session->userdata('member_logged')}

                GROUP BY films.film_id
            ";

            //--- Run the SQL Statement
            $search = $this->db->query($sql);

            //--- Check for search results
            if($search->num_rows() == 0)
            {

                return false;

            }
            else
            {

                return $search->result_array();

            }

        }

    }
