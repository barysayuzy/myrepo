<?

    class member_model extends CI_Model
    {

        var $member = false;

        function fetch($member_id)
        {

            $getMember = $this->db->query
            ("
                SELECT
                    members.*

                FROM
                  members

                WHERE
                  members.id = {$member_id}

                LIMIT 1
            ");

            if($getMember->num_rows() == 1)
            {

                $this->member = $getMember->row_array();

            }

            return $this;

        }

        function hasBillingProfile()
        {

            if($this->member)
            {

                if($this->member['billing_customer_id']&&$this->member['billing_payment_id'])
                {

                    return true;

                }
                else
                {

                    return false;

                }

            }
            else
            {

                return false;

            }

        }

        function isPremiumMember()
        {

            if($this->member)
            {

                if($this->member['service_expiration'] >= date("Y-m-d"))
                {

                    return true;

                }
                else
                {

                    return false;

                }

            }
            else
            {

                return false;

            }

        }

        function is_active_premium_member($memberObject)
        {

            if($memberObject['service_expiration'] >= date("Y-m-d"))
            {

                return true;

            }
            else
            {

                return false;

            }

        }

        function charge_subscription_fee($memberArray)
        {

            /* Params */
            $customer_id = $memberArray['billing_customer_id'];
            $payment_id = $memberArray['billing_payment_id'];
            $subscription_fee = 4.95;
            $fee_summary = 'Premium Member Subscription Fee';
            $invoice_id = time();

            $charge = $this->authcim->charge_card($customer_id, $payment_id, $subscription_fee, $invoice_id, $fee_summary);

            if(!$charge['status'])
            {

                return array('success'=>0, 'message'=>$charge['message']);

            }
            else
            {

                $insert = array();
                $insert['type'] = 'subscription';
                $insert['member_id'] = $memberArray['id'];
                $insert['datetime'] = date("Y-m-d H:i:s");
                $insert['total_charge'] = $subscription_fee;
                $insert['summary'] = $fee_summary;
                $insert['transaction_id'] = $charge['transaction_id'];
                $insert['authorization_code'] = $charge['auth_code'];

                $this->db->insert('transactions', $insert);

                return array('success'=>1);

            }

        }

        function purchase_film($film_id, $definition = 'sd', $purchase_type = 'buy')
        {

            /* Get Purchase Information */
            $data = $this->films->process_price($film_id, $definition, $purchase_type, $this->isPremiumMember());

            /* Params */
            $customer_id = $this->member['billing_customer_id'];
            $payment_id = $this->member['billing_payment_id'];
            $total_charge = $data['grand_total'];

            switch($purchase_type)
            {

                case "group":
                    $summary = "Season Pass to: \"{$data['season']['title']}\"";
                    break;

                case "rent":
                    $summary = "Rent film: \"{$data['film']['title']}\"";
                    break;

                case "buy":
                    $summary = "Purchase film: \"{$data['film']['title']}\"";
                    break;

            }

            $invoice_id = time();

            $charge = $this->authcim->charge_card($customer_id, $payment_id, $total_charge, $invoice_id, $summary);

            if(!$charge['status'])
            {

                return array('success'=>0, 'message'=>$charge['message']);

            }
            else
            {

                $insert = array();
                $insert['type'] = $purchase_type;
                $insert['member_id'] = $this->member['id'];
                $insert['datetime'] = date("Y-m-d H:i:s");
                $insert['film_id'] = $film_id;
                $insert['definition'] = $definition;
                $insert['total_charge'] = $total_charge;
                $insert['discount_total'] = $data['discount_total'];
                $insert['discount_percentage'] = $data['discount_percentage'];
                $insert['summary'] = $summary;
                $insert['transaction_id'] = $charge['transaction_id'];
                $insert['authorization_code'] = $charge['auth_code'];

                if($purchase_type == 'rent')
                {

                    $settings = $this->system_vars->get_settings();
                    $insert['rental_expiration'] = date("Y-m-d H:i:s", strtotime("+{$settings['rental_duration']} days"));

                }

                elseif($purchase_type == 'group')
                {

                    $insert['season_pass_id'] = $data['season']['id'];
                    unset($insert['film_id']);

                }

                $this->db->insert('transactions', $insert);

                //--- Send out email confirmation
                switch($purchase_type)
                {

                    case "rent":
                        $emailName = "purchase-rent-confirmation";
                        $emailParams = $this->member;
                        $emailParams['film_title'] = $data['film']['title'];
                        $emailParams['grand_total'] = $total_charge;
                        $emailParams['formatted_expiration_date'] = date("m/d/Y @ h:i A", strtotime("+{$settings['rental_duration']} days"));
                        break;

                    case "buy":
                        $emailName = "purchase-buy-confirmation";
                        $emailParams = $this->member;
                        $emailParams['film_title'] = $data['film']['title'];
                        $emailParams['grand_total'] = $total_charge;
                        break;

                    case "group":
                        $emailName = "purchase-group-confirmation";
                        $emailParams = $this->member;
                        $emailParams['season_title'] = $data['season']['title'];
                        $emailParams['grand_total'] = $total_charge;
                        break;

                }

                $this->system_vars->omail($this->member['email'], $emailName, $emailParams);

                //--- Return success
                return array('success'=>1);

            }

        }

        function checkFilmPurchase($film_id, $isSeason = false)
        {

            //--- Check member_playable_films VIEW
            //--- and make sure member has access to play
            //--- In ADDITION -- Get correct path for HD or SD film depending on
            //--- wether the film is premium (SD) or paid for (SD or HD -- Defined by transaction definition)

            $memberid = $this->session->userdata('member_logged');

            if($memberid)
            {

                $checkPlayableFilm = $this->db->query("SELECT * FROM member_playable_films WHERE film_id = {$film_id} AND member_id = {$memberid} LIMIT 1");
                return $checkPlayableFilm->row_array();

            }
            else
            {

                return false;

            }

        }

        //--- Get member playable films -- additionally sort by purchased (as opposed to premium members playback)
        function purchasedFilms($purchasesOnly = false, $member_id = null, $compileByGenre = true)
        {

            if(!$member_id) $member_id = $this->session->userdata('member_logged');

            $getPurchasedFilms = $this->db->query
            ("
                SELECT
                    member_playable_films.genre_id as genre_id,
                    genres.title as genre_title,
                    member_playable_films.film_id as id,
                    member_playable_films.poster

                FROM member_playable_films

                JOIN genres ON genres.id = member_playable_films.genre_id

                WHERE
                    ".($purchasesOnly ? "member_playable_films.isPremiumPlay = 0 AND" : "")."
                	member_playable_films.member_id = {$member_id} AND
                	member_playable_films.genre_id IS NOT NULL
            ");

            if($getPurchasedFilms->num_rows() == 0)
            {

                return false;

            }
            else
            {

                //--- Sort the records into GENRE groups

                if($compileByGenre)
                {

                    //--- Re-build & Re-Format The Array
                    $finalArray = array();

                    foreach($getPurchasedFilms->result_array() as $i=>$f)
                    {
                        $finalArray[$f['genre_id']]['films'][] = $f;
                        $finalArray[$f['genre_id']]['genre']['id'] = $f['genre_id'];
                        $finalArray[$f['genre_id']]['genre']['title'] = $f['genre_title'];
                        $finalArray[$f['genre_id']]['title'] = $f['genre_title'];
                    }

                    //--- Re-Key The Array
                    $array = array_values($finalArray);

                    //--- Sort The Array By Genre Title
                    $array = $this->system_vars->subval_sort($array, 'title');

                    //--- Return The Array
                    return $array;

                }
                else
                {

                    //--- Output raw film list
                    return $getPurchasedFilms->result_array();

                }

            }

        }

        //--- Purchased Films Array
        function searchPurchasedFilms($query = null, $type = 'all', $params = array(), $purchasesOnly = false, $member_id = null)
        {

            if(!$member_id) $member_id = $this->session->userdata('member_logged');

            //--- Query String Search
            $searchSQL = "";
            if($query)
            {

                $query = strip_tags(addslashes($query));
                $queryArray = explode(" ", $query);

                $searchSQL = "";
                foreach($queryArray as $q) $searchSQL .= " (films.title LIKE '%{$q}%' OR shows.title LIKE '%{$q}%') AND";

            }

            //--- Type Search
            $typeSql = "";
            switch($type)
            {

                case "movie":
                    $typeSql = " type = 'movie' AND";
                    break;

                case "show":
                    $typeSql = " type = 'show' AND";
                    break;

                default:
                    break;

            }

            //--- Get Genre
            $genreSql = "";
            if(isset($params['genre'])) $genreSql = " films.id IN (SELECT film_genres.film_id FROM film_genres WHERE film_genres.genre_id = {$params['genre']} ) AND ";

            //--- Get Value
            $valueSQL = "";
            if(isset($params['value'])) $valueSQL = " films.id IN (SELECT film_values.film_id FROM film_values WHERE film_values.value_id = {$params['value']} ) AND ";

            //--- Run SQL
            $get = $this->db->query
            ("
                SELECT
                    films.id,

                    CASE WHEN transactions.type = 'group'
                        THEN (SELECT shows.title FROM shows WHERE shows.id = films.parent_series LIMIT 1)
                        ELSE films.title
                    END as title,

                    CASE WHEN transactions.type = 'group'
                        THEN (SELECT shows.poster FROM shows WHERE shows.id = films.parent_series LIMIT 1)
                        ELSE films.poster
                    END as poster,

                    GROUP_CONCAT( film_genres.genre_id) as genre_list,
                    GROUP_CONCAT( genres.title) as genre_title_list

                FROM films

                LEFT JOIN transactions ON transactions.member_id = {$member_id} AND (films.id = transactions.film_id OR films.parent_series = transactions.season_pass_id )
                LEFT JOIN shows ON shows.id = films.parent_series
                JOIN members ON members.id = {$member_id}
				JOIN film_genres ON film_genres.film_id = films.id
				JOIN genres ON genres.id =  film_genres.genre_id

                WHERE

                    {$searchSQL}
                    {$typeSql}
                    {$genreSql}
                    {$valueSQL}
                    (
                        films.id = transactions.film_id OR
                        films.parent_series = transactions.season_pass_id
                        ".($purchasesOnly ? "" : "OR (films.premium_stream = 1 AND members.premium = 1)")."
                    )

                GROUP BY
                  CASE WHEN films.parent_series > 0
                    THEN films.parent_series
                    ELSE films.id
                  END

                ORDER BY
                    (SELECT COUNT(id) FROM member_film_playback WHERE member_film_playback.film_id = film_genres.film_id ) DESC,
                    films.title
            ");

            if($get->num_rows() == 0)
            {

                return false;

            }
            else
            {

                return $get->result_array();

            }

        }

    }

