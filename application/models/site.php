<?

    class Site extends CI_Model
    {

        function getGenres()
        {

            $get = $this->db->query(" SELECT * FROM genres ORDER BY title ");
            return $get->result_array();

        }

        function getHeroImages($type = 'store')
        {

            $getHeroImages = $this->db->query("SELECT * FROM hero_images WHERE hero_images.type = '{$type}' ");
            return $getHeroImages->result_array();

        }

        function getGenre($id)
        {

            $get = $this->db->query(" SELECT * FROM genres WHERE id = {$id} LIMIT 1");
            return $get->row_array();

        }

        function getValues()
        {

            $get = $this->db->query(" SELECT * FROM `values` ORDER BY title ");
            return $get->result_array();

        }

        function getValue($id)
        {

            $get = $this->db->query(" SELECT * FROM `values` WHERE id = {$id} LIMIT 1");
            return $get->row_array();

        }

        function getEducationalValues()
        {

            $get = $this->db->query(" SELECT * FROM educational_values ORDER BY title ");
            return $get->result_array();

        }

        function getEducationalValue($id)
        {

            $get = $this->db->query(" SELECT * FROM educational_values WHERE id = {$id} LIMIT 1");
            return $get->row_array();

        }

    }
