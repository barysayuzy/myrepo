
    <style>

        .billingSection{ font-size:14px; }
        .billingSection .left{ float:left; width:300px; }

        .billingSection .left .billing-overview{ margin:25px 0; }
        .billingSection .left .billing-overview div{ text-align:left; }

        .billingSection .right{ float:right; width:700px; border-left:solid 1px #C2C2C2; padding-left:20px; }

        .billingSection td{ font-size:14px; }

    </style>

    <section class="billingSection noTopPadding container-fluid" id="content">

        <div class='left'>

            <h2 class='light'>Your Active Billing Profile</h2>

            <?

                if($this->member['billing_customer_id'])
                {

                    echo "
                    <div class='billing-overview'>
                        <div>**** {$this->member['billing_card']}</div>
                        <div>{$this->member['billing_name']}</div>
                        <div>{$this->member['billing_address']}</div>
                        <div>{$this->member['billing_city']}, {$this->member['billing_state']} {$this->member['billing_zip']}</div>
                    </div>

                    <div style='margin:15px 0 0;' align='left'>
                        <a href='/account/billing/delete' class='btn btn-mini' onClick=\"Javascript:return confirm('Are you sure you want to delete your billing profile? This will cause your premium membership to be removed next billing cycle. Do you want to continue?');\">Delete My Billing Profile</a>
                    </div>
                    ";

                }
                else
                {

                    echo "<p style='padding:15px 0 0;color:#C0C0C0;'>You do not have an active billing profile.</p>";

                }

            ?>

        </div>
        <div class='right'>

            <h2 class='light'><?=$page_title?></h2>

            <p style='margin:15px 0;'><?=$page_description?></p>

            <form action='/account/billing/save' method='POST'>

                <table width='100%' cellPadding='10'>

                    <tr>
                        <td width='200'><b>Credit Card Number:</b></td>
                        <td><input type='text' name='cc_num' value='<?=set_value('cc_num')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'><b>Expiration Date:</b></td>
                        <td><?=$this->system_vars->exp_month("exp_month", set_value('exp_month'))?> <?=$this->system_vars->exp_year("exp_year", set_value('exp_year'))?></td>
                    </tr>

                    <tr><td colSpan='2'><hr /></td></tr>

                    <tr>
                        <td width='200'><b>First Name:</b></td>
                        <td><input type='text' name='first_name' value='<?=set_value('first_name')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'><b>Last Name:</b></td>
                        <td><input type='text' name='last_name' value='<?=set_value('last_name')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'><b>Address:</b></td>
                        <td><input type='text' name='address' value='<?=set_value('address')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'><b>City:</b></td>
                        <td><input type='text' name='city' value='<?=set_value('city')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'><b>State:</b></td>
                        <td><?=$this->system_vars->state_array_select_box('state', set_value('state'))?></td>
                    </tr>

                    <tr>
                        <td width='200'><b>Zip:</b></td>
                        <td><input type='text' name='zip' value='<?=set_value('zip')?>'></td>
                    </tr>

                    <tr>
                        <td width='200'>&nbsp;</td>
                        <td>

                            <input type='submit' name='submit' value='<?=$save_button?>' class='btn btn-large btn-info'>

                        </td>
                    </tr>

                </table>

            </form>

        </div>
        <div class='clearfix'></div>

    </section>