
    <div class='genreSection'>

        <?

            //--- List PURCHASED films
            if(!empty($purchasedFilms)&&$purchasedFilms)
            {

                echo "
                <div class='genre'>
                    <h2 class='pull-left genreTitle'>My Purchased Films</h2>
                    <a href='/account/main/listPurchased' class='pull-right more'>All Purchased Films</a>
                    <div class='clearfix'></div>
                </div>
                <div class='films'>";

                foreach($purchasedFilms as $i=>$film)
                {

                    if($i < 6)
                    {
                        echo "<a href='/movie/view/{$film['id']}' data-movie-id='{$film['id']}'><img src='/assets/uploads/{$film['poster']}' style='width:169px;' class='img-polaroid' /></a>";
                    }

                }

                echo "</div>";

            }

            //--- List ALL other films
            foreach($films as $genreObject)
            {

                echo "
                    <div class='genre'>
                        <h2 class='pull-left genreTitle'>{$genreObject['genre']['title']}</h2>
                        <a href='/account/main/genre/{$genreObject['genre']['id']}' class='pull-right more'>More In {$genreObject['genre']['title']}</a>
                        <div class='clearfix'></div>
                    </div>

                    <div class='films'>";

                foreach($genreObject['films'] as $i=>$film)
                {

                    if($i < 6)
                    {
                        echo "<a href='/movie/view/{$film['id']}' data-movie-id='{$film['id']}'><img src='/assets/uploads/{$film['poster']}' style='width:169px;' class='img-polaroid' /></a>";
                    }

                }

                echo "</div>";

            }

        ?>

    </div>