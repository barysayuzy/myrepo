
    <style>
        #confirmation h1 { text-align:center; font-size:1.375em; color:#242424; font-weight:normal; margin:25px 0 0; }
        #confirmation h1 strong { color:#2db8eb; font-weight:bold; }
        #confirmation h2 { text-align:center; font-weight:normal; color:#6e6e6e; font-size:1.875em; }
        #confirmation h2 a { color:#000; }
        #confirmation article { font-size:1.125em; color:#343434; background:url(/assets/img/tv.jpg) no-repeat; padding-left:52%; height:362px; margin-left:20px; margin-top:2em; padding-top:3em; }
        #confirmation article h3 { color:#343434; font-size:1em; }
        #confirmation article .btn { margin-right:20px; margin-top:2em; }
    </style>

    <div id="confirmation">
        <h1>
            <strong>Sorry.</strong> You don't have access to any films yet...
        </h1>
        <h2>
            Upgrade your account to a Premium Account for only $4.99/Month
        </h2>
        <article>
            <h3>What's in it for me?</h3>
            <p>- Unlimited Access to Storybox Streaming films</p>
            <p>- Receive a 5% discount in all store purchases.</p>
            <a href="/account/upgrade" class="btn btn-info btn-large">Upgrade Now!</a>
        </article>
    </div>