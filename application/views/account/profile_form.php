
    <style>

        .profileSection{ font-size:14px; }

    </style>

    <section class="profileSection container-fluid noTopPadding" id="content">

        <h2 class='light'><span>My Account //</span> Edit My Profile</h2>

        <form class='formattedForm' action='/account/profile/save' method='POST' enctype="multipart/form-data">

            <table width='100%' cellPadding='0' cellSpacing='0'>

                <tr>
                    <td style='width:250px;'>First Name: <span style='color:red;'>*</span></td>
                    <td><input type='text' name='first_name' value='<?=set_value('first_name', $this->member['first_name'])?>'></td>
                </tr>

                <tr>
                    <td style='width:250px;'>Last Name: <span style='color:red;'>*</span></td>
                    <td><input type='text' name='last_name' value='<?=set_value('last_name', $this->member['last_name'])?>'></td>
                </tr>

                <tr>
                    <td style='width:250px;'>Email Address:</td>
                    <td><?=$this->member['email']?></td>
                </tr>

                <tr><td colSpan='2'><div class='caption'>Leave password fields blank to keep your current password</div></td></tr>

                <tr>
                    <td style='width:250px;'>Choose A New Password:</td>
                    <td><input type='password' name='password' value='<?=set_value('password')?>'></td>
                </tr>

                <tr>
                    <td style='width:250px;'>Re-Type New Password:</td>
                    <td><input type='password' name='password2' value='<?=set_value('password2')?>'></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td><input type='submit' name='save' value='Save Profile' class='btn'></td>
                </tr>

            </table>

        </form>

    </section>
