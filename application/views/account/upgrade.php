<section class="upgradeDowngradeSection noTopPadding container-fluid" id="content">

    <h2 class='light black'>Upgrade / Downgrade Account</h2>
    <div class='account_status'>Your Account Status: <?=$account_status?></div>

    <div id='upgradeDowngrade'>
        <div class='left' style='height:502px;'>

            <div class='tv'></div>

            <?

                if($this->member['billing_customer_id'])
                {

                    echo "
                    <div class='billing-overview'>
                        <div><strong>Your Current Billing Profile:</strong></div>
                        <div>**** {$this->member['billing_card']}</div>
                        <div>{$this->member['billing_name']}</div>
                        <div>{$this->member['billing_address']}</div>
                        <div>{$this->member['billing_city']}, {$this->member['billing_state']} {$this->member['billing_zip']}</div>
                    </div>

                   <div style='margin:15px 0 0;' align='center'>
                       <div><a href='/account/billing' class='btn btn-mini'>Modify Billing Profile</a></div>
                   </div>
                    ";

                }

            ?>

        </div>
        <div class='right'>

            <a href='/' class='pull-right'>Learn More</a>
            <h2 class='light black'>What's in it for me?</h2>

            <ul class='upgradeBox'>
                <li>
                    <h3>StoryBox Unlimited</h3>
                    <p>Unlimited access to StoryBox streaming film library.</p>
                </li>
                <li>
                    <h3>Premium Discounts</h3>
                    <p>Get 5% OFF ALL purchases on StoryBox films & rentals!</p>
                </li>
            </ul>

            <div class='doItNow'>

                <?

                    if($this->member['premium'])
                    {

                        echo "
                        <div style='padding:25px 0;color:#666;text-align:center;font-size:12px;'>Your premium service will continue through ".date("m/d/Y", strtotime($this->member['service_expiration']))."</div>
                        <div align='center'><a href='/account/upgrade/downgrade' onClick=\"Javascript:return confirm('Are you sure you want to disable your premium subscription?');\" class='btn btn-large'>Disable My Premium Subscription</a></div>";

                    }
                    else
                    {

                        echo "
                        <div class='left'>
                            <p>For Only <b>$4.99</b> Per Month</p>
                        </div>
                        <div class='pull' align='center'>
                            <a href='/account/upgrade/process_upgrade' onClick=\"Javascript:return confirm('Are you sure you want to upgrade your account to a premium account? We will process your credit card the first months service. Would you like to continue?');\" class='btn btn-info btn-large'>Enable My Premium Account Now</a>
                        </div>
                        <div class='clearfix'></div>
                        ";

                    }

                ?>

            </div>

            <div class='noticeCopy'>** By enabling your premium members, we will attempt to charge the billing profile you have saved. The current day willb e the first day of your billing cycle paid in advance. Declined cards will temporarily downgrade your account status back to a free account until a new billing profile has been added and your preium member re-enabled. - Read our billing policy by clicking here.</div>

        </div>
        <div class='clearfix'></div>
    </div>

</section>