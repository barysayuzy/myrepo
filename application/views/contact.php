
	<style>
	
		.left{ float:left; width:48%; }
		.right{ float:right; width:48%; }
		.captcha{ margin-bottom:5px; margin-top:15px; }
		
		.submit_div{ margin-top:15px; }
		
		.tb{ padding:10px 0; }
	
	</style>

    <div style='padding:30px;'>

		<div class='left'>
		
			<h2><?=$title?></h2>
			<?=html_entity_decode(utf8_decode($content))?>
			
		</div>
		<div class='right'>
		
			<h2>Contact Us via Email</h2>
		
			<form action='/contact/submit' method='POST' style='margin:15px 0 0;padding-right:10px;'>
				
				<div class='tb'><input type='text' name='name' value='<?=set_value('name')?>' placeholder='Your Name' /></div>
				<div class='tb'><input type='text' name='email' value='<?=set_value('email')?>' placeholder='Your Email Address' /></div>
				<div class='tb'><input type='text' name='phone' value='<?=set_value('phone')?>' placeholder='Your Phone Number' /></div>
				<div class='tb'><input type='text' name='username' value='<?=set_value('username')?>' placeholder='Your Username (If you have one)' /></div>
				<div class='tb'>
					<select name="subject">
						<option value=''>Subject</option>
						<option value="General Help"<?=set_select('subject','General Help')?>>General Help</option>
						<option value="Report Abuse"<?=set_select('subject','Report Abuse')?>>Report Abuse</option>
						<option value="Report Page Error"<?=set_select('subject','Report Page Error')?>>Report Page Errors</option>
						<option value="Suggestions"<?=set_select('subject','Suggestions')?>>Suggestions</option>
						<option value="Other, No Listed"<?=set_select('subject','Other, No Listed')?>>Other, Not Listed</option>
					</select>
				</div>
				<div class='tb'><textarea name='comments' placeholder="Comments / Questions" rows='5'><?=set_value('comments')?></textarea></div>
				
				<div class='captcha'><?=$captcha?></div>
				<div class='tb'><input type='text' name='captcha' value='' placeholder='Enter the security code above' style='width:225px;' /></div>
				
				<div class='submit_div' align='left'><input type='submit' name='register' value='Submit' class='btn btn-primary btn-large'></div>
				
			</form>
		
		</div>
		<div class='clearfix'></div>
	</div>