
	</div>
	<footer id="footer">
		<ul>
			<li class="pull-left">
				Copyright 2013, &ldquo;All Rights Reserved&rdquo;
			</li>
			<li><a href="/about">About</a></li>
			<li><a href="/privacy">Privacy Policy</a></li>
			<li><a href="/terms">Terms of Use</a></li>
			<li><a href="/faq">FAQs</a></li>
			<li><a href="/contact">Contact Us</a></li>
			<li class="pull-right" id="social-links">
				<a href="https://twitter.com/StoryboxChris" target='_blank' class="twitter ir">Follow Us on Twitter</a>
				<a href="https://www.facebook.com/pages/Storyboxfamilycom/321405416293" target='_blank' class="facebook ir">Like Us on Facebook</a>
			</li>
		</ul>
	</footer>

</body>
</html>
