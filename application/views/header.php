<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?= (isset($this->template_title) ? $this->template_title : $meta['title']); ?> :: Storybox Entertainment</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/main.css">

    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="/media/bootstrap/js/bootstrap.min.js"></script>
    <script src='/assets/js/movie_popover.js'></script>

    <!-- Coin -->
    <script src='/assets/js/coin/coin-slider.min.js'></script>
    <link rel="stylesheet" href="/assets/js/coin/coin-slider-styles.css">

	<!--[if gte IE 9]>
	  <style type="text/css">.gradient { filter: none; }</style>
	<![endif]-->

    <script>

        $(function()
        {

            $('#coin-slider').coinslider({ width : 1122, height : 560, navigation : false, links : false, hoverPause : false });

            $('.facebookLoginButton').click(function(e)
            {

                e.preventDefault();

                FB.login(function()
                {

                    FB.api('/me', function(response)
                    {

                        $.post('/register/facebook', response, function(object)
                        {

                            if(object.error == '1')
                            {

                                alert(object.message);

                            }
                            else
                            {

                                window.location = object.redirect;

                            }

                        }, 'json');

                    });

                }, { scope : 'email' });

            });

        });

    </script>
    <style>
        .valueMenu{ min-width:200px; }
    </style>

</head>
<body class="<?= $body_class; ?>">

    <div id="fb-root"></div>
    <script>

        window.fbAsyncInit = function()
        {

            FB.init(
            {
                appId      : '278274838964454', // App ID
                channelUrl : '<?=site_url()?>channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : true  // parse XFBML
            });

        };

        // Load the SDK asynchronously
        (function(d){ var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0]; if (d.getElementById(id)) {return;} js = d.createElement('script'); js.id = id; js.async = true; js.src = "//connect.facebook.net/en_US/all.js"; ref.parentNode.insertBefore(js, ref); }(document));

    </script>

	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->

	<div id="wrapper">
		<header id="header" class="gradient">
			<h1 id="logo"><a href="<?= site_url(); ?>" class="ir">Storybox</a></h1>
			<nav id="nav-top">
				<? if ($member_logged) : ?>
					<a href="#" class='dropdown-toggle' data-toggle="dropdown">My Account</a>
                    <ul class="account-drop-down dropdown-menu">
                        <li><a href='/account/profile'>Modify Profile</a></li>
                        <li><a href='/account/billing'>My Billing Profile</a></li>
                        <li><a href='/account/upgrade'>Upgrade / Downgrade Account</a></li>
                    </ul>
					<a href="<?= site_url('/main/logout/'); ?>" onclick="return confirm('Are you sure you want to logout?');">Logout</a>
				<? else : ?>
					<a href="<?= site_url('/register/login'); ?>">Sign In</a>
					<a href="<?= site_url('/register/'); ?>">Join Now</a>
				<? endif; ?>
			</nav>

			<? if(!empty($this->selectedTab)&&$this->selectedTab=='my_Storybox'): ?>

                <!--

                    ** MY Storybox HEADER **

                -->

                <ul id="nav-main" class="clearfix">
                    <li class="pull-left">

                        <form action="/account/main/search/" method="get" class="form-search">
                            <input type='hidden' name='type' value='all'>
                            <div class="input-append">
                                <input type="text" name="query" value="<?=$this->input->get('query')?>" placeholder="Search My Storybox..." class="search-query input-medium">
                                <button type="submit" class="btn btn-info">Go</button>
                            </div>
                        </form>

                    </li>
                    <li <?=(!empty($this->selectedTab)&&$this->selectedTab == 'my_storybox' ? " class='active'" : "")?>><a href="/account/">My Storybox</a></li>
                    <li <?=(!empty($this->selectedTab)&&$this->selectedTab == 'store' ? " class='active'" : "")?>><a href="/">Store</a></li>

                    <li class="pull-right dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Values <b class="caret"></b>
                        </a>
                        <!-- Value Menu -->
                        <ul class="dropdown-menu valueMenu">

                            <?

                                echo "<li class='nav-header'>Storybox Values</li>";

                                //--- Main Storybox Values
                                foreach($this->site->getValues() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/account/main/value/{$g['id']}'>{$g['title']}</a></li>";

                                }

                                echo "<li class='nav-header'>Educational Values</li>";

                                //--- Educational Values
                                foreach($this->site->getEducationalValues() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/account/main/educational/{$g['id']}'>{$g['title']}</a></li>";

                                }

                            ?>

                        </ul>
                    </li>
                    <li class="pull-right dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Genre <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <?

                                foreach($this->site->getGenres() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/account/main/genre/{$g['id']}'>{$g['title']}</a></li>";

                                }

                            ?>
                        </ul>
                    </li>

                </ul>

            <? else: ?>

                <!--

                    ** STANDARD HEADER **

                -->

                <ul id="nav-main" class="clearfix">
                    <li class="pull-left">

                        <form action="/movie/search/" method="get" class="form-search">
                            <input type='hidden' name='type' value='all'>
                            <div class="input-append">
                                <input type="text" name="query" value="<?=$this->input->get('query')?>" placeholder="Search Films Here..." class="search-query input-medium">
                                <button type="submit" class="btn btn-info">Go</button>
                            </div>
                        </form>

                    </li>
                    <li <?=(!empty($this->selectedTab)&&$this->selectedTab == 'my_storybox' ? " class='active'" : "")?>><a href="/account/">My Storybox</a></li>
                    <li <?=(!empty($this->selectedTab)&&$this->selectedTab == 'store' ? " class='active'" : "")?>><a href="/">Store</a></li>
                    <li class="pull-right dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Values <b class="caret"></b>
                        </a>

                        <!-- Value Menu -->
                        <ul class="dropdown-menu valueMenu">
                            <?

                            echo "<li class='nav-header'>Storybox Values</li>";

                                foreach($this->site->getValues() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/movie/value/{$g['id']}'>{$g['title']}</a></li>";

                                }

                                echo "<li class='nav-header'>Educational Values</li>";

                                //--- Educational Values
                                foreach($this->site->getEducationalValues() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/movie/educational/{$g['id']}'>{$g['title']}</a></li>";

                                }

                            ?>
                        </ul>
                    </li>
                    <li class="pull-right dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Genre <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <?

                                foreach($this->site->getGenres() as $g)
                                {

                                    if(!$g['hide_main_menu']) echo "<li><a href='/movie/genre/{$g['id']}'>{$g['title']}</a></li>";

                                }

                            ?>
                        </ul>
                    </li>
                </ul>

            <? endif;

                if(!empty($hero_images))
                {

                    echo "<div id='coin-slider'>";

                    foreach($hero_images as $h)
                    {

                        echo "<a href='#'><img src='/media/assets/{$h['image']}' class='bgimage' /></a>";

                    }

                    echo "</div>";

                }

            ?>

        </header>

		<?php if ( ! empty($errors)) : ?>
			<div class="alert alert-error responseBox"><a class="close" data-dismiss="alert" href="#">&times;</a><?= implode(PHP_EOL, $errors); ?></div>
		<?php endif; ?>

		<?php if ( ! empty($message)) : ?>
			<div class="alert alert-info responseBox"><a class="close" data-dismiss="alert" href="#">&times;</a><?= $message; ?></div>
		<?php endif; ?>
