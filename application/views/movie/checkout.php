
    <style>

        #checkoutWrapper{ padding:50px 0; }
        #checkoutWrapper h1{ font-size:22px; color:#242424; font-weight:normal; line-height:20px; padding:0; margin:0; }
        #checkoutWrapper .caption{ font-size:16px; color:#49c2ee; padding:8px 0; }
        #checkoutWrapper .premium_discount{ font-size:16px; color:red; padding:8px 0; }
        #checkoutWrapper .well{ width:568px; margin-top:35px; }
        #checkoutWrapper .img{ width:158px; }
        #checkoutWrapper .img img{ width:158px; }

        #checkoutWrapper .billing_profile{ width:390px; }
        #checkoutWrapper .billing_profile .title{ padding:35px 0 0; }
        #checkoutWrapper .billing_profile .link{ padding:35px 0 0; }

        #checkoutWrapper .confirm_button{ padding:35px 0 0; }
        #checkoutWrapper .confirm_button .btn{ padding:25px 30px; }

    </style>

    <script>

        $(document).ready(function()
        {

            $('#finalPurchase').click(function(e)
            {

                // e.preventDefault();

                $(this).attr('disabled', 'disabled');
                $(this).html("Processing Purchase...");
                $(this).removeClass('btn-info');

            });

        });

    </script>

    <div id='checkoutWrapper' align='center'>
        <h1><?=$title?></h1>
        <div class='caption'><?=$caption?></div>
        <?

            if($this->memberObject->isPremiumMember())
            {

                echo "<div class='premium_discount'>{$premium_discount}</div>";

            }

        ?>

        <div class='well'>
            <div class='img pull-left'>
                <img src='<?=$image?>' class='img-polaroid' />
            </div>
            <div class='pull-right billing_profile'>
                <div class='title'><b>Your Current Billing Profile</b></div>
                <?

                    if($this->memberObject->hasBillingProfile())
                    {

                        echo "
                        <div class='card_number'>**** {$this->member['billing_card']}</div>
                        <div class='name'>{$this->member['billing_name']}</div>
                        <div class='address1'>{$this->member['billing_address']}</div>
                        <div class='address2'>{$this->member['billing_city']}, {$this->member['billing_state']} {$this->member['billing_zip']}</div>
                        <div class='link'><a href='/purchase/change_billing_profile/{$type}/{$film_id}/{$definition}' class='btn'>Change Billing Profile</a></div>
                        ";

                    }
                    else
                    {

                        echo "
                        <div class='card_number'><i style='font-size:12px;padding:20px;'>You do not have a billing profile</i></div>
                        <div class='link'><a href='/purchase/change_billing_profile/{$type}/{$film_id}/{$definition}' class='btn btn-danger btn-large'>Add Billing Profile To Continue</a></div>
                        ";

                    }

                ?>

            </div>
            <div class='clearfix'></div>
        </div>

        <?

            if($this->memberObject->hasBillingProfile())
            {

                echo "<div align='center' class='confirm_button'><a id='finalPurchase' href='/purchase/confirm/{$film_id}/{$definition}/{$type}/".(isset($episode_film_id) ? $episode_film_id : "")."' class='btn btn-large btn-info'>Confirm Purchase</a></div>";

            }

        ?>

    </div>