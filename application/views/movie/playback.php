<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>StoryBox | <?=$title?></title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="/media/flowplayer/flowplayer-3.2.12.min.js"></script>
    <style>
        body{background:#000;margin:0;padding:0;}
    </style>
    <script>

        $(function()
        {

            $('#movieBox_player').height($(window).height());

            flowplayer("movieBox_player", "/media/flowplayer/flowplayer-3.2.16.swf",
            {

                clip:
                {
                    url: 'mp4:<?=$playback_path?>',
                    scaling: 'fit',
                    provider: 'hddn'
                },

                plugins:
                {

                    hddn:
                    {

                        url: "/media/flowplayer/flowplayer.rtmp-3.2.12.swf",
                        netConnectionUrl: 'rtmp://50.19.115.70/storytest/'
                    }
                }

            });

        });

    </script>
</head>

<body>

    <div id='movieBox_player'></div>

</body>
</html>