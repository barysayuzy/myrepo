<style>

    .tab-content{ font-size:14px; line-height:20px; }

    #movieBox #movieBox_bg{ z-index:2; opacity:.85; background:#000; position:fixed; top:0; left:0; width:100%; height:100%; }
    #movieBox #movieBox_player{ z-index:3; position: fixed; top:0; left:0; width:100%; height:100%; }
    #movieBox #movieBox_close{ z-index:4; position: fixed; top:10px; right:10px; }
    #movieBox #movieBox_close span{ font-size:18px; text-decoration: none; color:#FFF; font-weight: bold; }

</style>

<script type="text/javascript" src="/media/flowplayer/flowplayer-3.2.12.min.js"></script>
<script>

    $(function()
    {

        <?

            //-- If the user wants a preview - auto-load the preview screen
            if($this->uri->segment('4')=='preview')
            {

                echo "setTimeout(function()
                {

                    $('#watchPreviewAnchor').trigger('click');

                }, 500);";

            }

        ?>

        $('.loadPreview').click(function(e)
        {

            e.preventDefault();
            $('body').prepend("<div id='movieBox'><div id='movieBox_bg'></div><a href='/' id='movieBox_close'><span>X</span></a><div id='movieBox_player'></div></div>");

            flowplayer("movieBox_player", "/media/flowplayer/flowplayer-3.2.16.swf",
            {

                clip:
                {
                    url: 'mp4:<?=$preview_file?>',
                    scaling: 'fit',
                    provider: 'hddn'
                },

                plugins:
                {

                    hddn:
                    {

                        url: "/media/flowplayer/flowplayer.rtmp-3.2.12.swf",
                        netConnectionUrl: 'rtmp://50.19.115.70/storytest/'
                    }
                }

            });

        });

        $(document).on('click', '#movieBox_close', function(e)
        {
            e.preventDefault();
            $('#movieBox').remove();
        });

    });

</script>

<header class="movie" style='background-image:url(/assets/uploads/<?=$banner?>);'>

	<h1><?=$title?></h1>
	<h2><?=($subtitle ? $subtitle : "Start watching today!")?></h2>

    <? if($this->purchased): ?>

        <div>
            <a href="/movie/play/<?=$id?>" class="btn btn-large">Play Now</a>
        </div>

    <? else: ?>

        <div class="dropdown">
            <a href="/" class="btn btn-large dropdown-toggle" data-toggle="dropdown">Play Now</a>
            <ul class="dropdown-menu">
                <?

                    if($rent)
                    {
                        if($sd_film_id&&$rent_price_sd>0) echo "<li><a href=\"/purchase/rent/{$id}/sd\">Rent - $".number_format($rent_price_sd, 2)."</a></li>";
                        if($hd_film_id&&$rent_price_hd>0) echo "<li><a href=\"/purchase/rent/{$id}/hd\">Rent HD - $".number_format($rent_price_hd, 2)."</a></li>";
                    }

                    if($purchase)
                    {
                        if($sd_film_id&&$purchase_price_sd>0) echo "<li><a href=\"/purchase/buy/{$id}/sd\">Buy - $".number_format($purchase_price_sd, 2)."</a></li>";
                        if($hd_film_id&&$purchase_price_hd>0) echo "<li><a href=\"/purchase/buy/{$id}/hd\">Buy HD - $".number_format($purchase_price_hd, 2)."</a></li>";
                    }

                    if($season && $season['series']['allow_group_purchase'])
                    {
                        echo "<li style='background:#666;'><a style='color:#FFF;text-shadow:1px 1px 0 #000;' href=\"/purchase/group/{$season['series']['id']}/sd/{$id}\">Buy Season Pass - $".number_format($season['series']['group_price'], 2)."</a></li>";
                    }

                ?>
            </ul>
        </div>

    <? endif; ?>

</header>
<section id="content" class="movie clearfix">
	<div class="col-left">
		<ul>
			<li class="poster gradient vert-inverse">
				<img src="/assets/uploads/<?=$poster?>" alt="">
			</li>
			<li>
				<a href="/" class="loadPreview btn btn-info btn-large" id='watchPreviewAnchor'>Watch Preview</a>
			</li>

            <? if($this->purchased): ?>

                <li>
                    <a href="/movie/play/<?=$id?>" class="btn btn-large">Play Now</a>
                </li>

            <? elseif(!$this->session->userdata('member_logged')): ?>

                <li>
                    <a href="/movie/login/<?=$id?>" class="btn btn-large">Play Now</a>
                </li>

            <? elseif($purchase||$rent): ?>

                <li class="dropdown">
                    <a href="#" class="btn btn-info btn-large dropdown-toggle" data-toggle="dropdown">Play Now <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?

                            if($rent)
                            {
                                if($sd_film_id&&$rent_price_sd>0) echo "<li><a href=\"/purchase/rent/{$id}/sd\">Rent - $".number_format($rent_price_sd, 2)."</a></li>";
                                if($hd_film_id&&$rent_price_hd>0) echo "<li><a href=\"/purchase/rent/{$id}/hd\">Rent HD - $".number_format($rent_price_hd, 2)."</a></li>";
                            }

                            if($purchase)
                            {
                                if($sd_film_id&&$purchase_price_sd>0) echo "<li><a href=\"/purchase/buy/{$id}/sd\">Buy - $".number_format($purchase_price_sd, 2)."</a></li>";
                                if($hd_film_id&&$purchase_price_hd>0) echo "<li><a href=\"/purchase/buy/{$id}/hd\">Buy HD - $".number_format($purchase_price_hd, 2)."</a></li>";
                            }

                            if($season && $season['series']['allow_group_purchase'])
                            {
                                echo "<li style='background:#666;'><a style='color:#FFF;text-shadow:1px 1px 0 #000;' href=\"/purchase/group/{$season['series']['id']}/sd/{$id}\">Buy Season Pass - $".number_format($season['series']['group_price'], 2)."</a></li>";
                            }

                        ?>
                    </ul>
                </li>

            <? else: ?>
            <? endif; ?>

		</ul>
	</div>
	<div class="col-right">
		<div class="description">
			<p>
                <b>Description</b><br />
				<?=$description?>
			</p>
		</div>
		<div id="tabs">
			<ul class="nav nav-tabs">
                <?
                    if($season)
                    {
                        echo "<li class=\"active\"><a href=\"#episodes\" data-toggle=\"tab\">Episodes</a></li>";

                    }

                ?>
				<li <?=(!$season ? "class='active'" : "")?>><a href="#values" data-toggle="tab">Show Details</a></li>
				<?
					if ($isPremiumMember) {
				?>
				<li><a href="#education" data-toggle="tab">Learning Zone</a></li>
				<li><a href="#family" data-toggle="tab">Family Time</a></li>
				<?	}?>
			</ul>
			<div class="tab-content">
				<div class="tab-pane <?=($season ? "active" : "")?>" id="episodes">
					<script>
						function filter_year_range()
						{
							
							var year_from = $("#year_range_from").val();
							var year_to = $("#year_range_to").val();
							
							episode_trs = $("[id^='eash_episode']");
							ep_count = episode_trs.length;
							var index =0; 
							for (; index<ep_count; index++)
							{
								episode_id = $(episode_trs[index]).attr('id');
								sub_year_index = episode_id.lastIndexOf("_");
								sub_year = episode_id.substring(sub_year_index+1);
								
								if (sub_year >= year_from && sub_year <= year_to )
								{
									$(episode_trs[index]).show();
								}
								else
								{
									$(episode_trs[index]).hide();
								}
							}
						}
					</script>
					<table width='100%'>
						<tbody>
							<tr style="padding: 0px; margin: 0px; height: 10px;border: none; border-spacing: 0px;" >
								<td style='width:60px;'>
								</td>
								<td class=\"description\">
									<div style="float:left">
										<input class="year_range" type="number" id="year_range_from" min="1" max="9999" value="1930">
										
									</div>
									<p style="float:left" >--</p>
									<div style="float:left">
										<input class="year_range" type="number" id="year_range_to" min="1" max="9999" value="2014">
									</div>
									
									<div style="float:left">
										<input type="button" class = "btn btn-info btn-small" onclick = "javascript:filter_year_range()" value="Filter">
									</div>  
								</td>
                            </tr>
							<?
                                $i = 1;
                                foreach($season['films'] as $s)
                                {
                                	$sub_year = $s['sub_year'];
                                		
                                    echo "
                                    <tr id='eash_episode{$i}_{$sub_year}'>
                                        <td style='width:60px;'>
                                        	<input 
                                            <div class=\"poster poster-small\">
                                                <img src=\"/assets/uploads/{$poster}\" alt=\"\">
                                            </div>
                                        </td>
                                        <td class=\"description\">
                                            <h4>Episode {$i}: {$s['title']}</h4>
                                            <p>{$s['description']}</p>
                                        </td>
                                        <td style='text-align:right;'>
                                            <a href=\"/movie/view/{$s['id']}\" class=\"btn ".($id==$s['id'] ? " disabled" : "btn-info")."\">View Details</a>
                                        </td>
                                    </tr>
                                    ";

                                    $i++;
                                }
                            ?>

						</tbody>
					</table>
				</div>
				<div class="tab-pane <?=(!$season ? "active" : "")?>" id="values">

					<? if($type == "show"){ ?>
                    	<h2 style="padding:0 0 10px 0;">Show Details</h2>
                    <? }else { ?>
                    	<h2 style="padding:0 0 10px 0;">Movie Details</h2>
                    <? } ?>
                    
                    <? if($type == "show"){ ?>
                    
						<? if($creator): ?>
	                        <p><b>Creator</b>: <?=$creator?></p>
	                    <? endif; ?>
                    
	                    <? if($director): ?>
	                        <p><b>Director</b>: <?=$director?></p>
	                    <? endif; ?>
	
	                    <? if($stars): ?>
	                        <p><b>Stars</b>: <?=$stars?></p>
	                    <? endif; ?>
	                 
                    <? }else { ?>
                    	
                    	<? if($director): ?>
	                        <p><b>Director</b>: <?=$director?></p>
	                    <? endif; ?>
	                    
	                    <? if($stars): ?>
	                        <p><b>Stars</b>: <?=$stars?></p>
	                    <? endif; ?>
                    	
                    	<? if($creator): ?>
	                        <p><b>Creator</b>: <?=$creator?></p>
	                    <? endif; ?>
	

                    <? } ?>

                    <? if($genres): ?>
                        <p><b>Genres</b>:
                            <? foreach($genres as $v): ?>
                                <?=(end($genres)==$v ? $v['title'] : $v['title'].", ")?>
                            <? endforeach; ?>
                        </p>
                    <? endif; ?>

                    <? if($values): ?>
                        <p><b>Values</b>:
                            <? foreach($values as $v): ?>
                                <?=(end($values)==$v ? $v['title'] : $v['title'].", ")?>
                            <? endforeach; ?>
                        </p>
                    <? endif; ?>

                    <? if($educational_values): ?>
                        <p><b>Educational Values</b>:
                            <? foreach($educational_values as $v): ?>
                                <?=(end($values)==$v ? $v['title'] : $v['title'].", ")?>
                            <? endforeach; ?>
                        </p>
                    <? endif; ?>

                    <? if($parents): ?>
                        <p><b>Parents</b>: <?=$parents?></p>
                    <? endif; ?>

				</div>
				<div class="tab-pane" id="education">
                    <?

                        if($educational_values)
                        {

                            $n = count($educational_values) - 1;
                            foreach($educational_values as $i=>$v)
                            {

                                echo "
                                <p>
                                    <div><h2>".($v['post_title'] ? $v['post_title'] : $v['title'])."</h2></div>
                                    ".utf8_encode(html_entity_decode($v['description']))."
                                </p>";

                                if($i != $n) echo "<hr />";

                            }

                        }
                        else
                        {

                            echo "<p>The educational values in this film will be updated soon. Please stay tuned.</p>";

                        }

                    ?>
				</div>
				<div class="tab-pane" id="family">

                    <?

                    if($family_time)
                    {

                        $n = count($family_time) - 1;
                        foreach($family_time as $i => $f)
                        {

                            echo "
                                <p>
                                    <h2>{$f['title']}</h2>
                                    <p>".utf8_encode(html_entity_decode($f['content']))."</p>
                                </p>";

                            if($i != $n) echo "<hr />";

                        }

                    }
                    else
                    {

                        echo "<p>There are no family time entries for this film at this time. Please stay tuned.</p>";

                    }

                    ?>

                    </table>
				</div>
			</div>
		</div>
<!--		<script>
		$(function() {
			$('.nav-tabs a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});
		});
		</script>-->
	</div>
</section>