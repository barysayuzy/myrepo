
	<script>
		function see_more_searchresult()
		{
			$("#more_show").show();
			$("#see_more_link").hide();
		}
	</script>
    <div class='genreSection'>
    <?
    	if (count($films) > 6)
    	{
    		echo "
    		<div class='genre'>
    		
    		<h2 class='pull-left genreTitle'>{$title}</h2>
    		<a id='see_more_link' href='javascript:see_more_searchresult()' class='pull-right more'>See More...</a>
    		<div class='clearfix'></div>
    		</div>
    		";
    	}
    	else
    	{
    		echo "
    		<div class='genre'>
    		<h2 class='genreTitle'>{$title}</h2>
    		</div>
    		";
    	}
        

        if($films)
        {
            echo "<div class='films'>";
            echo "<div id = 'default_show' style='display:block'>";

            foreach($films as $i=>$film)
            {
                if($i == 6) {
                	echo "</div>";
                	echo "<div id ='more_show' style='display:none'>";	
                }
                
                echo "<a href='/movie/view/{$film['id']}' data-movie-id='{$film['id']}'><img src='/assets/uploads/{$film['poster']}' style='width:169px;' class='img-polaroid' /></a>";
            }
            echo "</div>";
            echo "</div>";
        }
        else
        {
            echo "<p>Sorry, please try a different search</p>";
        }
    ?>
    </div>