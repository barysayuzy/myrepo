<?

	echo "<h2>".$this->profile['member']['username']."</h2>
	
	<div class='expert_block' style='border:none;'>
				
		<div class='ex_left' align='left'>
		
			<a href=\"/profile/view/{$this->profile['id']}\"><img src=\"".$this->profile['member']['profile']."\" width='100' style='border:solid 2px #E0E0E0;'></a>
			
			<div style='padding:5px 0 0;'><b>Offline</b></div>
			".($this->uri->segment('2')!='view' ? "<div style='padding:5px 0 0;'><a href=\"/profile/view/{$this->profile['id']}\"><img src=\"/media/images/user_icon.png\"> View Profile</a></div>" : "")."
			<div style='padding:5px 0 0;'><a href=\"/profile/add_to_favorites/{$this->profile['id']}\"><img src=\"/media/images/star_icon.png\"> Add to favorite</a></div>
		
		</div>
		
		<div class='ex_right' align='left'>
		
			<h3><a href=\"/profile/view/{$this->profile['id']}\">{$this->profile['member']['username']}</a></h3>
			
			<div style='padding:5px 0 0;'><strong>Rating:</strong> N/A</div>  
			
			<div style='padding:5px 0 0;'><strong>Expert In:</strong> {$this->profile['subcategories']}</div>	
	
			<div style='padding:5px 0 0;'><strong>Available Modes Of Communication:</strong> {$this->profile['communication_type']} </div>				
			<div style='padding:5px 0 0;line-height:18px;'>{$this->profile['brief_description']}</div>";
			
			if($this->profile['available_for_email']=='1' && $this->uri->segment('2') != 'send_question')
			{
			
				echo "
				<div class='email_button'>
					<a href=\"/profile/send_question/{$this->profile['id']}\" class='blue-button'><span>Send Question</span></a>
					<div class='price'>($ ".number_format($this->profile['price_per_email'], 2)." per question)</div>
				</div>";
			
			}
	
		echo "</div>
		
		<div class='clear'></div>
		
	</div>
	
	<hr />
	";

?>