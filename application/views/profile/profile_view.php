
	<style>
	
		.desc{ margin-bottom:15px; line-height:22px; }
	
	</style>

	<div class='content_area'>
	
		<?
		
			$this->load->view('profile/badge');
		
			echo "
			<h2>Description</h2>
			<div class='desc'>".nl2br($this->profile['detailed_description'])."</div>
			
			<h2>Degrees</h2>
			<div class='desc'>".nl2br($this->profile['degrees'])."</div>
			
			<h2>Experience</h2>
			<div class='desc'>".nl2br($this->profile['experience'])."</div>
			
			<hr />
			
			<h2>Reviews & Ratings</h2>
			
			<div>This expert does not have any reviews or ratings.</div>
			";
		
		?>
	
	</div>