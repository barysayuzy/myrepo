
	<script src='/media/javascript/jqui/jquery-ui-1.8.16.custom.min.js'></script>
	<link rel="stylesheet" href="/media/javascript/jqui/css/overcast/jquery-ui-1.8.16.custom.css" />

	<script src='/media/javascript/datetime/jquery-ui-timepicker-addon.js'></script>
	<link rel="stylesheet" href="/media/javascript/datetime/jquery-ui-timepicker-addon.css" />

	<style>
	
		.ui-widget{ font-size:0.9em; }
		.datetime{ cursor: pointer; background:none !important; border:none; color:blue; text-decoration:underline; }
	
	</style>

	<script>
	
		$(document).ready(function()
		{
		
			$('.datetime').datetimepicker
            ({
            	ampm: true,
				separator: ' @ '
            });
		
			$("input[name='deadline']").click(function()
			{
			
				if($(this).is(':checked'))
				{
				
					$('#div_deadline').show();
				
				}
				else
				{
				
					$('#div_deadline').hide();
				
				}
			
			});
		
		});
	
	</script>

	<div class='content_area'>
	
		<? $this->load->view('profile/badge'); ?>
		
		<h2>Send <?=$this->profile['member']['username']?> a question</h2>
		
		<form action='/profile/submit_question/<?=$this->profile['id']?>' method='POST'>
		
			<table width='100%' cellPadding='10'>
				
				<tr>
					<td width='150'><b>Title:</b></td>
					<td><input type='text' name='title' value='<?=set_value('title')?>' style='width:95%;'></td>
				</tr>
				
				<tr>
					<td width='150'><b>Question:</b></td>
					<td><textarea name='question' rows='10' style='width:95%;'><?=set_value('question')?></textarea></td>
				</tr>
				
				<tr>
					<td width='150'><b>Deadline:</b></td>
					<td>
					
						<div style='float:left;width:70px;'><input type='checkbox' name='deadline' value='1' <?=set_checkbox('deadline','1')?>> Yes</div>

						<div id='div_deadline' style='float:left;width:300px;display:<?=(set_value('deadline')=='1' ? "block" : "none")?>;'>
							<table cellPadding='0' cellSpacing='0'>
								<tr>
									<td style='padding:0 15px;'>Select Date:</td>
									<td>
										<input type='text' style='width:150px;' name='date' value='<?=set_value('date', date("m/d/Y @ h:i A", strtotime("+3 day")))?>' readonly class='datetime'>
									</td>
								</tr>
							</table>
							
							<div class='clear'></div>
						</div>
					
					</td>
				</tr>
				
				<tr>
					<td width='150'><b>Expert Average:</b></td>
					<td><div>$<?=number_format($this->profile['price_per_email'], 2)?> Per Question</div></td>
				</tr>
				
				<tr>
					<td width='150'><b>Your Price Range:</b></td>
					<td>
						<select name="price">
							<option <?=set_select('price','$5.00 - $25.00')?>>$5.00 - $25.00</option>
							<option <?=set_select('price','$25.00 - $50.00')?>>$25.00 - $50.00</option>
							<option <?=set_select('price','$50.00 - $100.00')?>>$50.00 - $100.00</option>
							<option <?=set_select('price','Not Sure - Let the Expert Decide')?>>Not Sure - Let the Expert Decide</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td width='150'>&nbsp;</td>
					<td><a href='/' class='blue-button submit'><span>Send Question</span></a></td>
				</tr>
			
			</table>
		
		</form>
	
	</div>