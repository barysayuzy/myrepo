<div id="confirmation">
	<h1>
		<strong>Congratulations!</strong> You've successfully created your free StoryBox account.
	</h1>
	<h2>
		Upgrade your account to a Premium Account for only $4.99/Month
	</h2>
	<article>
		<h3>What's in it for me?</h3>
		<p>- Unlimited Access to Storybox Streaming films</p>
		<p>- Receive a 5% discount in all store purchases.</p>
		<a href="/account/upgrade" class="btn btn-info btn-large">Upgrade Now!</a>
		<a href="/" class="btn">No, Thanks.</a>
	</article>
</div>