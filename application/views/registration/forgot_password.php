
	<section id="content" class="container-fluid">
		<div class="row-fluid">
			<h2 style="margin-bottom:5px;">Forgot Password?</h2>
            <p>Use the form below to choose your new password. We will send you an email that contains a link to confirm your password reset. Your new password will <u>NOT</u> be effective until the link in the email is clicked.</p>
		</div>
		<hr>
		<div class="row-fluid">
			<form method="post" action="/register/forgot_password_submit" id="loginform" class='span7'>
				<div class="control-group">
					<div class="controls">
						<input type="email" class="span8" placeholder="Your email address" name="email" value='<?=set_value('email')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" class="span8" placeholder="Choose a new password" name="password" value='<?=set_value('password')?>'>
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls">
						<input type="password" class="span8" placeholder="Re-type your new password" name="password2" value='<?=set_value('password2')?>'>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-large">Submit</button>
				</div>
			</form>
		</div>
	</section>