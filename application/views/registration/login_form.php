
	<section id="content" class="container-fluid">
		<div class="row-fluid">
			<h2 class="span7">Member Login</h2>
			<h2 class="span5 center">New User? <span class="gray">Join Now!</span></h2>
		</div>
		<hr>
		<div class="row-fluid">
			<form method="post" action="/register/login_submit" id="loginform" class='span7'>
				<div class="control-group">
					<div class="controls">
						<input type="email" class="span8" placeholder="Email" name="email_address" value='<?=set_value('email_address')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" class="span8" placeholder="Password" name="password" value='<?=set_value('password')?>'>
					</div>
				</div>
				<div class="form-actions">
					<button type="submit" class="btn btn-large">Sign in</button>
                    <a href='#' class="facebookLoginButton" style="margin-left:92px;"><img src="/assets/img/loginwithfacebook.jpg"></a>
				</div>
				<a href="/register/forgot_password">Forgot Password?</a>
			</form>
			<div class="span5 center">
				<h3>Don't have an account? Register today!<br>It's FAST &amp; FREE.</h3>
				<a href="/register" class="btn btn-info btn-large" style='margin:15px 0 0;'>Register Now!</a>
			</div>
		</div>
	</section>