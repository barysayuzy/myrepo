
	<hgroup class="gradient vert">
		<h1>Register Today</h1>
		<h2>It's FAST &amp; FREE! Start straming movies instantly!</h2>
	</hgroup>
	<section id="content" class="container-fluid">
		<div class="row-fluid">
			<form action="/register/submit" method="post" class="span8 center">
				<div class="control-group">
					<div class="controls">
						<input type="text" class="span8" placeholder="Your First Name" name='first_name' value='<?=set_value('first_name')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="text" class="span8" placeholder="Your Last Name" name='last_name' value='<?=set_value('last_name')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="email" class="span8" placeholder="Your Email Address" name='email_address' value='<?=set_value('email_address')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="email" class="span8" placeholder="Re-Type Your Email Address" name='email_address2' value='<?=set_value('email_address2')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" class="span8" placeholder="Choose a New Password" name='password' value='<?=set_value('password')?>'>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" class="span8" placeholder="Re-Enter Your New Password" name='password2' value='<?=set_value('password2')?>'>
					</div>
				</div>
				<p class="disclaimer">
					By clicking Create Account, you agree to the
					<a href="/terms" target='_blnk'>Terms of Use</a>
					and
					<a href="/privacy" target='_blnk'>Privacy Policy</a>.
				</p>
				<div class="form-actions">
					<button type="submit" class="btn btn-large">Sign Up!</button>
				</div>
			</form>
			<div class="span4 center">
				<div id="why" class="gradient horiz bordered rounded">
					<a href="/why" class="video-link ir">Watch a Video</a>
					<h4>Why Should I Register?</h4>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
						libero sem, rutrum ut auctor at, vestibulum vel justo. Aliquam rhoncus,
						sapien in mattis interdum, nisi enim tempus justo, eget mollis augue
						nulla euismod erat.
					</p>
					<a href="/why" class="underline">Learn More</a>
				</div>
			</div>
		</div>
	</section>