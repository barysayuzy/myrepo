<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>I'm Your Answer</title>
	<link type="text/css" href="/media/css/stylesheet.css" rel="stylesheet" />
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
</head>

<body>

	<div class="page">
		
		<div class="logo">
	    	<h1><a href="/index.php" class="logo">Keywords go here for I'm your answer</a></h1>
	    </div>
	    
	    <div class="portlet" id="yw0">
			<div class="portlet-content">
				<div class="top_header_bar" />
					<a class="blue-button" href="/login"><span>Login</span></a>
					<a class="blue-button" href="/client/registration"><span>Client Register</span></a>
					<a class="blue-button" href="/expert/registration"><span>Expert Register</span></a>
				</div>
			</div>
		</div>
		
		<div class="portlet" id="yw1">
			<div class="portlet-content">
				<div class="navigation">
				
					<ul>
						<li><a href="/index.php">Home</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/3">Arts & Entertainment</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/6">Business & Finance</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/40">Health & Wellness</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/101">Technology</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/75">Spirituality & Religion</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/58">Society & Lifestyle</a></li>
						<li><img src="/media/images/navigation-break.jpg" width="2" height="38" /></li>
						<li><a href="/common/category/catID/1">More</a>	</li>
					</ul>
					
				</div>
			</div>
		</div> <!-- .portlet -->
	
		<div class="banner">
	    	
	    	<h1><span style="font-size: 35px;">get</span> answers
	        <br/><span style="font-size: 35px; padding-left: 80px;">from the</span> experts</h1>
	        <p>I'm Your Answer...connects you to real Experts screened in numerous fields of expertise. Receive answers to all your questions in real time, by chat or email - you choose!</p>
	   
		</div> <!-- .banner -->
	    
		<div class="side_content">
	
			<div class="search">        
				<form name="search" method="post" action="/common/search">
					<input type="text" name="search" placeholder="Search..." class="textInput" />
					<input type="image" src="/media/images/search-btn.jpg" name="image" width="35" height="35" class="submit_button" />
				</form>        
			</div>
	
			<div class="articles">
			
				<h2>New Articles</h2>
				
				<!-- Article -->
				<p class="date">2011-06-01 15:10:30</p>
                	<h3><a href="/common/articles/view/id/4">test article added by admin</a></h3>
					<p>hello short</p>
				</p>

                <a href="/common/articles/view/id/4">READ MORE &#187;</a>  
				<p style="clear: both;"></p>
				<!-- -->
				
				<!-- Article -->
				<p class="date">2011-06-01 15:10:30</p>
                	<h3><a href="/common/articles/view/id/4">test article added by admin</a></h3>
					<p>hello short</p>
				</p>

                <a href="/common/articles/view/id/4">READ MORE &#187;</a>  
				<p style="clear: both;"></p>
				<!-- -->
			
			
			</div>
			
			<div class="join_us">
				<img src="/media/images/join_us_img.jpg" alt="Join Us" width="95" height="73" align="left" />
				<h2>Are you an expert?<br />Join us NOW!</h2>
				<a href="/expertfaq">click here to learn more &#187;</a>
			</div>
		
		</div>   <!-- .side_content -->  
		
		<div class="main_content">
		
			<div class="top_experts"> 
			   	
				<h2>
					TOP EXPERTS
					<span style="float: right; margin-top: -7px; *margin-top:-50px;">
						<a href="http://www.facebook.com/pages/ImYourAnswer/261280350569238"><img border="0" src="/media/images/facebook.png" height="32" width="32" /></a>
			    		<a href="http://twitter.com/ImYourAnswer"><img border="0" src="/media/images/twitter.png" height="32" width="32" /></a>
					</span>
				</h2>

				<div class="expert_profile_index">
					
					<div class="expert_profile">
						<img src="/media/images/avatar/125.jpg" width="85" height="85" />
						<h3><a href="/common/profile/view/id/125">MegaIT</a></h3>
						<p>11+ years experience in computer sciences and prac...</p>
					</div>
					
					<p style="clear: both;"></p>
					
					<div class="shortcuts">
						<a href="/common/profile/view/id/125"><img src="images/user_icon.png" width="15" height="15" style="padding-left: 15px;" /></a>
						<a href="/client/myexperts/add/id/125"><img src="images/star_icon.png" width="18" height="15" /></a>
						<a href="/question/postRequest/toExpert/id/125"><img src="images/comment_icon.png" width="16" height="15" /></a>
						<a class="more" href="/common/profile/view/id/125">read more &#187;</a>
					</div>
					
				</div>  <!-- .expert_profile_index -->
				
				<div class="expert_profile_index">
					
					<div class="expert_profile">
						<img src="/media/images/avatar/125.jpg" width="85" height="85" />
						<h3><a href="/common/profile/view/id/125">MegaIT</a></h3>
						<p>11+ years experience in computer sciences and prac...</p>
					</div>
					
					<p style="clear: both;"></p>
					
					<div class="shortcuts">
						<a href="/common/profile/view/id/125"><img src="images/user_icon.png" width="15" height="15" style="padding-left: 15px;" /></a>
						<a href="/client/myexperts/add/id/125"><img src="images/star_icon.png" width="18" height="15" /></a>
						<a href="/question/postRequest/toExpert/id/125"><img src="images/comment_icon.png" width="16" height="15" /></a>
						<a class="more" href="/common/profile/view/id/125">read more &#187;</a>
					</div>
					
				</div>  <!-- .expert_profile_index -->
				
				<div class="expert_profile_index">
					
					<div class="expert_profile">
						<img src="/media/images/avatar/125.jpg" width="85" height="85" />
						<h3><a href="/common/profile/view/id/125">MegaIT</a></h3>
						<p>11+ years experience in computer sciences and prac...</p>
					</div>
					
					<p style="clear: both;"></p>
					
					<div class="shortcuts">
						<a href="/common/profile/view/id/125"><img src="images/user_icon.png" width="15" height="15" style="padding-left: 15px;" /></a>
						<a href="/client/myexperts/add/id/125"><img src="images/star_icon.png" width="18" height="15" /></a>
						<a href="/question/postRequest/toExpert/id/125"><img src="images/comment_icon.png" width="16" height="15" /></a>
						<a class="more" href="/common/profile/view/id/125">read more &#187;</a>
					</div>
					
				</div> <!-- .expert_profile_index -->
			
			</div> <!-- .top_experts -->
		
			<p style="clear: both;"></p>
			
			<div class="meet_our_experts">
				
				<a href="/common/category/catID/1"><h2>Meet Our Experts</h2></a>
				<img src="/media/images/meet_our_experts.jpg" width="320" height="87" />
				<p>Our Experts are approved and verified for their expertise in various fields including, relationships, spirituality, technology, web design, business, pets, wellness, lifestyle and so much more.</p>
				<p>Our Experts are waiting to share their knowledge and are passionate about what they do. You can chat live right now with an expert, send a quick email or post a question on our QnABids bulletin board. Click on an Expert`s picture to read their full profile.</p>
			
			</div> <!-- .meet_our_experts -->
			
			<div class="advice_column">
				<h2>QnABids</h2>
				<img src="/media/images/advice_column.jpg" width="320" height="87" />
				<p>Get real-life advice from our Experts and choose what you want to pay. It's easy. Ask your question and let our Experts tell you how they can help you. Then you choose the Expert that is best qualified to answer your question, at the price you want to pay!</p>
				<p>Whether you have questions about our relationship, advice on becoming a new Mom, repairing your car, scratching your head about taxes, planting a vegtable garden or you need a web designer - look no further!</p>
			</div> <!-- .advice_column -->
		
		</div> <!-- .main_content -->
		
	    <p style="clear: both;"></p>
	    
	    <div class="footer">
			<p>
				<a href="/aboutus" target="_self">About Us</a> &nbsp; | &nbsp;
				<a href="/contactus" target="_self">Contact Us</a> &nbsp; | &nbsp;
				<a href="/newsletter">Newsletter</a> &nbsp; | &nbsp;
				<a href="/expert/registration" target="_self">Become an Expert</a> &nbsp; | &nbsp;
				<a href="/common/content/expertsList" target="_self">Expert Directory</a> &nbsp; | &nbsp;
				<a href="/qnabids" target="_self">QnABids</a> &nbsp; | &nbsp;
				<a href="/privacy" target="_self">Privacy Policy</a> &nbsp; | &nbsp;
				<a href="/faq" target="_self">FAQs</a> &nbsp; | &nbsp;
				<a href="/disclaimer" target="_self">Disclaimer</a>
			</p>
		</div> <!-- .footer -->
		
	</div> <!-- .page -->

</body>
</html>