
<style>

    .special_td .std{ padding:20px; border-bottom:solid 1px #BBB; }
    .special_td:last-child .std{ padding:20px; border-bottom:none; }

    .tb{ padding:10px; border:solid 1px #BBB; }

</style>

<script src="/media/javascript/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/media/javascript/uploadify/uploadify.css">

<script type="text/javascript">

    var fileSelected = 0;

    $(function()
    {

        /*
        $('#file_upload').uploadify(
        {
            'swf'      : '/media/javascript/uploadify/uploadify.swf',
            'uploader' : '/vadmin/as3/upload',
            'queueID' : 'queue',
            'auto' : false,
            'onSelect' : function()
            {

                fileSelected = 1;
                $('#queue-label').remove();

            },
            'onUploadComplete': function()
            {

                window.location = window.location;

            }

        });
        */

        $('#filmForm').submit(function(e)
        {

            var title = $("input[name='title']").val();

            if(!title)
            {

                e.preventDefault();
                alert("Please enter a title for this upload...");

            }
            /*
            else if(!fileSelected)
            {

                alert("Please select a file to upload...");

            }
            */
            else
            {

                $("input[name='submit']").attr('disabled', 'disabled').val("Uploading...");

                // $("#file_upload").uploadify("settings", "formData", {videoTitle : $("input[name='title']").val()});
                // $('#file_upload').uploadify('upload','*');

            }

        });

    });

</script>

<div class='round-top blue_box_nr' style='margin:10px 0 0;border-bottom:none;'>

    <table width='100%'>

        <tr>
            <td><h1>File Management</h1></td>
            <td align='right'>



            </td>
        </tr>

    </table>

</div>

<div class='round-bottom white_box_nr' style='padding:0;'>

    <div style='padding:20px;'>

        <form action='/vadmin/as3/upload' id='filmForm' class='well' method='POST' enctype="multipart/form-data">

            <legend>Upload A New File</legend>

            <p>Note: Files will not be immediately available on Amazon S3. They will be queued for upload and the administrator email will be notified when the upload has completed.</p>

            <table cellPadding='10'>
                <tr>
                    <td width='150'><b>File Title:</b></td>
                    <td><input type='text' name='title' value='' placeholder='Enter a title...' class='input-xxlarge'></td>
                </tr>
                <tr>
                    <td width='150' valign='top'><b>Upload A New File:</b></td>
                    <td>

                        <input id="file_upload" name="file_upload" type="file" multiple="true">

                        <!--
                        <div style='margin:25px 0 0;background:#FFF;' class='well'>
                            <div id="queue"><span id='queue-label'>Select a file above to start your upload...</span></div>
                        </div>
                        -->

                    </td>
                </tr>
                <tr>
                    <td width='150'><b>&nbsp;</b></td>
                    <td><input type='submit' name='submit' value='Start Upload' class='btn btn-large btn-primary'></td>
                </tr>
            </table>

        </form>

        <!-- Video File List -->

        <div class='well' style='background:#FFF;'>

            <table class='table table-striped table-hover'>

                <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Title:</th>
                        <th>Original Filename:</th>
                        <th>S3:</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>

                <?

                    foreach($files as $f)
                    {

                        echo "
                        <tr>
                            <td width='100'>".date("m/d/Y", strtotime($f['date']))."</td>
                            <td>".(trim($f['title']) ? $f['title'] : "No Video Title...")."</td>
                            <td>{$f['orig_file_name']}</td>
                            <td>".($f['s3'] ? "Uploaded" : "Pending")."</td>
                            <td width='50' style='text-align:center;'><a href='/vadmin/amazon/delete/{$f['id']}' onClick=\"Javascript:return confirm('Are you sure you want to delete this file? Anything you have linked will be broken...');\" class='btn'><span class='icon icon-trash icon-black'></span></a></td>
                            <td width='100' style='text-align:center;'><a href='/vadmin/amazon/stream/{$f['id']}' class='btn btn-inverse'>Stream</a></td>
                        </tr>
                        ";

                    }

                ?>

            </table>

        </div>

    </div>

</div>