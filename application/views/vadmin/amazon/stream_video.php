
    <p>&nbsp;</p>
    <p class='label label-info'>** Video is streaming using VODS3 via WOWZA on Amazon EC2 **</p><br />
    <p class='label label-info'>** Using FlowPlayer with RTMP support **</p>

    <!-- include flowplayer -->
    <script type="text/javascript" src="/media/flowplayer/flowplayer-3.2.12.min.js"></script>

    <a
        href="/media/films/<?=$filename?>"
        style="display:block;width:520px;height:330px"
        id="player">
    </a>

    <script>

        flowplayer("player", "/media/flowplayer/flowplayer-3.2.16.swf",
        {

            clip:
            {
                url: 'mp4:<?=$filename?>',
                scaling: 'fit',
                provider: 'hddn'
            },

            plugins:
            {

                hddn:
                {

                    url: "/media/flowplayer/flowplayer.rtmp-3.2.12.swf",
                    netConnectionUrl: 'rtmp://50.19.115.70/storytest/'
                }
            }

        });

    </script>