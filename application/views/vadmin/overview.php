
	<style>
	
		.special_td .std{ padding:20px; border-bottom:solid 1px #BBB; }
		.special_td:last-child .std{ padding:20px; border-bottom:none; }
		
		.tb{ padding:10px; border:solid 1px #BBB; }
	
	</style>

	<div class='round-top blue_box_nr' style='margin:10px 0 0;border-bottom:none;'>
	
		<table width='100%'>
		
			<tr>
				<td><h1><?=(!$subnav ? $nav['title'] : $nav['title'] . " > " . $subnav['title'])?></h1></td>
				<td align='right'>
				
					<a href='/vadmin/search/index/<?=$nav['id']?>/<?=($subnav['id'] ? $subnav['id'] : '0')?>' class='btn btn-inverse btn-mini'>Search</a> &nbsp;  &nbsp;
				
					<a href='/vadmin/main/add_record/<?=$nav['id']?>/<?=($subnav['id'] ? $subnav['id'] : '0')?>' class='btn btn-inverse btn-mini'>Add Record</a> &nbsp;  &nbsp;
					
					<a href='/vadmin/main/export_data/<?=$nav['id']?>/<?=($subnav['id'] ? $subnav['id'] : '0')?>' class='btn btn-inverse btn-mini'>Download CSV</a>
					
					<?=($pagination ? "<span class='pagination'>{$pagination}</span>" : "")?>
				
				</td>			
			</tr>
		
		</table>
	
	</div>
	
	<div class='round-bottom white_box_nr' style='padding:0;'>
	
		<div style='background:#EFECE5;padding:5px 5px 5px 15px;border-bottom:solid 1px #BBB;'><?=number_format($total_results)?> Record(s) Found</div>

		<?
		
			if(!$data)
			{
			
				// No Data
				echo "<p style='padding:10px;'>There is no data to display in ".(!$subnav ? $nav['title'] : $subnav['title'])." </p>";
			
			}
			else
			{
			
				echo "<table width='100%' cellPadding='10' cellspacing='0'>";
			
				// List Data
				foreach($data as $d)
				{
				
					echo "<tr class='special_td'>";
				
						foreach($fields as $f)
						{
						
							// Get Field Based On SPEC Data
							$spec_type = (isset($specs['spec'][$f]) ? $specs['spec'][$f]['value'] : "TB||50");
							$spec_array = explode("||", $spec_type);
							
							// Load & Configure The Module
							$specMod = strtolower(trim( $spec_array[0] ));
							if($f=='id') $specMod = 'lb';
							
							// Load Field
							$this->$specMod->config($f,$d[$f],$spec_array);
							$displayView = $this->$specMod->display_view();
						
							echo "<td class='std'>{$displayView}</td>";
						
						}
						
						echo "<td class='std' align='center' width='25'><a href='/vadmin/main/edit_record/{$nav['id']}/".($subnav['id'] ? $subnav['id'] : '0')."/{$d['id']}'>Edit</a></td>";
						echo "<td class='std' align='center' width='25'><a href='/vadmin/main/delete_record/{$nav['id']}/".($subnav['id'] ? $subnav['id'] : '0')."/{$d['id']}' onClick=\"Javascript:return confirm('Are you sure you want to delete this record?');\">Delete</a></td>";
						
					echo "</tr>";
				
				}
				
				echo "</table>";
			
			}
		
		?>
	
	</div>