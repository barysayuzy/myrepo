
	<div class='round-top blue_box_nr' style='margin:10px 0 0;border-bottom:none;'>
	
		<table width='100%'>
		
			<tr>
				<td><h1>Payments</h1></td>
				<td align='right'>
					
					<? if($transactions): ?>
					<a href='/vadmin/payments/init_payments' class='btn btn-small btn-danger' onClick="Javascript:return confirm('Are you sure you want to process a mass payment to all listed members?');">Process Mass Payment</a>
					<? endif; ?>
					
				</td>			
			</tr>
		
		</table>
	
	</div>
	
	<div class='round-bottom white_box_nr' style='padding:0;'>
	
		<div style='padding:20px;'>
		
			<?
			
				if(!$transactions)
				{
				
					// No Data
					echo "<p style='padding:10px;'>No transactions to process at this time...</p>";
				
				}
				else
				{
				
					echo "<table width='100%' cellPadding='10' cellspacing='0'>
					
						<tr class='special_td'>
								<td><b>Name:</b></td>
								<td><b>PayPal Email:</b></td>
								<td><b>Phone Number:</b></td>
								<td><b>Balance:</b></td>
							</tr>";
				
					// List Data
					foreach($transactions as $i => $user)
					{
					
						if($user['balance'] > 0)
						{
					
							echo "
							<tr class='special_td'>
								<td>{$user['first_name']} {$user['last_name']}</td>
								<td>{$user['paypal']}</td>
								<td>{$user['phone_number']}</td>
								<td>$".number_format($user['balance'], 2)."</td>
							</tr>
							";
						
						}
					
					}
					
					echo "</table>";
				
				}
			
			?>
		
		</div>
	
	</div>