
	<style>
	
		.special_td .std{ padding:20px; border-bottom:solid 1px #BBB; }
		.special_td:last-child .std{ padding:20px; border-bottom:none; }
		
		.tb{ padding:10px; border:solid 1px #BBB; }
	
	</style>

	<div class='round-top blue_box_nr' style='margin:10px 0 0;border-bottom:none;'>
	
		<table width='100%'>
		
			<tr>
				<td><h1>Search</h1></td>
				<td align='right'>
				
					&nbsp;
				
				</td>			
			</tr>
		
		</table>
	
	</div>
	
	<div class='round-bottom white_box_nr' style='padding:0;'>
	
		<form action='/vadmin/search/perform/<?=$this->uri->segment('4')?>/<?=$this->uri->segment('5')?>' method='POST'>
			
			<div align='center' style='padding:20px;'>
			<table cellPadding='10'>
				
				<tr>
					<td style='width:50px;'><b>Search:</b></td>
					<td><input type='text' name='query' style='width:300px;' value='<?=$this->input->post('query')?>'></td>
					<td><select name='field'>
					<?
					
						foreach($fields as $f)
						{
						
							echo "<option value='{$f}'".($this->input->post('field')==$f ? " selected" : "").">{$f}</option>";
						
						}
					
					?>
					</select></td>
					<td><input type='submit' name='submit' value='Search'></td>
				</tr>
				
			</table>
			</div>
			
			<div style='height:2px; background:#C0C0C0;margin:20px 0;'></div>
			
			<div style='padding:20px;'>
			
				<h1>Search Results</h1>
			
				<?
				
					if(isset($results)&&$results)
					{
					
						echo "<table cellPadding='10' width='100%' cellSpacing='0'>";
						
						foreach($results as $r)
						{
						
							echo "<tr>";
							
								foreach($overview as $o)
								{
								
									echo "<td style='border-bottom:solid 1px #E0E0E0;'>{$r[$o]}</td>";
								
								}
								
								echo "<td style='border-bottom:solid 1px #E0E0E0;' align='right'><a href='/vadmin/main/edit_record/{$this->uri->segment('4')}/{$this->uri->segment('5')}/{$r['id']}'>View</a></td>";
							
							echo "</tr>";
						
						}
						
						echo "</table>";
					
					}
					else
					{
					
						echo "<p>Enter a search above to start...</p>";
					
					}
				
				?>
			
			</div>
		
		</form>
	
	</div>