if ( ! window.hasOwnProperty('requirejs'))
{
	var requirejs = {};
}

requirejs.config({

	urlArgs: "bust=" + (new Date()).getTime(),

	paths: {
		'modernizr': [
//			'//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min',
			'vendor/modernizr-2.6.2.min'
		],
		'jquery': [
//			'//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery.min',
			'vendor/jquery-1.8.2.min'
		],
		'knockout': [
//			'//cdnjs.cloudflare.com/ajax/libs/knockout/2.2.1/knockout-min',
			'vendor/knockout-2.2.1.min'
		],
		'knockout-mapping': [
//			'//cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.3.5/knockout.mapping'
		],
		'bootstrap': [
//			'//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min',
			'vendor/bootstrap-2.3.1.min'
		]
	}
});