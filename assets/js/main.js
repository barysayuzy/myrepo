if ( ! window.hasOwnProperty('require'))
{
	var require = function() { };
}

var data = {
	sections: [
		{
			name: 'Courage',
			movies: [
				{ img: '/assets/uploads/movie1.jpg', watch: true },
				{ img: '/assets/uploads/movie2.jpg', buy: { 'Option 1': '#', 'Option 2': '#' }, rent: true },
				{ img: '/assets/uploads/movie3.jpg', find: { Amazon: '#', iTunes: '#' } },
				{ img: '/assets/uploads/movie1.jpg' },
				{ img: '/assets/uploads/movie4.jpg' },
				{ img: '/assets/uploads/movie5.jpg' }
			]
		},
		{
			name: 'Courage',
			movies: [
				{ img: '/assets/uploads/movie1.jpg' },
				{ img: '/assets/uploads/movie2.jpg' },
				{ img: '/assets/uploads/movie3.jpg' },
				{ img: '/assets/uploads/movie1.jpg' },
				{ img: '/assets/uploads/movie4.jpg' },
				{ img: '/assets/uploads/movie5.jpg' }
			]
		}
	]
};

require(['bootstrap']);

require(['knockout'], function(ko) {

	var Application = function(data, $context) {

		var self = this;

		$context = $context || $('body');

		for (var i in data)
		{
			self[i] = data[i];
		}

		self.showMovieTitles = function() {
			$('.movie.loading').removeClass('loading');
			$('.movie-titles', $context).fadeIn();
		};

		self.InfoWindow = (function() {

			var exports	= {},
				infowindow	= $('<div class="info-window clearfix" data-bind="template: { name: \'info-window\', data: InfoWindow }"></div>'),
				hider	= null;

			exports = {
				title		: ko.observable(),
				img			: ko.observable(),
				description	: ko.observable(),
				labels		: ko.observableArray([]),
				buttons		: ko.observable(),
				show: function(movie, event) {

					event && event.stopPropagation();
					clearTimeout(hider);

					if (movie)
					{
						infowindow.stop().hide();

						exports.img(movie.img);
						exports.title('Annie');
						exports.description('<p>Based on the Little Orphan Annie comic strip and adapted from the hit Broadway musical, "Annie" is the story of a plucky, red-headed orphan and her adventures in New York City as she tries to find a family to take her in.</p>');
						exports.labels(['Action / Adventure', 'Faith']);

						var buttons = ['<a href="#" class="btn btn-info">Watch Preview</a>'];

						if (movie.hasOwnProperty('watch'))
						{
							buttons.push('<a href="#" class="btn btn-info">Watch Now!</a>')
						}

						if (movie.hasOwnProperty('buy'))
						{
							var buy_button = '<a href="#" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Buy Now <b class="caret"></b></a><ul class="dropdown-menu">';

							for (var buy_option in movie.buy)
							{
								buy_button += '<li><a href="'+movie.buy[buy_option]+'">'+buy_option+'</a></li>';
							}

							buy_button += '</ul>';

							buttons.push(buy_button);
						}

						if (movie.hasOwnProperty('rent'))
						{
							buttons.push('<a href="#" class="btn btn-info">Rent Now</a>')
						}

						if (movie.hasOwnProperty('find'))
						{
							var find_button = '<a href="#" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Find Now <b class="caret"></b></a><ul class="dropdown-menu">';

							for (var find_option in movie.find)
							{
								find_button += '<li><a href="'+movie.find[find_option]+'">'+find_option+'</a></li>';
							}

							find_button += '</ul>';

							buttons.push(find_button);
						}

						$.each(buttons, function(i, btn) {
							buttons[i] = '<div class="btn-group">'+btn+'</div>';
						});

						exports.buttons(buttons.join(''));

						if (event && event.currentTarget)
						{
							var offset = $(event.currentTarget).offset();

							offset.top -= 10;
							offset.left -= 10;

							var $window = $(window);

							var maxleft = $window.width() - infowindow.outerWidth() - 10;
							var maxtop  = $window.height() - infowindow.outerHeight() - 10 + $window.scrollTop();
							var mintop  = $window.scrollTop() + 10;

							infowindow.css({
								top: Math.max(Math.min(offset.top, maxtop), mintop),
								left: Math.min(offset.left, maxleft)
							});
						}

						infowindow.fadeIn();

						infowindow.on('mousedown', exports._cancel);
						$(document).on('mousedown', exports.hide);
					}

				},
				hide: function() {
					infowindow.fadeOut('fast');

					$(document).off('mousedown', exports.hide);
					infowindow.off('mousedown', exports._cancel);
				},
				showIntent: function(movie, event) {
					clearTimeout(hider);
					var self = this;
					hider = setTimeout(function() {
						exports.show.call(self, movie, event);
					}, 250);
				},
				hideIntent: function() {
					clearTimeout(hider);
					hider = setTimeout(exports.hide, 1000);
				},
				_cancel: function(e) {
					e.stopPropagation();
				}
			};

			infowindow.hover(function() {
				exports.show();
			}, exports.hideIntent);

			infowindow.appendTo($context);

			return exports;

		})();

		ko.applyBindings(self);

		self.showMovieTitles();

		return self;
	};

	this.app = new Application(data);

});