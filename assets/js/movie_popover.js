
    //--- Initialize Popover menu
    //--- If the end-user holds their mouse over the movie poster
    //--- Longer than 1/2 second, then show a standardized movie popup
    var mouseoverTimeout;
    var httpRequest;

    $(function()
    {

        $('.genreSection .films a').hover(function()
        {

            $('#movie_popover').remove();

            var anchorObject = $(this);

            mouseoverTimeout = setTimeout(function()
            {

                generatePopoverMenu(anchorObject);

            }, 500);

        }, function()
        {

            clearTimeout(mouseoverTimeout);

        });

    });

    //--- Generate the popover menu
    function generatePopoverMenu(anchorObject)
    {

        //--- Obtain Position From Anchor Element
        var anchorPosition = anchorObject.position();
        var film_id = anchorObject.attr('data-movie-id');

        //--- Add popover div to DOM
        $('body').prepend("<div id='movie_popover'><div class='left' align='left'></div><div class='right' align='left'></div><div class='clearfix'></div></div>");

        //--- Check to make sure the popover isn't falling off the page
        //--- If it is, then let's scoot it over a bit to make sure it's not
        var furtherstRightPos = $(window).width();
        var moviePopoverFurthestRight = anchorPosition.left + $('#movie_popover').width();

        if(moviePopoverFurthestRight >= furtherstRightPos)
        {

            var moviePopoverDifference = Math.abs(moviePopoverFurthestRight-furtherstRightPos);

            anchorPosition.left -= moviePopoverDifference;

        }

        //--- Adjust Position Settings for #movie_popover
        $('#movie_popover').css({ top : anchorPosition.top-50, left : anchorPosition.left-35 });
        $('#movie_popover').fadeIn('slow', function()
        {

            //--- Load menu data
            loadMovie(film_id);

        });

        //--- Create a mouse-leave function to close the popup
        $('#movie_popover').mouseleave(function()
        {

            closePopoverMenu();

        });

    }

    //--- Load & Populate Popver Menu
    function loadMovie(filmId)
    {

        showLoadingIndicator();
        httpRequest = $.get('/movie/popover_data/'+filmId, function(object)
        {

            $('#movie_popover .left').hide();
            $('#movie_popover .right').hide();

            $('#movie_popover .left').html("<a href='/movie/view/"+filmId+"' class='learn_more'><img src='/assets/uploads/"+object.poster+"' class='img-polaroid' /></a>");
            $('#movie_popover .right').html("<div class='data'><div class='title'>"+object.title+"</div><div class='description'>"+object.description+"</div><a href='/movie/view/"+filmId+"' class='learn_more'>Learn More</a></div><div class='navbar'><div class='navbuttons'></div><div class='tags'></div></div>");

            /*
            if(object.type != 'show')
            {



            }
            */

            if(object.isPurchased)
            {

                $('#movie_popover .right .navbar .navbuttons').html("<a href='#' class='btn btn-info'>Preview</a> <a href='/movie/play/"+filmId+"' class='btn btn-info'>Play Now</a>");

            }
            else
            {

                if(typeof object.purchaseObject != 'undefined')
                {

                    var purchaseLinkString = "";
                    $.each(object.purchaseObject, function(i)
                    {

                        purchaseLinkString = purchaseLinkString + "<li><a href='"+this.link+"'>"+this.title+"</a></li>";

                    });

                    $('#movie_popover .right .navbar .navbuttons').html("<div class='dropdown'><a href='/movie/view/"+filmId+"/preview' class='btn btn-info'>Preview</a> <a class='dropdown-toggle btn btn-info' data-toggle='dropdown' href='#'>Play Now <span class='caret'></span></a><ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>"+purchaseLinkString+"</ul></div>");

                }
                else
                {

                    $('#movie_popover .right .navbar .navbuttons').html("<a href='/movie/view/"+filmId+"/preview' class='btn btn-info'>Preview</a>");

                }

            }

            //--- Update movie tags
            $('#movie_popover .right .navbar  .tags').html("");
            if(object.value) $('#movie_popover .right .navbar  .tags').append("<span class='label'>"+object.value+"</span> ");
            if(object.educationalValue) $('#movie_popover .right .navbar  .tags').append("<span class='label'>"+object.educationalValue+"</span>");

            clearLoadingIndicator();

            $('#movie_popover .left').fadeIn('slow');
            $('#movie_popover .right').fadeIn('slow');

        }, 'json');

    }

    function showLoadingIndicator()
    {

        $('#movie_popover .left').append("<div id='loadingIndicator'>L o a d i n g . . .</div>");

    }

    function clearLoadingIndicator()
    {

        $('#movie_popover .left #loadingIndicator').remove();

    }

    function closePopoverMenu()
    {

        //--- Clear popover from dom
        $('#movie_popover').remove();

        //--- Abort any ajax requests
        if(httpRequest)
        {
            httpRequest.abort();
        }

    }
